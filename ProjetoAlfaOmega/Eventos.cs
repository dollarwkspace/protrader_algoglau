﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using AlgoGlauNamespace.SDKEncapsulation;
using PTLRuntime.NETScript;
using PTLRuntime.NETScript.Charts;

namespace AlgoGlauNamespace
{
    public class Eventos
    {
        private ProjectAlgoGlau robo = null;
        private int i, j = 0;
        public Eventos(ProjectAlgoGlau _robo) => robo = _robo;
        public void Orders_OrderAdded(Order obj) => robo.mainControl.ordersController.UpdateOrders();
        public void Orders_OrderRemoved(Order obj)
        {
            robo.mainControl.RemoveFromInnerStatesBuffer(InnerState.lockdownPorOrdemEnviada);
            robo.mainControl.ordersController.UpdateOrders();
        }
        public void Orders_OrderExecuted(Order obj) => robo.mainControl.ordersController.UpdateOrders(obj.Id);

        public void OnPaintChart(object sender, PaintChartEventArgs args)
        {
            try
            {
                if (args.Rectangle.Height > 350)
                {
                    args.Graphics.DrawString($"Horário:{robo.horario} ,Calor máximo: R${robo.calor.ToString()} , Máx Contratos: {robo.maximoDeContratosAbertos.ToString()}  ,Estado atual: {robo.estado.ToString()} ,contadorDeReEntradas: {robo.contadorDeReEntradas}", robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Height - 16);
                    args.Graphics.DrawString($"Diferença de Médias  ==> ", robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Height - 1);
                    args.Graphics.DrawString($"Sinal: {Math.Round(robo.GetDifMediasCurta(), 2)}", robo.font, robo.brush, args.Rectangle.X + 270, args.Rectangle.Height - 1);
                    args.Graphics.DrawString($",Suporte: {Math.Round(robo.GetDifMediasLonga(), 2)}", robo.font, robo.brush, args.Rectangle.X + 370, args.Rectangle.Height - 1);                    
                    if (robo.estado == State.trailingStop) args.Graphics.DrawString($"Pts do médio: {robo.mainControl.PontosDoMedio}", robo.font, robo.brushAlert, args.Rectangle.X + 5, args.Rectangle.Height - 140);
                    if (robo.estado == State.reentrada) args.Graphics.DrawString($"Pts do médio: {robo.mainControl.PontosDoMedio}", robo.font, robo.brushAlert, args.Rectangle.X + 5, args.Rectangle.Height - 155);
                    if (robo.estado == State.reentrada) args.Graphics.DrawString($"Próx.ReEn: {robo.proximaReEntrada}", robo.font, robo.brushAlert, args.Rectangle.X + 5, args.Rectangle.Height - 140);
                    args.Graphics.DrawString($"Lucro/dia: {robo.lucroDia}", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 110);
                    args.Graphics.DrawString($"Loss/dia: {robo.lossDia}", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 95);
                    args.Graphics.DrawString($"Resultado/dia: {robo.mainControl.timelineController.ResultadoDia}", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 80);
                    args.Graphics.DrawString($"TamTick: {robo.GetTickSize()}", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 65);
                    args.Graphics.DrawString($"Cotação: {robo.cotacaoAtual}", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 50);
                    args.Graphics.DrawString($"Max:{robo.maxMs} tks", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 35);
                    args.Graphics.DrawString($"Min:{robo.minMs} tks", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 20);
                    args.Graphics.DrawString($"Cur:{robo.ms} tks", robo.font, robo.brush, args.Rectangle.X + 5, args.Rectangle.Height - 5);
                }

                if (robo.mainControl.timelineController.PosicaoMedia.Amount != 0 && args.Rectangle.Height > 350)
                {
                    args.Graphics.DrawString("Posição", robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Qtde", robo.font, robo.brush, args.Rectangle.X + 180, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Preço Médio", robo.font, robo.brush, args.Rectangle.X + 220, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Preço Atual", robo.font, robo.brush, args.Rectangle.X + 320, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Tipo", robo.font, robo.brush, args.Rectangle.X + 420, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Lucro/Prejuízo", robo.font, robo.brush, args.Rectangle.X + 480, args.Rectangle.Y + 20);
                    args.Graphics.DrawString("Robô ->", robo.font, robo.brush, args.Rectangle.X + 10, args.Rectangle.Y + 40);
                    args.Graphics.DrawLine(new Pen(Color.White), new PointF(args.Rectangle.X + 110, args.Rectangle.Y + 60), new PointF(args.Rectangle.X + 610, args.Rectangle.Y + 60));
                    args.Graphics.DrawString(robo.mainControl.timelineController.PosicaoMedia.Amount.ToString(), robo.font, robo.brush, args.Rectangle.X + 180, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(String.Format("{0:C}", robo.mainControl.timelineController.PosicaoMedia.OpenPrice), robo.font, robo.brush, args.Rectangle.X + 220, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(String.Format("{0:C}", robo.cotacaoAtual), robo.font, robo.brush, args.Rectangle.X + 320, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(robo.mainControl.timelineController.PosicaoMedia.Side.ToString(), robo.font, robo.brush, args.Rectangle.X + 420, args.Rectangle.Y + 40);
                    args.Graphics.DrawString(String.Format("{0:C}", robo.mainControl.timelineController.PosicaoMedia.Lucro.ToString()), robo.font, robo.brush, args.Rectangle.X + 480, args.Rectangle.Y + 40);
                    args.Graphics.DrawString("Posicao Ref:", robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Height - 46);
                    if (robo.mainControl.timelineController.PosicaoAtiva != null)
                    {
                        args.Graphics.DrawString($"Qtde: {robo.mainControl.timelineController.PosicaoAtiva?.Amount.ToString()}", robo.font, robo.brush, args.Rectangle.X + 190, args.Rectangle.Height - 46);
                        args.Graphics.DrawString($"OpenPrice: {String.Format("{0:C}", robo.mainControl.timelineController.PosicaoAtiva?.OpenPrice)}", robo.font, robo.brush, args.Rectangle.X + 260, args.Rectangle.Height - 46);
                        args.Graphics.DrawString($"Tipo: {robo.mainControl.timelineController.PosicaoAtiva?.Side.ToString()}", robo.font, robo.brush, args.Rectangle.X + 420, args.Rectangle.Height - 46);
                    }
                    else
                    {
                        args.Graphics.DrawString("NULA", robo.font, robo.brush, args.Rectangle.X + 250, args.Rectangle.Height - 20);
                    }
                    DrawPositions(args.Graphics, args.Rectangle);

                    int count = robo.mainControl.ordersController.orders.Count();
                    args.Graphics.DrawString($"Orders: ", robo.font, robo.brush, args.Rectangle.X + 110, args.Rectangle.Y + 20);

                }


                if (args.Rectangle.Height > 350) DrawLog(args.Graphics, args.Rectangle);

                
            }
            catch (Exception exc)
            {
                //robo.Print($"OnPaintChart Erro {exc.Message}");                
            }
        }


        private void DrawPositions(Graphics gr, Rectangle rect)
        {
            j = 80;
            try
            {
                if (robo.showAllTimeline)
                {
                    robo.mainControl.timelineController.AllItems?.ForEach(posicao =>
                    {
                        string horaEntrada = posicao.Entrada.OpenTime?.ToLocalTime().ToShortTimeString();
                        string qtdeEntrada = posicao.Entrada.Amount.ToString();
                        string precoEntrada = String.Format("{0:C}", posicao.Entrada.OpenPrice);
                        string ladoEntrada = posicao.Entrada.Side.ToString();
                        double lucroEntrada = posicao.Entrada.Lucro ?? 0;
                        string horaSaida = (posicao.Saida?.OpenTime?.ToLocalTime().ToShortTimeString()) ?? "";
                        string qtdeSaida = posicao.Saida?.Amount.ToString() ?? "0";
                        string precoSaida = String.Format("{0:C}", posicao.Saida?.OpenPrice) ?? "0";
                        string ladoSaida = posicao.Saida?.Side.ToString() ?? "---";
                        double lucroSaida = posicao.Saida?.Lucro ?? 0;

                        gr.DrawString(
                        $"( In: {horaEntrada} {qtdeEntrada} {ladoEntrada} R${precoEntrada} / Out: {horaSaida} {qtdeSaida} {ladoSaida} R${precoSaida} ) -> Lucro: {lucroEntrada + lucroSaida} "
                            , robo.font, robo.brush, rect.X + 110, rect.Y + j);
                        j += 15;
                    });
                }
                else
                {

                    robo.mainControl.timelineController.ItensAtivos?.ForEach(posicao =>
                    {
                        string horaEntrada = posicao.Entrada.OpenTime?.ToLocalTime().ToShortTimeString();
                        string qtdeEntrada = posicao.Entrada.Amount.ToString();
                        string precoEntrada = String.Format("{0:C}", posicao.Entrada.OpenPrice);
                        string ladoEntrada = posicao.Entrada.Side.ToString();
                        double lucroEntrada = posicao.Entrada.Lucro ?? 0;
                        string horaSaida = (posicao.Saida?.OpenTime?.ToLocalTime().ToShortTimeString()) ?? "";
                        string qtdeSaida = posicao.Saida?.Amount.ToString() ?? "0";
                        string precoSaida = String.Format("{0:C}", posicao.Saida?.OpenPrice) ?? "0";
                        string ladoSaida = posicao.Saida?.Side.ToString() ?? "---";
                        double lucroSaida = posicao.Saida?.Lucro ?? 0;

                        gr.DrawString(
                        $"( In: {horaEntrada} {qtdeEntrada} {ladoEntrada} R${precoEntrada} / Out: {horaSaida} {qtdeSaida} {ladoSaida} R${precoSaida} ) -> Lucro: {lucroEntrada + lucroSaida} "
                            , robo.font, robo.brush, rect.X + 110, rect.Y + j);
                        j += 15;
                    });
                }
            }
            catch (Exception exc)
            {
                robo.Print($"DrawPositions: Erro {exc.Message}");
            }
        }

        private void DrawLog(Graphics gr, Rectangle rect)
        {
            i = 100;
            try
            {
                robo.logDin.ForEach(logItem =>
                {
                    gr.DrawString(logItem, robo.font, robo.brush, rect.X + 10, j + 50 + i);
                    i += 15;
                });
            }
            catch (Exception exc)
            {

            }
        }

    }
}
