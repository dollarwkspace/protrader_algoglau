﻿using PTLRuntime.NETScript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoGlauNamespace.SDKEncapsulation
{
    public class PositionWrapper : IEquatable<PositionWrapper>
    {
        public PositionWrapper() { }
        public string Id { get; set; }
        public double? Amount { get; set; } //nullable type
        public Operation? Side { get; set; } //nullable type
        public double? OpenPrice { get; set; } //nullable type        
        public double? Lucro { get; set; } //nullable type
        public DateTime? OpenTime { get; set; } //nullable type
        public string NumRobo { get; set; }

        public bool Equals(PositionWrapper other)
        {
            if (GetType() != other.GetType()) return false;
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return OpenPrice.Equals(other.OpenPrice) && Amount.Equals(other.Amount) &&
                   Side.Equals(other.Side);
        }

        public bool NotEquals(PositionWrapper other)
        {
            if (GetType() != other.GetType()) return true;
            if (ReferenceEquals(null, other)) return true;
            if (ReferenceEquals(this, other)) return false;
            return !Id.Equals(other.Id) || !OpenPrice.Equals(other.OpenPrice) || !Amount.Equals(other.Amount) ||
                   !Side.Equals(other.Side) || !NumRobo.Equals(other.NumRobo);
        }

        public override int GetHashCode() => Id.GetHashCode();

        public static bool operator ==(PositionWrapper pos1, PositionWrapper pos2) => pos1.Equals(pos2);
        public static bool operator !=(PositionWrapper pos1, PositionWrapper pos2) => pos1.NotEquals(pos2);

        public static PositionWrapper operator +(PositionWrapper pos1, PositionWrapper pos2)
        {
            try
            {
                if (ReferenceEquals(pos1, null)) return pos1;
                if (ReferenceEquals(pos2, null)) return pos2;

                if (pos1.Amount == 0 && pos2.Amount == 0)
                {
                    return new PositionWrapper()
                    {
                        Amount = 0, OpenPrice = 0, Lucro = (pos1.Lucro ?? 0) + (pos2.Lucro ?? 0), Side = null
                    };
                }

                if (pos1.Side == pos2.Side)
                {
                    return new PositionWrapper()
                    {
                        Amount = (pos1?.Amount ?? 0) + (pos2?.Amount ?? 0),
                        OpenPrice = (((pos1?.Amount ?? 0) * (pos1?.OpenPrice ?? 0)) + ((pos2?.Amount ?? 0) * (pos2?.OpenPrice ?? 0))) / ((pos1?.Amount ?? 0) + (pos2?.Amount ?? 0)),
                        Side = pos1?.Side,
                        Id = pos1?.Id,
                        Lucro = (pos1.Lucro ?? 0) + (pos2.Lucro ?? 0),
                        NumRobo = pos1?.NumRobo,
                        OpenTime = pos1?.OpenTime
                    };
                }
                else
                {
                    PositionWrapper pReturn = new PositionWrapper()
                    {
                        Id = pos1.Id,
                        NumRobo = pos1.NumRobo,
                        OpenTime = pos1.OpenTime
                    };

                    pReturn.Amount = (pos1?.Amount ?? 0) + (pos2?.Amount ?? 0);
                    pReturn.Side = (pReturn.Amount > 0) ? Operation.Sell : Operation.Buy;

                    if (pReturn.Amount == 0)
                    {
                        pReturn.Lucro = (pos1.Lucro ?? 0) + (pos2.Lucro ?? 0) + (pos1.Amount ?? 0) * ((pos1.OpenPrice ?? 0) - (pos2.OpenPrice ?? 0));
                        pReturn.Side = null;
                        pReturn.OpenPrice = 0;
                    }
                    if (pReturn.Amount != 0)
                    {
                        double? contratosFechados = (Math.Abs(pos1.Amount ?? 0) < Math.Abs(pos2.Amount ?? 0)) ? pos1.Amount : pos2.Amount;
                        if (pos1.Side == Operation.Buy)
                        {
                            pReturn.Lucro = Math.Abs(contratosFechados ?? 0) * (pos2.OpenPrice - pos1.OpenPrice);
                            pReturn.Lucro += (pos1.Lucro ?? 0) + (pos2.Lucro ?? 0);
                        }
                        else
                        {
                            pReturn.Lucro = Math.Abs(contratosFechados ?? 0) * (pos1.OpenPrice - pos2.OpenPrice);
                            pReturn.Lucro += (pos1.Lucro ?? 0) + (pos2.Lucro ?? 0);
                        }

                        pReturn.OpenPrice = (Math.Abs(pos1?.Amount ?? 0) > Math.Abs(pos2?.Amount ?? 0)) ? (pos1.OpenPrice ?? 0) : (pos2.OpenPrice ?? 0);
                    }

                    return pReturn;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
