﻿using PTLRuntime.NETScript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoGlauNamespace.SDKEncapsulation
{
    public interface IOrderRepository
    {
        HashSet<OrderWrapper> GetOrdersFromServer();
        string SendOrdersToServer(INewOrderRequestWrapper request);
        bool ClearOrders();
    }
    public class OrderRepository : IOrderRepository
    {
        private ProjectAlgoGlau robo;
        private List<Order> ordersTemp = new List<Order>();
        private HashSet<OrderWrapper> buffer = new HashSet<OrderWrapper>();        
        public OrderRepository(ProjectAlgoGlau robo) => this.robo = robo;
        public HashSet<OrderWrapper> GetOrdersFromServer()
        {
            ordersTemp.Clear();
            buffer.Clear();
            ordersTemp = robo.Orders.GetOrders(true)?.ToList().FindAll(x => x.Instrument.BaseName == robo.papel && x.Comment == robo.numeroRobo.ToString());
           
            if (ordersTemp.Count > 0)
            {                
                ordersTemp.ForEach(ord => 
                {
                    double convertedAmount = ord.Amount;
                    //if (ord.Amount > 0 && ord.Side == Operation.Buy) convertedAmount = +ord.Amount;
                    if (ord.Amount < 0 && ord.Side == Operation.Sell) convertedAmount = -ord.Amount;
                    buffer.Add(
                    new OrderWrapper(robo)
                    {
                        Id = ord.Id,
                        Type = ord.Type,
                        Amount = convertedAmount,
                        Side = ord.Side,
                        Price = ord.Price,
                        LinkedTo = ord.LinkedTo,
                        NumRobo = ord.Comment,
                        Status = ord.Status,
                        IsActive = ord.IsActive,
                        OpenTime = ord.Time,
                        CloseTime = ord.CloseTime,
                        FilledAmount = ord.FilledAmount,
                        FillPrice = ord.FillPrice
                    });
                });                
            }
            return buffer;
        }

        public string SendOrdersToServer(INewOrderRequestWrapper req)
        {
            NewOrderRequest request = new NewOrderRequest(){
                Account = robo.conta,
                Amount = req.Amount,
                Comment = robo.numeroRobo.ToString(),
                ExpirationTime = req.ExpirationTime,
                Instrument = req.Instrument,
                LinkTo = req.LinkTo,
                MagicNumber = robo.numeroRobo,
                MarketRange = req.MarketRange,
                Price = req.Price,
                ProductType = req.ProductType,
                Side = req.Side,
                StopLossOffset = req.StopLossOffset,
                StopPrice = req.StopPrice,
                TakeProfitOffset = req.TakeProfitOffset,
                TimeInForce = req.TimeInForce,
                TrStopOffset = req.TrStopOffset,
                Type = req.Type
            };
            return robo.Orders.Send(request);
        }

        public bool ClearOrders()
        {
            ordersTemp.Clear();
            bool allCanceled = true;
            ordersTemp = robo.Orders.GetOrders(true)?.ToList().FindAll(x => x.Instrument.BaseName == robo.papel && x.Comment == robo.numeroRobo.ToString());
            if (ordersTemp.Count > 0) ordersTemp.ForEach(ord => allCanceled = ord.Cancel());
            return allCanceled;
        }

    }
}
