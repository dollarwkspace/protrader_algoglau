﻿using PTLRuntime.NETScript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoGlauNamespace.SDKEncapsulation
{
    public interface IPositionRepository
    {
        HashSet<PositionWrapper> GetPositionsFromServer();        
    }
    public class PositionRepository : IPositionRepository
    {
        private ProjectAlgoGlau robo;
        private List<Position> positionsTemp = new List<Position>();
        private HashSet<PositionWrapper> buffer = new HashSet<PositionWrapper>();        
        public PositionRepository(ProjectAlgoGlau robo) => this.robo = robo;
        public HashSet<PositionWrapper> GetPositionsFromServer()
        {
            positionsTemp.Clear();            
            positionsTemp = robo.Positions.GetPositions()?.ToList().FindAll(x => x.Instrument.BaseName == robo.papel);            
            
            if (positionsTemp.Count > 0)
            {
                buffer.Clear();
                positionsTemp.ForEach(pos => 
                {
                    buffer.Add(
                    new PositionWrapper()
                    {
                        Id = pos.Id,                        
                        Amount = pos.Amount,
                        Side = pos.Side,
                        OpenPrice = pos.OpenPrice,
                        OpenTime = pos.OpenTime,
                        NumRobo = pos.Comment                        
                    });
                });                
            }
            return buffer;
        }

    }
}
