﻿using PTLRuntime.NETScript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoGlauNamespace.SDKEncapsulation
{
    public interface IQuoteRepository
    {
        double GetAsk();
        double GetBid();
        double GetLast();
        DateTime GetTime();
    }

    public class QuoteRepository : IQuoteRepository
    {
        private ProjectAlgoGlau robo;
        public QuoteRepository(ProjectAlgoGlau robo) => this.robo = robo;

        public DateTime GetTime() => robo.Instruments.Current.LastQuote?.Time ?? DateTime.MinValue;
        public double GetAsk() => robo.Instruments.Current.LastQuote?.Ask ?? 0;
        public double GetBid() => robo.Instruments.Current.LastQuote?.Bid ?? 0;
        public double GetLast() => robo.Instruments.Current.LastQuote?.Last ?? 0;
    }
}
