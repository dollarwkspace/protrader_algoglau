﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoGlauNamespace.SDKEncapsulation
{
    public class PositionsController
    {
        private IPositionRepository positionRepository;
        public PositionsController(ProjectAlgoGlau robo, IPositionRepository pos=null)
        {
            positionRepository = pos ?? new PositionRepository(robo);
        }

        private HashSet<PositionWrapper> _positions = new HashSet<PositionWrapper>();
        private HashSet<PositionWrapper> _bufferPositions = new HashSet<PositionWrapper>();
        public PositionWrapper[] positions { get => _positions.ToArray(); }

        public PositionWrapper[] UpdatePositions()
        {
            _bufferPositions.Clear();
            _bufferPositions = positionRepository.GetPositionsFromServer();
            _positions = _bufferPositions;
            return positions;
        }
    }
}
