﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoGlauNamespace.SDKEncapsulation
{
    public class QuoteWrapper
    {
        private IQuoteRepository quoteRepo;
        public QuoteWrapper(ProjectAlgoGlau robo, IQuoteRepository quoteRepo=null)
        {
            this.quoteRepo = quoteRepo ?? new QuoteRepository(robo);
        }
        public DateTime Time { get => quoteRepo.GetTime();  } 
        public double Ask { get => quoteRepo.GetAsk(); }
        public double Bid { get => quoteRepo.GetBid(); }
        public double Last { get => quoteRepo.GetLast(); }
    }
}
