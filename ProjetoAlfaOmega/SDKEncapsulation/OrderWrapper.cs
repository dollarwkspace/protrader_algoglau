﻿using PTLRuntime.NETScript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoGlauNamespace.SDKEncapsulation
{
    public class OrderWrapper : IEquatable<OrderWrapper>
    {
        private ProjectAlgoGlau robo;
        public OrderWrapper(ProjectAlgoGlau robo) => this.robo = robo;
        public string Id { get; set; }
        public OrdersType? Type { get; set; } //nullable type
        public double Amount { get; set; }
        public Operation? Side { get; set; } //nullable type
        public double Price { get; set; }
        public string LinkedTo { get; set; }
        public string NumRobo { get; set; }
        public OrderStatus? Status { get; set; } //nullable type
        public bool? IsActive { get; set; } //nullable type
        public double? FillPrice { get; set; } //nullable type
        public double? FilledAmount { get; set; } //nullable type
        public DateTime? OpenTime { get; set; } //nullable type
        public DateTime? CloseTime { get; set; } //nullable type

        public bool Equals(OrderWrapper other)
        {
            if (GetType() != other.GetType()) return false;
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id.Equals(other.Id) && Price.Equals(other.Price) && Amount.Equals(other.Amount) &&
                   Side.Equals(other.Side) && LinkedTo.Equals(other.LinkedTo) && IsActive.Equals(other.IsActive) &&
                   NumRobo.Equals(other.NumRobo) && Status.Equals(other.Status) && Type.Equals(other.Type);
        }

        public bool NotEquals(OrderWrapper other)
        {
            if (GetType() != other.GetType()) return true;
            if (ReferenceEquals(null, other)) return true;
            if (ReferenceEquals(this, other)) return false;
            return !Id.Equals(other.Id) || !Price.Equals(other.Price) || !Amount.Equals(other.Amount) ||
                   !Side.Equals(other.Side) || !LinkedTo.Equals(other.LinkedTo) || !IsActive.Equals(other.IsActive) ||
                   !NumRobo.Equals(other.NumRobo) || !Status.Equals(other.Status) || !Type.Equals(other.Type);
        }

        public bool Modify(double newPrice, double newAmount) => robo.Orders.GetOrderById(Id)?.Modify(newPrice, newAmount) ?? false;
        public bool Modify(double newPrice) => robo.Orders.GetOrderById(Id)?.Modify(newPrice) ?? false;

        public override int GetHashCode() => Id.GetHashCode() + Price.GetHashCode() + Amount.GetHashCode();

        //public static bool operator >(OrderWrapper ord1, OrderWrapper ord2) => ord1.Id.CompareTo(ord2.Id) == 1;
        //public static bool operator >=(OrderWrapper ord1, OrderWrapper ord2) => ord1.Id.CompareTo(ord2.Id) >= 0;
        //public static bool operator <(OrderWrapper ord1, OrderWrapper ord2) => ord1.Id.CompareTo(ord2.Id) == -1;
        //public static bool operator <=(OrderWrapper ord1, OrderWrapper ord2) => ord1.Id.CompareTo(ord2.Id) <= 0;
        public static bool operator ==(OrderWrapper ord1, OrderWrapper ord2) => ord1.Equals(ord2);
        public static bool operator !=(OrderWrapper ord1, OrderWrapper ord2) => ord1.NotEquals(ord2);

    }

}
