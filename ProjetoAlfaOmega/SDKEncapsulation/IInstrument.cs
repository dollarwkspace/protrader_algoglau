﻿using PTLRuntime.NETScript;
using PTLRuntime.NETScript.Options;
using System;

namespace AlgoGlauNamespace.SDKEncapsulation
{
      public interface IInstrument
    {
          double Low { get; }
          double High { get; }
          double Open { get; }
          OptionExerciseStyle? OptionExerciseStyle { get; }
          OptionType OptionType { get; }
          double Strike { get; }
          DateTime LastTradingDate { get; }
          DateTime MaturityDate { get; }
          DateTime ExpirationDate { get; }
          double CrossPrice { get; }
          int Precision { get; }
          int QuoteDelay { get; }
          decimal PipsSize { get; }
          InstrumentType Type { get; }
          double LotStep { get; }
          double Close { get; }
          double PrevClose { get; }
          double? FiftyTwoWeekHighPrice { get; }
          double? FiftyTwoWeekLowPrice { get; }
          Quote LastQuote { get; }
          Quote FirstQuote { get; }
          Instrument UnderlierInstrument { get; }
          string BaseName { get; }
          double VolumePostMarket { get; }
          bool AllowShortPositions { get; }
          double VolumePreMarket { get; }
          double MinimalLot { get; }
          long TicksPostMarket { get; }
          long Ticks { get; }
          LimitType LimitType { get; }
          double LowLimit { get; }
          double HighLimit { get; }
          double Volume { get; }
          double PrevSettlementPrice { get; }
          double SettlementPrice { get; }
          long TicksPreMarket { get; }
          QuotingType QuotingType { get; }
          SwapModel SwapModel { get; }
          double SwapBuy { get; }
          DataType ChartType { get; }
          string Description { get; }
          TradingPosition TradingPositionMode { get; }
          TradingMode TradingStatus { get; }
          double TickCost { get; }
          Asset CounterCurrency { get; }
          Asset BaseCurrency { get; }
          string Route { get; }
          string Symbol { get; }
          string Name { get; }
          double TickSize { get; }
          int LotSize { get; }
          double SwapSell { get; }

          event Action<Instrument> Update;

          string FormatPrice(double price, int precision);
          bool Equals(object obj);
          bool Equals(Instrument obj);
          string FormatPrice(double price);
          string GetAdditionalValue(string fieldName);
          Level2[] getAsksDepth();
          Level2[] getBidsDepth();
          DerivativeSettings GetDerivativeSettings();
          int GetHashCode();
          Trade GetLastTrade();
          MarginSettings GetMarginSettings();
          PricesQuote GetPricesQuote();
          TradingSession GetTradingSessionActive();
          TradingSession[] GetTradingSessions();
          double getVWAPAsk(double amount);
          double getVWAPBid(double amount);
          bool IsAllowableOrderType(OrdersType ordersType);
          bool IsAllowableTimeInForce(TimeInForce timeInForce, OrdersType ordersType);
          double RoundPrice(double price);
          double RoundProfit(double profit);          
          
    }
}
