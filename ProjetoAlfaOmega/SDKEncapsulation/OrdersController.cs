﻿using PTLRuntime.NETScript;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoGlauNamespace.SDKEncapsulation
{
    public class OrdersController
    {
        private IOrderRepository _orderRepository;
        public OrdersController(IOrderRepository orderRepository) => _orderRepository = orderRepository;

        public event Action<OrderWrapper> OrderAddedEvent;
        public event Action<OrderWrapper> OrderRemovedEvent;
        public event Action OrderEmptyEvent;
        public event Action<OrderWrapper> OrderExecutedEvent;

        private void OnOrderAddedToBuffer(OrderWrapper ord) => OrderAddedEvent?.Invoke(ord);
        private void OnOrderRemoved(OrderWrapper ord) => OrderRemovedEvent?.Invoke(ord);
        private void OnOrderEmpty() => OrderEmptyEvent?.Invoke();
        private void OnOrderExecuted(OrderWrapper ord) => OrderExecutedEvent?.Invoke(ord);

        private HashSet<OrderWrapper> _orders = new HashSet<OrderWrapper>();
        private HashSet<OrderWrapper> _bufferorders = new HashSet<OrderWrapper>();
        public OrderWrapper[] orders { get => _orders.ToArray(); }
        public OrderWrapper[] buffer { get => _bufferorders.ToArray(); }

        public OrderWrapper[] UpdateOrders(string ExecutedId = "")
        {
            _bufferorders = _orderRepository.GetOrdersFromServer();
            _bufferorders = _bufferorders == null ? new HashSet<OrderWrapper>() : _bufferorders;
            _bufferorders?.Except(_orders)?.ToList().ForEach(ord => { _orders.Add(ord); OnOrderAddedToBuffer(ord); });

            _orders?.Except(_bufferorders)?.ToList().ForEach(ord =>
                {
                    if (_orders.Contains(ord))
                    {
                        _orders.Remove(ord);
                        if (ord.Id == ExecutedId)
                        {
                            OnOrderExecuted(ord);
                        }
                        else
                        {
                            OnOrderRemoved(ord);
                        }
                    }
                    if (_orders.Count == 0) OnOrderEmpty();
                });
            
            _orders = _bufferorders?.ToHashSet();
            return orders;
        }

        public string SendOrders(INewOrderRequestWrapper request)
        {
            return _orderRepository.SendOrdersToServer(request);
        }

        public bool ClearOrders() => _orderRepository.ClearOrders();

    }

}
