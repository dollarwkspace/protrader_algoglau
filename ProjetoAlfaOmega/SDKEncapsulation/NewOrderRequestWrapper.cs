﻿using PTLRuntime.NETScript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoGlauNamespace.SDKEncapsulation
{
    public interface INewOrderRequestWrapper
    {
        Account Account { get; set; }
        double Amount { get; set; }
        string Comment { get; set; }
        DateTime ExpirationTime { get; set; }
        Instrument Instrument { get; set; }
        string LinkTo { get; set; }
        int MagicNumber { get; set; }
        int MarketRange { get; set; }
        double Price { get; set; }
        ProductType ProductType { get; set; }
        Operation Side { get; set; }
        double StopLossOffset { get; set; }
        double StopPrice { get; set; }
        double TakeProfitOffset { get; set; }
        TimeInForce TimeInForce { get; set; }
        double TrStopOffset { get; set; }
        OrdersType Type { get; set; }
    }

    public class NewOrderRequestWrapper : INewOrderRequestWrapper
    {
        public double Amount { get; set; }
        public double Price { get; set; }
        public double StopPrice { get; set; }
        public TimeInForce TimeInForce { get; set; }
        public OrdersType Type { get; set; }
        public Account Account { get; set; }
        public string Comment { get; set; }
        public Operation Side { get; set; }
        public string LinkTo { get; set; }
        public double StopLossOffset { get; set; }
        public double TakeProfitOffset { get; set; }
        public double TrStopOffset { get; set; }
        public int MarketRange { get; set; }
        public DateTime ExpirationTime { get; set; }
        public Instrument Instrument { get; set; }
        public ProductType ProductType { get; set; }
        public int MagicNumber { get; set; }
    }
}
