﻿using PTLRuntime.NETScript;
using PTLRuntime.NETScript.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoGlauNamespace.SDKEncapsulation
{
      public interface IInstruments
    {
          Instrument Current { get; }

          event Action<Instrument, Quote> NewQuote;
          event Action<Instrument> InstrumentAdded;
          event Action<Instrument, Level2> NewLevel2;
          event Action<Instrument, Trade> NewTrade;

          Instrument GetInstrument(string symbol, string route);
          Instrument GetInstrument(string symbol, string route, DateTime expirationDate);
          Instrument GetInstrument(string name);        
          Task<Instrument> GetInstrumentAsync(string symbol, string route);        
          Task<Instrument> GetInstrumentAsync(string symbol, string route, DateTime expirationDate);        
          Task<Instrument> GetInstrumentAsync(string name);
          Instrument[] GetInstruments();
          List<Instrument> GetOptionInstruments(OptionSeries series);        
          Task<List<Instrument>> GetOptionInstrumentsAsync(OptionSeries series);
          List<OptionSeries> GetOptionSeries(Instrument underlier);        
          Task<List<OptionSeries>> GetOptionSeriesAsync(Instrument underlier);
          string[] GetSortedInstrumentNames(bool onlyTradable = false);
          Instrument[] GetSortedInstruments();
          bool Subscribe(Instrument instrument, QuoteTypes types);
          bool Unsubscribe(Instrument instrument, QuoteTypes types);
    }
}
