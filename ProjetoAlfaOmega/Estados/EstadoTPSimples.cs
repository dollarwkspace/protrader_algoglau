﻿using System;
using PTLRuntime.NETScript;

namespace AlgoGlauNamespace
{

    public class EstadoTPSimples : Estado
    {
        private ProjectAlgoGlau robo;
        public EstadoTPSimples(ProjectAlgoGlau _robo) => robo = _robo; //CONSTRUTOR DA CLASSE

        //MÉTODOS PÚBLICOS
        public override void Run()
        {
            try
            {
                string log = "********************   Estado TPSL Simples ********************";
                robo.Print(log);

                //Variáveis locais


                //CONDIÇÕES DE SAÍDA                
                if (robo.mainControl.timelineController.PosicaoMedia.Amount == 0)
                {
                    robo.estado = robo.mainControl.GetEstadoEntrada();
                    robo.mainControl.ordersController.ClearOrders();
                    return;
                }

                //INFO TELA                                


                //Atualização de variáveis


                //BLOQUEIO DE EXECUÇÃO

                robo.mainControl.PrintInnerStateEvents();
                if (robo.mainControl.GetInnerStatesBuffer().Count > 0) return;

                //CONDIÇÕES DE ENTRADA

                if (robo.mainControl.ordersController.orders.Length == 0)
                {
                    if (robo.comando.CriaParDeSaida() != "-1") robo.Print("Ordem TP enviada!");
                    if (robo.comando.CriaSaidaStopLoss() != "-1") robo.Print("Ordem SL enviada!");                    
                }
                

                //INFO final
                               

            }
            catch (Exception ex)
            {
                robo.Print($"Estado Inicio: Erro {ex.Message} {ex.InnerException.Message}");
            }

        }
    }
}

