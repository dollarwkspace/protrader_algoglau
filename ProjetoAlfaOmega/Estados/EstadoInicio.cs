﻿using System;
using PTLRuntime.NETScript;

namespace AlgoGlauNamespace
{
    public abstract class Estado
    {
        public abstract void Run();
    }

    public class EstadoInicio : Estado
    {
        private ProjectAlgoGlau robo;
        public EstadoInicio(ProjectAlgoGlau _robo) => robo = _robo; //CONSTRUTOR DA CLASSE

        private bool autorizaOperacaoVenda = false;
        private bool autorizaOperacaoCompra = false;
        
        private double GetDifMediasCurtissima() => robo.GetDifMediasCurtissima();
        private double GetDifMediasCurta() => robo.GetDifMediasCurta();
        private double GetDifMediasLonga() => robo.GetDifMediasLonga();
        private Operation GetTendenciaCurtissima() => GetDifMediasCurtissima() > 0 ? Operation.Buy : Operation.Sell;
        private Operation GetTendenciaCurta() => GetDifMediasCurta() > 0 ? Operation.Buy : Operation.Sell;
        private Operation GetTendenciaLonga() => (GetDifMediasLonga() > 0) ? Operation.Buy : Operation.Sell;
        private bool AbaixoMediaLonga() => robo.lastQuote.Last < robo.mLonga;
        private bool AcimaMediaLonga() => robo.lastQuote.Last > robo.mLonga;
        private bool AbaixoMediaCurta() => robo.lastQuote.Last < robo.mCurta;
        private bool AcimaMediaCurta() => robo.lastQuote.Last > robo.mCurta;

        private void ChecaHabilitacaoDeVenda()
        {
            if (AcimaMediaCurta() && AbaixoMediaLonga()) autorizaOperacaoVenda = true;
        }
        private void ChecaHabilitacaoDeCompra()
        {
            if (AbaixoMediaCurta() && AcimaMediaLonga()) autorizaOperacaoCompra = true;
        }
        private void ChecaHabilitacaoCompraVenda()
        {
            if (robo.lastQuote.Last > robo.mLonga) ChecaHabilitacaoDeCompra();
            if (robo.lastQuote.Last < robo.mLonga) ChecaHabilitacaoDeVenda();
        }

        private bool PodeEntrarVendendo() => autorizaOperacaoVenda && GetTendenciaCurtissima() == Operation.Sell && robo.lastQuote.Last < robo.mCurta;
        private bool PodeEntrarComprando() => autorizaOperacaoCompra && GetTendenciaCurtissima() == Operation.Buy && robo.lastQuote.Last > robo.mCurta;


        //MÉTODOS PÚBLICOS
        public override void Run()
        {
            try
            {
                string log = "********************   Estado Início ********************";
                robo.Print(log);

                //Variáveis locais
                


                //CONDIÇÕES DE SAÍDA                
                if (robo.mainControl.timelineController.PosicaoMedia.Amount != 0)
                {
                    robo.estado = robo.mainControl.GetEstadoControle();
                    return;
                }

                //INFO TELA                                
                robo.Print($"Tendência mCurtissima: {GetTendenciaCurtissima()}  Tendência mCurta: {GetTendenciaCurta()}  Tendência mLonga: {GetTendenciaLonga()}");
                robo.Print($"Compra habilitada ? {autorizaOperacaoCompra}");
                robo.Print($"Venda habilitada ? {autorizaOperacaoVenda}");
                
                //Atualização de variáveis


                //BLOQUEIO DE EXECUÇÃO

                robo.mainControl.PrintInnerStateEvents();
                if (robo.mainControl.GetInnerStatesBuffer().Count > 0) return;

                //CONDIÇÕES DE ENTRADA
                ChecaHabilitacaoCompraVenda();

                if(PodeEntrarComprando())
                {
                    if(robo.comando.Send_order(OrdersType.Market,Operation.Buy,0,robo.mainControl.timelineController.Contratos)!="-1")
                    {
                        robo.Print("Ordem de Compra enviada!");
                        autorizaOperacaoCompra = false;
                        autorizaOperacaoVenda = false;
                        //robo.estado = State.trailingStop;
                        return;
                    }
                    
                }

                if(PodeEntrarVendendo())
                {
                    if (robo.comando.Send_order(OrdersType.Market, Operation.Sell, 0, robo.mainControl.timelineController.Contratos) != "-1")
                    {
                        robo.Print("Ordem de Venda enviada!");
                        autorizaOperacaoCompra = false;
                        autorizaOperacaoVenda = false;
                        //robo.estado = State.trailingStop;
                        return;
                    }
                }

                //INFO final




            }
            catch (Exception ex)
            {
                robo.Print($"Estado Inicio: Erro {ex.Message} {ex.InnerException.Message}");
            }

        }
    }
}

