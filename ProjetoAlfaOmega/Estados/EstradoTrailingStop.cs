﻿using PTLRuntime.NETScript;
using System;
using System.Timers;
using System.Linq;
using AlgoGlauNamespace.SDKEncapsulation;

namespace AlgoGlauNamespace
{
    public class TrailingStop : Estado
    {
        private ProjectAlgoGlau robo;
        public Timer syncTimer;
        public TrailingStop(ProjectAlgoGlau _robo) { robo = _robo; SetSyncTimer(); }

        private TimeSpan timerLocal;

        //MÉTODOS PRIVADOS

        private double DeltaPosicaoMedia() { try { return robo.lastQuote.Last - robo.mainControl.timelineController.PosicaoMedia.OpenPrice ?? 0; } catch (Exception ex) { return 0; } }
        private bool AtivarTrailingStop() { try { return (robo.mainControl.timelineController.PosicaoMedia?.Side == Operation.Buy) ? DeltaPosicaoMedia() >= robo.gainParaDisparo : DeltaPosicaoMedia() <= -robo.gainParaDisparo; } catch (Exception ex) { return false; } }
        private double PontoDeTrailingStop() { try { return robo.RoundPrice(robo.mainControl.timelineController.PosicaoMedia.OpenPrice ?? 0 + DeltaPosicaoMedia() * robo.posicaoPercentualTrailingStop / 100); } catch (Exception ex) { return 0; } }
        //private bool CondicaoReEntradaBuyAtingida(PositionWrapper pos, double delta) { try { return robo.mainControl.timelineController.PosicaoMedia.Amount != 0 && pos.Side == Operation.Buy && robo.cotacaoAtual <= robo.mainControl.timelineController.PosicaoMedia.OpenPrice - delta; } catch (Exception ex) { return false; } }
        //private bool CondicaoReEntradaSellAtingida(PositionWrapper pos, double delta) { try { return robo.mainControl.timelineController.PosicaoMedia.Amount != 0 && pos.Side == Operation.Sell && robo.cotacaoAtual >= robo.mainControl.timelineController.PosicaoMedia.OpenPrice + delta; } catch (Exception ex) { return false; } }


        //MÉTODOS PÚBLICOS
        public override void Run()
        {
            try
            {
                string log = "********************   Estado Trailing Stop  ********************";
                robo.Print(log);

                //CONDIÇÕES DE SAÍDA
                if (robo.mainControl.timelineController.PosicaoMedia.Amount == 0)
                {
                    robo.estado = robo.mainControl.GetEstadoEntrada();
                    syncTimer.Enabled = false;
                    return;
                }

                //INFO TELA
                robo.Print($"Gain para Disparo: {robo.gainParaDisparo} , Posição % trailing Stop: {robo.posicaoPercentualTrailingStop}% ");
                robo.Print($"Trigger TRStop foi Atingido? {AtivarTrailingStop()} , Delta Posição Média: {DeltaPosicaoMedia()} , Ponto Trailing Stop: {PontoDeTrailingStop()}");
                if (robo.mainControl.ordersController.orders.Length > 0) robo.Print($"Ordem de Saída: {robo.mainControl.ordersController.orders[0].Price}");

                //CONDIÇÕES DE BLOQUEIO
                robo.mainControl.PrintInnerStateEvents();
                //if (robo.mainControl.GetInnerStatesBuffer().Count == 1 && robo.mainControl.GetInnerStatesBuffer().Exists(x => x == InnerState.termino)) goto Prossegue;
                if (robo.mainControl.GetInnerStatesBuffer().Count > 0) return;

                Prossegue:

                robo.mainControl.Update();
                if (robo.mainControl.ordersController.orders.Length == 0)
                {
                    if (robo.comando.CriaSaidaStopLoss() != "-1")
                    {
                        if (robo.myConnection.Status == PTLRuntime.NETScript.Application.ConnectionStatus.Disconnected) timerLocal = robo.lastQuote.Time.TimeOfDay;
                        if (robo.myConnection.Status == PTLRuntime.NETScript.Application.ConnectionStatus.Connected) syncTimer.Enabled = true;
                    }
                }

                if (EstouroDeTimerOffline()) SyncTimer_Elapsed();

                //PRINTS FINAIS

            }
            catch (Exception ex)
            {
                robo.Print($"Estado TrailingStop: Erro {ex.Message} {ex.InnerException.Message}");
            }

        }

        public void SetSyncTimer()
        {
            syncTimer = new Timer(robo.frequenciaDeAjusteTrailingStop * 1000);
            syncTimer.Elapsed += SyncTimer_Elapsed;
            syncTimer.AutoReset = true;
            syncTimer.Enabled = false;
        }

        private bool EstouroDeTimerOffline()
        {
            if (robo.myConnection.Status == PTLRuntime.NETScript.Application.ConnectionStatus.Disconnected)
            {
                if (robo.mainControl.LastQuote?.Time.TimeOfDay.Subtract(timerLocal).TotalSeconds > robo.frequenciaDeAjusteTrailingStop)
                {
                    timerLocal = robo.mainControl.LastQuote.Time.TimeOfDay;
                    return true;
                }
            }
            return false;
        }

        private void SyncTimer_Elapsed(object sender = null, ElapsedEventArgs e = null)
        {
            if (AtivarTrailingStop() && robo.mainControl.ordersController.orders.Length > 0 && robo.mainControl.timelineController.PosicaoMedia.Amount != 0)
            {
                bool autorizaMudanca = (robo.mainControl.timelineController.PosicaoMedia?.Side == Operation.Buy) ? PontoDeTrailingStop() > robo.mainControl.ordersController.orders[0]?.Price : PontoDeTrailingStop() < robo.mainControl.ordersController.orders[0]?.Price;
                if (autorizaMudanca)
                {
                    robo.mainControl.AddToInnerStatesBuffer(InnerState.equalizandoOrdem);
                    bool result = robo.mainControl.ordersController.orders[0].Modify(PontoDeTrailingStop());
                    robo.mainControl.RemoveFromInnerStatesBuffer(InnerState.equalizandoOrdem);
                }
            }
        }
    }
}

