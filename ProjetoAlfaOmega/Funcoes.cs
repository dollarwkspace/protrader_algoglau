﻿using System;
using PTLRuntime.NETScript;
using System.Linq;
using PTLRuntime.NETScript.Application;
using System.IO;
using System.Runtime.Serialization;
using System.Collections.Generic;
using AlgoGlauNamespace.SDKEncapsulation;
using AlgoGlauNamespace.Timeline;

namespace AlgoGlauNamespace
{

    public enum State { inicio, reentrada, trailingStop, TPSL, nulo };
    public enum TipoDeEntradaInicial { simples , Manual}
    public enum TipoDeOperacaoPosPosicao { reEntradaSimples, TrailingStop, TPSLSimples, Manual}

    public enum InnerState
    {
        lockdownPorOrdemEnviada, lockdownFinanceiro, lockdownPorStopGain,
        lockDownPorMaxNumeroDeTrades, lockdownSaidaPorLossMaximo, lockdownSemIndicadores,
        lockdownIndicadorZerado, lockdownSemCotacao, lockdownPorBolsaFechada, lockdownPorCalorMaximo,
        ordemDeEntradaFoiSolicitada, ordemDeSaidaFoiSolicitada, equalizandoOrdem,
        umaOrdemFoiRecebida, termino, bolsaFechada, ativo, lockdownPorMaximoNumeroDeContratosAtingidos
    }


    public enum Trend { Up = 1, Nenhuma = 0, Down = -1 }
    public enum TipoDeSaida { DifMédias, LossMaximo };
    public enum TipoDeRobo { tendencia }
    public enum TipoDeOrdem { limite, mercado };
    public enum TipoDeBloqueio { BUY, SELL, NULL };
    public enum TipoDeExecucao { OnTrade, OnQuote };
    public enum TipoDeTP { fixo, dinamico };

    public enum TipoStopSeguranca { PreçoMedio, MédiaLonga }

    public class LogWriter
    {
        private string m_exePath = string.Empty;

        public static void LogWrite(string logMessage, string arq = "C:\\robo\\log.txt", bool novo = true)
        {
            //m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            if (File.Exists(arq) && novo) File.Delete(arq);

            if (!File.Exists(arq))
            {
                //string s = ""; s = DateTime.Now.ToString("h:mm:ss tt");
                FileStream fs = File.Create(arq);
                //Byte[] info = new UTF8Encoding(true).GetBytes("File Created:" + s + "\r\n");
                //fs.Write(info, 0, info.Length);
                fs.Close();
            }

            try
            {
                using (StreamWriter w = File.AppendText(arq))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.WriteLine("{0}", logMessage);
                //txtWriter.WriteLine("-------------------------------");
            }
            catch (Exception ex)
            {
            }
        }
    }



    public class Funcoes
    {
        private ProjectAlgoGlau robo;
        private string log;

        public Funcoes(ProjectAlgoGlau _robo) { robo = _robo; log = robo.log; }


        public void InfoConexao(Connection myConnection)
        {
            //outputting of all the information about the current connection.
            robo.Print(
            "\nConnection name : \t" + myConnection.Name +
            "\nConnection address : \t" + myConnection.Address +
            "\nConnection login : \t" + myConnection.Login +
            "\nSLL on?  : \t" + myConnection.IsUseSSL +
            "\nProtocol version : \t" + myConnection.ProtocolVersion +
            "\nIs HTTP connection: \t" + myConnection.IsHTTPConnection +
            "\nConnection status : \t" + myConnection.Status
            );
        }

        public void AccountInformation(Account acc)
        {
            //outputting of all the 'Account' properties
            robo.Print(
                "Id : \t" + acc.Id + "\n" +
                "Name : \t" + acc.Name + "\n" +
                "User Login: \t" + acc.User.Login + "\n" +
                "Balance : \t" + acc.Balance + "\n" +
                "Leverage : \t" + acc.Leverage + "\n" +
                "Currency : \t" + acc.Currency + "\n" +
                "IsMasterAccount : \t" + acc.IsMasterAccount + "\n" +
                "IsDemo : \t" + acc.IsDemo + "\n" +
                "IsReal : \t" + acc.IsReal + "\n" +
                "IsLocked : \t" + acc.IsLocked + "\n" +
                "IsInvestor : \t" + acc.IsInvestor + "\n" +
                "Status : \t" + acc.Status + "\n" +
                "StopReason : \t" + acc.StopReason + "\n" +
                "GetStatusText : \t" + acc.GetStatusText() + "\n" +
                "Balance : \t" + acc.Balance + "\n" +
                "BeginBalance : \t" + acc.BeginBalance + "\n" +
                "BlockedBalance : \t" + acc.BlockedBalance + "\n" +
                "ReservedBalance : \t" + acc.ReservedBalance + "\n" +
                "InvestedFundCapital : \t" + acc.InvestedFundCapital + "\n" +
                "Credit : \t" + acc.Credit + "\n" +
                "CashBalance : \t" + acc.CashBalance + "\n" +
                "TodayVolume : \t" + acc.TodayVolume + "\n" +
                "TodayNet : \t" + acc.TodayNet + "\n" +
                "TodayTrades : \t" + acc.TodayTrades + "\n" +
                "TodayFees : \t" + acc.TodayFees + "\n" +
                "MarginForOrders : \t" + acc.MarginForOrders + "\n" +
                "MarginForPositions : \t" + acc.MarginForPositions + "\n" +
                "MarginTotal : \t" + acc.MarginTotal + "\n" +
                "MarginAvailable : \t" + acc.MarginAvailable + "\n" +
                "MaintanceMargin : \t" + acc.MaintanceMargin + "\n" +
                "MarginDeficiency : \t" + acc.MarginDeficiency + "\n" +
                "MarginSurplus : \t" + acc.MarginSurplus + "\n" +
                "CurrentPammCapital : \t" + acc.CurrentPammCapital + "\n" +
                "Equity : \t" + acc.Equity + "\n" +
                "OpenOrdersAmount : \t" + acc.OpenOrdersAmount + "\n" +
                "OpenPositionsAmount : \t" + acc.OpenPositionsAmount + "\n" +
                "OpenPositionsExposition : \t" + acc.OpenPositionsExposition + "\n" +
                "OpenPositionsCount : \t" + acc.OpenPositionsCount + "\n" +
                "OpenOrdersCount : \t" + acc.OpenOrdersCount + "\n"
            );
        }

        public void PosInfo(Position pos)
        {
            log =
                "PosInfo()-> " +
                "  Papel: " + pos.Instrument.BaseName + " / " +
                //"  Id: " + pos.Id + " / " +
                "  Qtde: " + pos.Amount + " / " +
                "  Tipo: " + pos.Side.ToString() + " / " +
                "  Preço Abertura: R$ " + pos.OpenPrice + " / " +
                "  Preço Atual: R$ " + pos.CurrentPrice + " / " +
                "  Número Mágico: " + pos.MagicNumber + " / ";
            //"  Lucro ou Prejuízo: R$" + pos.GetProfitNet();
            robo.Print(log);
        }

        public void OrderInfo(Order ord)
        {
            log = "OrderInfo()-> " +
                "  Papel: " + ord.Instrument.BaseName + " / " +
                //"  Id: " + ord.Id + " / " +
                "  Qtde: " + ord.Amount + " / " +
                "  Lado: " + ord.Side + " / " +
                "  Tipo: " + ord.Type + " / " +
                "  Preço: R$ " + ord.Price + " / " +
                "  Status: " + ord.Status + " / " +
                "  Número Mágico: " + ord.MagicNumber + " / " +
                "  Ativa: " + ord.IsActive;
            robo.Print(log);
        }


        /*public virtual bool EqualizaOrdemPosicao(PositionWrapper pos, OrderWrapper ord)
        {
            try
            {
                if (ord is null || ord?.Amount == 0) { return false; }
                if (pos is null || pos?.Amount == 0 || pos.Amount == null) { return false; }
                double price = 0;
                price = (pos.Side == Operation.Buy) ? robo.RoundPrice((pos.OpenPrice ?? 0) + robo.TP * robo.GetTickSize()) : robo.RoundPrice((pos.OpenPrice ?? 0) - robo.TP * robo.GetTickSize());
                log = "EqualizaOrdemPosição: Qtde Pos: " + pos.Amount + "\t Qtde Ordem: " + ord.Amount + "\t Preço correto:" + price + "\t Preço da Ordem: " + ord.Price + "\t TickSize: " + robo.GetTickSize();
                robo.Print(log);

                if (Math.Abs(pos.Amount ?? 0) != Math.Abs(ord.Amount))
                {
                    log = robo.horario + "EqualizaOrdemPosição: Ordem com diferença de quantidade em relação à posição. Equalizando...\t Preço correto: " + price + "\t Preço da Ordem: " + ord.Price + "\t Tp x TickSize: " + robo.TP * robo.GetTickSize();
                    robo.Print(log);
                    return ModificaOrdem(ord, price, Math.Abs(pos.Amount ?? 0));
                }

                if (Math.Abs(ord.Price - price) > robo.GetTickSize())
                {
                    log = robo.horario + "EqualizaOrdemPosição: Ordem com preço de saída incorreto. Equalizando...\t Preço correto: " + price + "\t Preço da Ordem: " + ord.Price + "\t Tp x TickSize: " + robo.TP * robo.GetTickSize();
                    robo.Print(log);
                    return ModificaOrdem(ord, price);
                }
                return false;
            }
            catch (Exception exc)
            {
                robo.Print("Erro EqualizaOrdemPosição");
                return false;
            }

        }*/

        public bool ModificaOrdem(OrderWrapper ord, double preco, double qtde = 0)
        {
            robo.mainControl.AddToInnerStatesBuffer(InnerState.equalizandoOrdem);
            bool result = false;

            if (preco != 0 && qtde != 0)
            {
                result = ord.Modify(preco, Math.Abs(qtde));
                if (result == false) robo.mainControl.RemoveFromInnerStatesBuffer(InnerState.equalizandoOrdem);
                return result;
            }
            if (preco != 0)
            {
                result = ord.Modify(preco);
                if (result == false) robo.mainControl.RemoveFromInnerStatesBuffer(InnerState.equalizandoOrdem);
                return result;
            }

            robo.mainControl.RemoveFromInnerStatesBuffer(InnerState.equalizandoOrdem);
            return false;

        }

        public void PrintaPosicoesOrdensAtivas()
        {
            Position[] posicoesGlobais = robo.Positions.GetPositions();
            Order[] ordensGlobais = robo.Orders.GetOrders(true);
            if (posicoesGlobais.Length > 0) { posicoesGlobais?.ToList().ForEach(pos => PosInfo(pos)); }
            if (ordensGlobais.Length > 0) { ordensGlobais?.ToList().ForEach(ord => OrderInfo(ord)); }
        }


        public bool RefreshIndicators()
        {
            try
            {
                robo.mCurtissima = robo.MM?.GetValue(0, 0) ?? 0;
                robo.mCurtissimaAnterior = robo.MM?.GetValue(0, 1) ?? 0;
                robo.mCurta = robo.MM?.GetValue(1, 0) ?? 0;
                robo.mCurtaAnterior = robo.MM?.GetValue(1, 1) ?? 0;
                robo.mLonga = robo.MM?.GetValue(2, 0) ?? 0;
                robo.mLongaAnterior = robo.MM?.GetValue(2, 1) ?? 0;
                robo.Print($"MCurtissima:{Math.Round(robo.mCurtissima, 2)}  MCurta:{Math.Round(robo.mCurta, 2)}  MLonga:{Math.Round(robo.mLonga, 2)} Cotação:{Math.Round(robo.cotacaoAtual, 2)}");
                return true;
            }
            catch (Exception exc)
            {
                robo.Print("RefreshIndicators: Erro na coleta dos indicadores e/ou posicoes e ordens . Erro: " + exc.Message);
                robo.Print("RefreshIndicators: InnerException " + exc.InnerException.Message);
                return false;
            }
        }


    }
}
