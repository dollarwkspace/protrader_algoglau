﻿using AlgoGlauNamespace.SDKEncapsulation;
using AlgoGlauNamespace.Timeline;
using PTLRuntime.NETScript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace AlgoGlauNamespace
{
    public class AppControl
    {
        private ProjectAlgoGlau robo;
        private IOrderRepository orderRepository;
        public OrdersController ordersController;
        public TimelineController timelineController;

        public AppControl(ProjectAlgoGlau robo, IOrderRepository orderRepo = null)
        {
            this.robo = robo;
            orderRepository = orderRepo ?? new OrderRepository(robo);
            ordersController = new OrdersController(orderRepository);
            timelineController = new TimelineController(robo);

            ordersController.OrderAddedEvent += (OrderWrapper ord) => { OnOrderAdded(ord); };
            ordersController.OrderExecutedEvent += (OrderWrapper ord) =>
            {
                timelineController.AddToTimeline(ord);
                OnOrderExecuted(ord);
            };
            ordersController.OrderRemovedEvent += (OrderWrapper ord) => OnOrderRemoved(ord);
            ordersController.OrderEmptyEvent += () => OnOrderEmpty();


            timelineController.OpenOrderRequestedEvent += (double Amount, Operation Side) =>
            {
                if (robo.comando.EnviaOrdem(Side, 0, Amount, OrdersType.Market) != "-1")
                {
                    while (GetInnerStatesBuffer().Exists(x => x == InnerState.lockdownPorOrdemEnviada)) { };
                }
            };

            timelineController.TimelineFechouEvent += () =>
            {
                RemoveFromInnerStatesBuffer(InnerState.lockdownPorMaximoNumeroDeContratosAtingidos);
            };

        }

        private double _pontosDoMedio = 0;
        private bool _bolsaAberta, _horarioTerminou;

        private List<InnerState> _innerStatesBuffer = new List<InnerState>();

        public virtual void AddToInnerStatesBuffer(InnerState valor) { if (!_innerStatesBuffer.Exists(x => x == valor)) _innerStatesBuffer.Add(valor); robo.innerState = valor; }
        public virtual void RemoveFromInnerStatesBuffer(InnerState valor)
        {
            if (_innerStatesBuffer.Exists(x => x == valor)) _innerStatesBuffer.Remove(_innerStatesBuffer.Find(x => x == valor));
        }

        public void ResetInnerState() => _innerStatesBuffer.Clear();
        public void PrintInnerStateEvents()
        {
            if (_innerStatesBuffer.Count > 0)
            {
                _innerStatesBuffer.ForEach(x =>
                {
                    switch (x)
                    {
                        case InnerState.bolsaFechada: { robo.Print(robo.horario + " BOLSA FECHADA "); break; }
                        case InnerState.lockdownFinanceiro: { robo.Print($"Lockdown por StopLossFinanceiro : R${robo.calor}. Data: {robo.mainControl.LastQuote?.Time.ToLocalTime().ToShortDateString()}"); break; }
                        case InnerState.lockdownIndicadorZerado: { robo.Print("Lockdown por média está zerada... aguarde"); break; }
                        case InnerState.lockdownPorBolsaFechada: { robo.Print("Lockdown por Bolsa Fechada!"); break; }
                        case InnerState.lockDownPorMaxNumeroDeTrades: { robo.Print("Lockdown por Máximo número de Trades! Aguardando próximo dia."); break; }
                        case InnerState.lockdownPorOrdemEnviada: { robo.Print("Lockdown por Ordem Enviada!"); break; }
                        case InnerState.lockdownPorStopGain: { robo.Print($"Lockdown por StopGainFinanceiro : R${robo.lucroDia}"); break; }
                        case InnerState.lockdownSaidaPorLossMaximo: { robo.Print("Lockdown Saída por Loss máximo!"); break; }
                        case InnerState.lockdownSemCotacao: { robo.Print("Lockdown por Cotação zerada!"); break; }
                        case InnerState.lockdownSemIndicadores: { robo.Print("Lockdown sem indicadores de Sinal!"); break; }
                        case InnerState.termino: { robo.Print("Lockdown por Horário de Término atingido... "); break; }
                        case InnerState.ordemDeEntradaFoiSolicitada: { robo.Print("Lockdown por ordem de Entrada foi solicitada..."); break; }
                        case InnerState.ordemDeSaidaFoiSolicitada: { robo.Print("Lockdown por ordem de Saída foi solicitada..."); break; }
                        case InnerState.lockdownPorCalorMaximo: { robo.Print("Lockdown por Calor Máximo"); break; }
                        case InnerState.lockdownPorMaximoNumeroDeContratosAtingidos: { robo.Print("Lockdown por Máx nº Contratos abertos"); break; }
                    }
                });
            }
        }

        public List<InnerState> GetInnerStatesBuffer() => _innerStatesBuffer;

        private DayOfWeek _currentDay = DayOfWeek.Sunday;
        private TimeSpan _currentHour;
        public DayOfWeek currentDay
        {
            get { return _currentDay; }
            private set { if (_currentDay != value && _currentDay != DayOfWeek.Sunday && _currentDay != DayOfWeek.Saturday) { _currentDay = value; OnDayChanged(); } _currentDay = value; }
        }
        public TimeSpan CurrentHour
        {
            get { return _currentHour; }
            set
            {
                _currentHour = value;
                robo.horario = value;
                if (robo.horario.Hours == 0) { robo.innerState = InnerState.lockdownSemCotacao; }
                if (robo.horario.CompareTo(robo.HoraDeInicio) < 0) { bolsaAberta = false; HorarioTerminou = true; }
                if (robo.horario.CompareTo(robo.HoraDeInicio) > 0 && robo.horario.CompareTo(robo.HoraDeTermino) < 0) { bolsaAberta = true; HorarioTerminou = false; }
                if (robo.horario.CompareTo(robo.HoraDeTermino) >= 0) { HorarioTerminou = true; }
                if (robo.horario.CompareTo(robo.HoraDeTerminoBolsa) >= 0) { bolsaAberta = false; HorarioTerminou = true; }
            }
        }

        public bool bolsaAberta
        {
            get { return _bolsaAberta; }
            private set
            {
                if (_bolsaAberta != value)
                {
                    _bolsaAberta = value;
                    if (_bolsaAberta == false) { OnBolsaFechada(); }
                    if (_bolsaAberta == true) { OnBolsaAberta(); }
                }
            }
        }

        public bool HorarioTerminou
        {
            get { return _horarioTerminou; }
            private set
            {
                if (_horarioTerminou != value)
                {
                    _horarioTerminou = value;
                    robo.horarioTerminou = value;
                    if (value) { OnHorarioTerminou(); }
                    if (!value) RemoveFromInnerStatesBuffer(InnerState.termino);
                }

            }
        }

        private QuoteWrapper _lastQuote;
        public QuoteWrapper LastQuote
        {
            get => _lastQuote;
            set
            {
                _lastQuote = value;
                if (!(value is null))
                {
                    if (value.Time == DateTime.MinValue || value.Time == null) return;
                    if (!System.Diagnostics.Debugger.IsAttached)
                    {
                        CurrentHour = value.Time.ToLocalTime().TimeOfDay;
                        currentDay = value.Time.ToLocalTime().DayOfWeek;
                    }
                    else
                    {
                        CurrentHour = value.Time.TimeOfDay;
                        currentDay = value.Time.DayOfWeek;
                    }

                }
                if (timelineController.PosicaoMedia.Amount != 0)
                {
                    PontosDoMedio = Math.Abs((timelineController.PosicaoMedia.OpenPrice ?? 0) - value.Last);
                }
            }
        }

        public double PontosDoMedio
        {
            get { return _pontosDoMedio; }
            private set
            {
                _pontosDoMedio = value;
                if (value != 0)
                {
                    if (timelineController.PosicaoMedia.Amount != 0)
                    {
                        if (timelineController.PosicaoMedia.Side == Operation.Buy && _pontosDoMedio > robo.lossMaximo && _lastQuote.Last < timelineController.PosicaoMedia.OpenPrice) OnSaidaPorLossMaximo();
                        if (timelineController.PosicaoMedia.Side == Operation.Sell && _pontosDoMedio > robo.lossMaximo && _lastQuote.Last > timelineController.PosicaoMedia.OpenPrice) OnSaidaPorLossMaximo();
                    }
                }
            }
        }
        public event Action DayChangedEvent;
        public event Action<OrderWrapper> OrderAddedEvent;
        public event Action<OrderWrapper> OrderExecutedEvent;
        public event Action<OrderWrapper> OrderRemovedEvent;
        public event Action OrderEmptyEvent;
        public event Action BolsaAbriuEvent;
        public event Action BolsaFechouEvent;
        public event Action SaidaPorLossMaximoEvent;
        public event Action HorarioTerminouEvent;

        private void OnDayChanged()
        {
            ResetInnerState();
            timelineController.ResetTimeLineVarsDueToDayChanged();
            DayChangedEvent?.Invoke();
        }
        private void OnOrderAdded(OrderWrapper ord)
        {
            RemoveFromInnerStatesBuffer(InnerState.lockdownPorOrdemEnviada);
            OrderAddedEvent?.Invoke(ord);
        }
        private void OnOrderExecuted(OrderWrapper ord)
        {
            RemoveFromInnerStatesBuffer(InnerState.lockdownPorOrdemEnviada);
            OrderExecutedEvent?.Invoke(ord);
        }
        private void OnOrderRemoved(OrderWrapper ord)
        {
            RemoveFromInnerStatesBuffer(InnerState.lockdownPorOrdemEnviada);
            OrderRemovedEvent?.Invoke(ord);
        }
        private void OnOrderEmpty() => OrderEmptyEvent?.Invoke();
        private void OnBolsaAberta()
        {
            ResetInnerState();
            BolsaAbriuEvent?.Invoke();
        }
        private void OnBolsaFechada()
        {
            AddToInnerStatesBuffer(InnerState.bolsaFechada);
            timelineController.ForceTimelineToClose();
            BolsaFechouEvent?.Invoke();
        }
        private void OnSaidaPorLossMaximo()
        {
            AddToInnerStatesBuffer(InnerState.lockdownSaidaPorLossMaximo);
            timelineController.ForceTimelineToClose();
            SaidaPorLossMaximoEvent?.Invoke();
        }
        private void OnHorarioTerminou()
        {
            AddToInnerStatesBuffer(InnerState.termino);
            HorarioTerminouEvent?.Invoke();
        }


        public void Update()
        {
            try
            {
                RefreshQuotation();
                if (LastQuote.Last == 0) { AddToInnerStatesBuffer(InnerState.lockdownSemCotacao); return; } else { RemoveFromInnerStatesBuffer(InnerState.lockdownSemCotacao); }
                if (!robo.funcoes.RefreshIndicators()) { AddToInnerStatesBuffer(InnerState.lockdownSemIndicadores); return; } else { RemoveFromInnerStatesBuffer(InnerState.lockdownSemIndicadores); }
                if (robo.mCurta == 0 || robo.mLonga == 0) { AddToInnerStatesBuffer(InnerState.lockdownIndicadorZerado); return; } else { RemoveFromInnerStatesBuffer(InnerState.lockdownIndicadorZerado); }

                PrintInnerStateEvents();
            }
            catch (Exception ex)
            {
                robo.Print($"Erro Status Update: {ex.ToString()}");
            }
        }

        public State GetEstadoEntrada()
        {
            switch (robo.tipoDeLogicaInicial)
            {
                case TipoDeEntradaInicial.simples: { return State.inicio; }
                case TipoDeEntradaInicial.Manual: { return State.nulo; }
                default: { return State.nulo; }
            }

        }

        public State GetEstadoControle()
        {
            switch (robo.tipoDeLogicaControle)
            {
                case TipoDeOperacaoPosPosicao.reEntradaSimples: { return State.reentrada; }
                case TipoDeOperacaoPosPosicao.TrailingStop: { return State.trailingStop; }
                case TipoDeOperacaoPosPosicao.TPSLSimples: { return State.TPSL; }
                case TipoDeOperacaoPosPosicao.Manual: { return State.nulo; }
            }
            return State.trailingStop;
        }

        public void RefreshQuotation()
        {
            try
            {
                LastQuote = robo.lastQuote;
                robo.cotacaoAtual = LastQuote.Last;
                ordersController.UpdateOrders();
            }
            catch (Exception ex)
            {
                robo.Print("Erro RefreshQuotation");
                Debug.Print(ex.Message);

            }
        }
    }
}
