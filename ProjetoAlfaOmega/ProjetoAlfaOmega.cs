﻿using AlgoGlauNamespace.SDKEncapsulation;
using PTLRuntime.NETScript;
using PTLRuntime.NETScript.Application;
using PTLRuntime.NETScript.Charts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace AlgoGlauNamespace
{
    public class ProjectAlgoGlau : NETStrategy
    {
        public Account conta;
        public Indicator MM;
        public Indicator sniper;
        public Connection myConnection = Connection.CurrentConnection; //create connection object.            
        public Font font = new Font("Tahoma", 10);
        public Font fontAlarm = new Font("Tahoma", 10, FontStyle.Bold);
        public Brush brush = new SolidBrush(Color.White);
        public Brush brushAlarm = new SolidBrush(Color.Red);
        public Brush brushAlert = new SolidBrush(Color.Yellow);
        public Queue<Level2> level2 = new Queue<Level2>();
        public Funcoes funcoes = null;
        public Comando comando = null;
        public Eventos eventos = null;
        public AppControl mainControl = null;
        public TrailingStop trailingStop = null;
        public EstadoInicio estadoInicio = null;
        public EstadoTPSimples tpslSimples = null;
        //private EstadoReEntrada estadoReEntrada = null;        
        public QuoteWrapper lastQuote;
        public TimeSpan horario;
        public Stopwatch watch = new Stopwatch();
        public List<string> logDin = new List<string>();

        public bool onQuoteWatcher = false;
        public State estado = State.nulo;
        public State nextState = State.nulo;
        public InnerState innerState = InnerState.ativo;
        public List<string> parametros = new List<string>();
        public List<string> comandosTela = new List<string>();
        public System.Timers.Timer syncTimer, debouncer;

        public string log;
        public double calor = 0;
        public double mCurtissima = 0;
        public double mCurtissimaAnterior = 0;
        public double mCurta = 0;
        public double mCurtaAnterior = 0;
        public double cotacaoAtual;
        public double mLonga = 0;
        public double mLongaAnterior = 0;
        public int hora = 0;
        public int contadorDeReEntradas = 0;
        public double[] ReEntradas;
        public double[] ValoresDeReEntradas;
        public string horaDoCalorMaximo = "";
        public string papel;
        public double maximoDeContratosAbertos = 0;
        public int contadorOrdemTravada = 0;
        public long ms = 0;
        public long maxMs = 0;
        public long minMs = 10000;
        public bool horarioTerminou = false;
        public bool saidaPorLossMaximo = false;
        public bool roboTerminouInicializacao = false;
        public bool lockdownPorStopGain = false;
        public bool onDebounce = false;
        public double proximaReEntrada = 0;
        public double calorPorTimeline = 0;
        public double lucroDia = 0;
        public double lossDia = 0;
        public double lucroSeFechamentoAtual = 0;
        public bool atualizouLossDia = false;

        public ProjectAlgoGlau()
            : base()
        {
            #region Initialization
            base.Author = "Camilo Chaves";
            base.Comments = "Projeto AlgoDollar";
            base.Company = "Dollar Investimentos";
            base.Copyrights = "Dollar Investimentos";
            base.DateOfCreation = "06/12/2019";
            base.ExpirationDate = 0;
            base.Version = "0.0.1";
            base.Password = "";
            base.ProjectName = "AlgoDollar";
            #endregion

            eventos = new Eventos(this);
            comando = new Comando(this);
            funcoes = new Funcoes(this);
            estadoInicio = new EstadoInicio(this);
            //estadoReEntrada = new EstadoReEntrada(this);
            tpslSimples = new EstadoTPSimples(this);
            trailingStop = new TrailingStop(this);
            mainControl = new AppControl(this);
            lastQuote = new QuoteWrapper(this);
        }

        //[InputParameter("Nome Conj. Parâmetros", 0)]
        public string nomeParam = "";
        [InputParameter("Início(hora)", 1)]
        public TimeSpan HoraDeInicio = new TimeSpan(9, 0, 0);
        [InputParameter("Término(hora)", 2)]
        public TimeSpan HoraDeTermino = new TimeSpan(17, 00, 00);
        [InputParameter("Término Bolsa(hora)", 3)]
        public TimeSpan HoraDeTerminoBolsa = new TimeSpan(17, 55, 00);
        [InputParameter("INDICADORES", 4)]
        public string texto1 = "_____________________________________________";
        [InputParameter("Média: Curtíssima", 5)]
        public int periodoMMovelCurtissima = 7;
        [InputParameter("Média: Curta", 6)]
        public int periodoMMovelCurta = 20;
        [InputParameter("Média: Longa", 6)]
        public int periodoMMovelLonga = 60;


        [InputParameter("OPERAÇÃO", 13)]
        public string texto2 = "_____________________________________________";
        [InputParameter("Máximo de Contratos ", 14)]
        public double maxPosAbertas = 80;
        [InputParameter("Contratos Iniciais", 15)]
        public int contratosIniciais = 5;
        //[InputParameter("Array Pontos de Reentradas", 16)]
        public string pontosDeReEntradas = "5,25,30,40";
        //[InputParameter("Array Valores de Reentradas", 17)]
        public string valoresDeReentrada = "5,10,20,40";
        [InputParameter("Stop Loss Financeiro", 18)]
        public int stopLossFinanceiro = 5000;
        [InputParameter("Stop por Calor Máximo", 19)]
        public int stopPorCalorMaximo = 5000;
        [InputParameter("Stop Gain Financeiro (Rend. Máximo)", 20)]
        public int stopGainFinanceiro = 5000;
        [InputParameter("Take Profit (ticks)", 21, 0, 20, 2, 0.01)]
        public double TP = 1;
        [InputParameter("Stop Loss (pts)", 22, 0, 20, 2, 0.01)]
        public double SL = 4;
        [InputParameter("Loss máximo em Pts", 23)]
        public int lossMaximo = 50;
        //[InputParameter("Dist em Pts Media Base (Curta)", 24)]
        //public int distPtsMediaBaseCurta = 2;
        [InputParameter("Multiplicador da Dif.Das Médias", 25, 0, 1000, 3, 0.001)]
        public double multiplicador = 10;
        [InputParameter("Market Range", 29)]
        public int marketRange = 10;
        [InputParameter("Custo de 1 contrato por ponto", 30)]
        public double custoContratoPorPonto = 25;

        [InputParameter("Habilita Trailing Stop?", 35)]
        public bool habilitaTrailingStop = true;
        [InputParameter("Gain para Disparo", 36, 0, 100, 3, 0.001)]
        public double gainParaDisparo = 2;
        [InputParameter("Posição% (última cotação e pos. média)", 37)]
        public int posicaoPercentualTrailingStop = 60;
        [InputParameter("Timer de Ajuste TRStop(s) ", 38)]
        public int frequenciaDeAjusteTrailingStop = 1;
        //[InputParameter("Habilitar Trailing de Ganho?", 42)]
        //public bool habilitouTrailingDeGanho = false;        

        [InputParameter("COMPORTAMENTO", 50)]
        public string texto3 = "_____________________________________________";

        /*[InputParameter("Respeitar Média Longa ? ", 51)]
        public bool respeitaMediaLonga = true;
        [InputParameter("Usar média entrada/reEntrada(laranja) em todos os trades? ", 52)]
        public bool usaMEntradaLaranjaEmTodosOsTrades = true;*/


        /*[InputParameter("Tipo de Robô", 51, new Object[]
     {"Tendência", TipoDeRobo.tendencia, "Contra Tendência",TipoDeRobo.contraTendencia})]*/
        public TipoDeRobo tipoDeRobo = TipoDeRobo.tendencia;
        /*[InputParameter("Tipo de Saída", 52, new Object[]
     {"Dif Médias Sinal", TipoDeSaida.DifMédias, "Loss Máximo",TipoDeSaida.LossMaximo })]*/
        public TipoDeSaida tipoDeSaida = TipoDeSaida.LossMaximo;
        [InputParameter("Tipo de BLOQUEIO", 54, new Object[]
{"BUY", TipoDeBloqueio.BUY, "SELL",TipoDeBloqueio.SELL, "Nenhum", TipoDeBloqueio.NULL })]
        public TipoDeBloqueio tipoDeBloqueio = TipoDeBloqueio.NULL;
        [InputParameter("Mostrar toda Timeline? ", 55)]
        public bool showAllTimeline = false;
        [InputParameter("Tipo de Lógica de Entrada", 56, new Object[]
            {"Início Simples", TipoDeEntradaInicial.simples, "Entrada Manual", TipoDeEntradaInicial.Manual})]
        public TipoDeEntradaInicial tipoDeLogicaInicial = TipoDeEntradaInicial.Manual;
        [InputParameter("Tipo de Lógica de Controle", 57, new Object[] 
        { "TrailingStop", TipoDeOperacaoPosPosicao.TrailingStop, "TPSL Simples", TipoDeOperacaoPosPosicao.TPSLSimples,
            "Manual", TipoDeOperacaoPosPosicao.Manual})]
        public TipoDeOperacaoPosPosicao tipoDeLogicaControle = TipoDeOperacaoPosPosicao.Manual;

        [InputParameter("Habilita Controle pelo Mouse? ", 58)]
        public bool habilitarComandosPeloMouse = true;

        [InputParameter("Mouse BUY", 59, new Object[] {
            "Esquerdo", MouseButtons.Left, "Meio", MouseButtons.Middle, "Direito", MouseButtons.Right, "X1", MouseButtons.XButton1, "X2", MouseButtons.XButton2})]
        public MouseButtons buttonBuy = MouseButtons.Left;

        [InputParameter("Mouse SELL", 60, new Object[] {
            "Esquerdo", MouseButtons.Left, "Meio", MouseButtons.Middle, "Direito", MouseButtons.Right, "X1", MouseButtons.XButton1, "X2", MouseButtons.XButton2})]
        public MouseButtons buttonSell = MouseButtons.Middle;

        [InputParameter("Mouse Saída", 61, new Object[] {
            "Esquerdo", MouseButtons.Left, "Meio", MouseButtons.Middle, "Direito", MouseButtons.Right, "X1", MouseButtons.XButton1, "X2", MouseButtons.XButton2})]
        public MouseButtons buttonSaida = MouseButtons.XButton1;

        [InputParameter("LOG BLOCK", 62)]
        public bool blockLog = true;

        [InputParameter("Número do Robô ", 57)]
        public int numeroRobo = 0;

        public TipoDeTP tipoDeTP = TipoDeTP.fixo;
        public override void Init()
        {
            //******************************
            //**** INICIALIZAÇÃO DO ROBÔ ***
            //******************************
            try
            {
                Print("Nome do Instrumento ativo: " + Instruments.Current.Name);
                papel = Instruments.Current.BaseName;
                Print("Nome BASE do Instrumento ativo: " + papel);
                conta = Accounts.Current;
                Instruments.Subscribe(Instruments.Current, QuoteTypes.Trade | QuoteTypes.Quote | QuoteTypes.Level2);

                Print("Papel: " + Instruments.Current.Name);
                Print("Tamanho do Point: " + Point);
                Print("Tamanho do tick: " + Instruments.Current.TickSize);

                Orders.OrderAdded += Orders_OrderAdded;
                Orders.OrderExecuted += Orders_OrderExecuted;
                Orders.OrderRemoved += Orders_OrderRemoved;
                cotacaoAtual = lastQuote.Last;

                SetDebouncer(500);

                Task.Run(() =>
                {
                    try
                    {

                        mainControl.Update();
                        trailingStop.SetSyncTimer();
                        funcoes.InfoConexao(myConnection);
                        funcoes.AccountInformation(conta);
                        Print("Fim da Inicialização do Robô");
                        roboTerminouInicializacao = true;

                        //if (tipoDeLogicaInicial == TipoDeEntradaInicial.simples)
                        //{
                            Print("Inicializando indicador Camilo 3MM");
                            MM = Indicators.iCustom("Camilo_3mm", CurrentData, periodoMMovelCurtissima, periodoMMovelCurta, periodoMMovelLonga);
                        //}

                        //setando estado inicial
                        estado = mainControl.GetEstadoEntrada();
                        //ReEntradas = pontosDeReEntradas.Split(',').Select(n => Convert.ToDouble(n)).ToArray();
                        //ValoresDeReEntradas = valoresDeReentrada.Split(',').Select(n => Convert.ToDouble(n)).ToArray();
                    }
                    catch (Exception exc)
                    {
                        Print("Erro na inicialização do Robô... " + exc.Message);
                        Print(exc.InnerException.Message);
                        FinalizaRobo();
                    }
                });
            }
            catch (Exception ex)
            {
                Print("Erro inicialização!", ex.ToString());
            }

        }


        public void FinalizaRobo()
        {
            Print("Finalizando Robô");
            TradingStrategy[] robos = TradingStrategy.GetTradingStrategies();
            foreach (var robo in robos)
            {
                robo.Stop();
                Print("Nome do robô:" + robo.Name);
            }
        }

        public override void OnQuote()
        {
            if (!roboTerminouInicializacao) { Print("Aguardando o término da inicialização..."); return; }
            logDin.Clear();
            watch.Restart();
            if (onQuoteWatcher) Print("ERRO OnQuote: Não FINALIZOU ANÁLISE OnQuote por alguma exceção!");
            onQuoteWatcher = true;
            ExecutaAnalise();
            onQuoteWatcher = false;
            watch.Stop();
            ms = watch.ElapsedTicks;
            if (maxMs < ms) maxMs = ms;
            if (minMs > ms) minMs = ms;
        }

        public override void NextBar()
        {

        }

        public override void Complete()
        {
            Instruments.Unsubscribe(Instruments.Current, QuoteTypes.Quote | QuoteTypes.Trade | QuoteTypes.Level2);
            sniper = null; MM = null;
            if (syncTimer != null) syncTimer.Enabled = false;
            trailingStop.syncTimer.Enabled = false;

            Orders.OrderAdded -= Orders_OrderAdded;
            Orders.OrderExecuted -= Orders_OrderExecuted;
            Orders.OrderRemoved -= Orders_OrderRemoved;
        }


        public override void OnPaintChart(object sender, PaintChartEventArgs args)
        {

            Point clientPoint = CurrentChart.PointToClient(Cursor.Position);
            int x = clientPoint.X;
            int y = clientPoint.Y;
            TimePrice timeprice = CurrentChart.GetTimePrice(x, y);
            args.Graphics.DrawString($"{timeprice.Time.ToLocalTime().ToShortTimeString()} hrs, R${timeprice.Price} . {Control.MouseButtons.ToString()}", font, brush, clientPoint);
            args.Graphics.DrawString($"Window ID:{CurrentChart.ID}", font, brush, args.Rectangle.Width - 80, args.Rectangle.Height);
            

            if (habilitarComandosPeloMouse)
            {
                if (Control.MouseButtons == buttonBuy && DentroDaJanela(clientPoint, args.Rectangle))
                {
                    if (!onDebounce)
                    {
                        if (comando.Send_order(OrdersType.Market, Operation.Buy, 0, mainControl.timelineController.Contratos) != "-1")
                        {
                            Print("Ordem de Compra enviada!");                                                                                    
                        }
                    }                        
                    debouncer.Enabled = true;
                    onDebounce = true;
                }

                if (Control.MouseButtons == buttonSell && DentroDaJanela(clientPoint, args.Rectangle))
                {                    
                    if (!onDebounce)
                    {
                        if (comando.Send_order(OrdersType.Market, Operation.Sell, 0, mainControl.timelineController.Contratos) != "-1")
                        {
                            Print("Ordem de Venda enviada!");                            
                        }
                    }
                    debouncer.Enabled = true;
                    onDebounce = true;
                }                              
              
            }

            eventos.OnPaintChart(sender, args);
        }

        private void Orders_OrderAdded(Order obj) => eventos.Orders_OrderAdded(obj);
        private void Orders_OrderRemoved(Order obj) => eventos.Orders_OrderRemoved(obj);
        private void Orders_OrderExecuted(Order obj) => eventos.Orders_OrderExecuted(obj);

        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************         
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************
        //********************************************************************************************************************************  

        public virtual double GetDifMediasCurtissima() => (mCurtissima - mCurtissimaAnterior) * multiplicador;
        public virtual double GetDifMediasCurta() => (mCurta - mCurtaAnterior) * multiplicador;
        public virtual double GetDifMediasLonga() => (mLonga - mLongaAnterior) * multiplicador;
        public virtual ConnectionStatus GetConnectionStatus() => myConnection.Status;

        public bool DentroDaJanela(Point coord, Rectangle janela) => (coord.X > 0 && coord.X < janela.Width && coord.Y > 0 && coord.Y < janela.Height);


        private void SetDebouncer(int v)
        {
            debouncer = new System.Timers.Timer(v);
            debouncer.Elapsed += Debouncer_Elapsed;
            debouncer.AutoReset = true;
        }

        private void Debouncer_Elapsed(object sender, ElapsedEventArgs e)
        {
            onDebounce = false;
            debouncer.Enabled = false;
        }

        private void ExecutaAnalise()
        {
            if (Instruments.Current.BaseName == papel)
            {
                try
                {
                    log = "********************   NewQuote   ********************";
                    Print(log);
                    mainControl.Update();

                    switch (estado)
                    {
                        case State.inicio: { estadoInicio.Run(); break; }
                        case State.trailingStop: { trailingStop.Run(); break; }
                        case State.TPSL: { tpslSimples.Run(); break; }
                    }
                }
                catch (Exception ex)
                {
                    Print("Erro NewQuote");
                }
            }
        }

        public virtual void Print(string message)
        {
            if (!blockLog) { base.Print(message); return; }
            logDin.Add(message);
        }

        public virtual double GetMinimumLot() => Instruments.Current.MinimalLot;
        public virtual double GetTickSize() => Instruments.Current.TickSize;
        public virtual double RoundPrice(double price) => Instruments.Current.RoundPrice(price);


    }

    static class Extensions
    {
        public static bool In<T>(this T item, params T[] items)
        {
            if (items == null)
                throw new ArgumentNullException("items");

            return items.Contains(item);
        }
    }

}
