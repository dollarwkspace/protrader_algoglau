﻿using System;
using PTLRuntime.NETScript;
using System.Linq;
using AlgoGlauNamespace.SDKEncapsulation;

namespace AlgoGlauNamespace
{
    public class Comando
    {
        private ProjectAlgoGlau robo = null;
        private string log;

        public Comando(ProjectAlgoGlau _robo) { robo = _robo; log = robo.log; }

       public virtual string ReEntrada(double preco, PositionWrapper pos)
        {
            if (pos.Side == Operation.Buy)
            {
                robo.Print("ReEntrada: Tentando emitir ordem de Reentrada...");
                return Send_order(OrdersType.Market, Operation.Buy, preco, robo.ValoresDeReEntradas[robo.contadorDeReEntradas], 0);
            }

            if (pos.Side == Operation.Sell)
            {
                robo.Print("ReEntrada: Tentando emitir ordem de Reentrada...");
                return Send_order(OrdersType.Market, Operation.Sell, preco, robo.ValoresDeReEntradas[robo.contadorDeReEntradas], 0);
            }

            return "-1";
        }

        public virtual string Send_order(OrdersType type, Operation side, double price, double contratos, double takeProfit=0)
        {
            if (side == Operation.Buy && (robo.tipoDeBloqueio == TipoDeBloqueio.BUY) ) { robo.Print("SendOrder: ENTRADA BUY BLOQUEADA PELO USUÁRIO"); return "-1"; }
            if (side == Operation.Sell && (robo.tipoDeBloqueio == TipoDeBloqueio.SELL) ) { robo.Print("SendOrder: ENTRADA SELL BLOQUEADA PELO USUÁRIO"); return "-1"; }

            if (robo.mainControl.timelineController.PosicaoMedia.Amount != 0)
            {
                if (Math.Abs(robo.mainControl.timelineController.PosicaoMedia.Amount??0) + contratos > robo.maxPosAbertas)
                {
                    log = "Send_Order: Não é possível abrir mais contratos!";
                    robo.mainControl.AddToInnerStatesBuffer(InnerState.lockdownPorMaximoNumeroDeContratosAtingidos);
                    robo.Print(log);
                    return "-1";
                }
            }
            
            QuoteWrapper quote = robo.lastQuote;
            if (quote.Last == 0 || quote is null) { robo.Print("SendOrder: Cotação nula"); return "-1"; }
            if (price == 0) price = (side == Operation.Buy) ? robo.RoundPrice(quote.Ask) : robo.RoundPrice(quote.Bid); else price = robo.RoundPrice(price);


            INewOrderRequestWrapper request = new NewOrderRequestWrapper
            {
                Instrument = robo.Instruments.Current,
                Type = type,
                Side = side,
                Amount = Math.Abs(contratos),
                MarketRange = robo.marketRange                            
            };

            if (robo.mainControl.timelineController.PosicaoMedia.Amount != 0) request.LinkTo = robo.mainControl.timelineController.PosicaoAtiva?.Id??"0";

                if (type != OrdersType.Stop) request.Price = price;
            if (type == OrdersType.Stop) request.StopPrice = price;

        
            log = $"{robo.horario} Send_Order(): Type:{type} Contratos:{contratos} Side:{side} Price:{price} StopPrice: {request.StopPrice}";
            robo.Print(log);

            robo.Print($"SendOrder: Emitindo uma ordem de {request.Side} no preço R${robo.RoundPrice(price)} Qtde:{request.Amount} Ponto Atual da Média:{robo.mCurta}");

            robo.mainControl.AddToInnerStatesBuffer(InnerState.lockdownPorOrdemEnviada);
            string result = robo.mainControl.ordersController.SendOrders(request);

            if (result == "-1") { log = $"{robo.horario} SendOrder: Ordem não aceita pela corretora"; robo.Print(log); robo.mainControl.RemoveFromInnerStatesBuffer(InnerState.lockdownPorOrdemEnviada); return "-1"; }

            log = $"{robo.horario} SendOrder: Ordem com ticket {result} foi enviada para a corretora!";
            robo.Print(log);            
            return result;
        }

        public virtual string CriaParDeSaida()
        {            
            if (robo.mainControl.timelineController.PosicaoMedia.Amount!=0)
            {
                PositionWrapper pos = robo.mainControl.timelineController.PosicaoMedia;
                string result = EnviaOrdem(
                    (pos.Side == Operation.Buy) ? Operation.Sell : Operation.Buy,
                    (pos.Side == Operation.Buy) ? (pos.OpenPrice ?? 0) + robo.TP * robo.GetTickSize() : (pos.OpenPrice ?? 0) - robo.TP * robo.GetTickSize(),
                    (pos.Amount ?? 0),
                    OrdersType.Manual);                    
                if (result != "-1") return result;
                return "-1";
            } else
            {         
                throw new Exception("Cria Par de Saída foi chamado com posição zerada!");                
            }           
            
        }

        public virtual string CriaSaidaStopLoss()
        {
            if (robo.mainControl.timelineController.PosicaoMedia.Amount != 0)
            {
                PositionWrapper pos = robo.mainControl.timelineController.PosicaoMedia;
                string result = EnviaOrdem(
                    (pos.Side == Operation.Buy) ? Operation.Sell : Operation.Buy,
                    (pos.Side == Operation.Buy) ? (pos.OpenPrice ?? 0) - robo.SL * robo.GetTickSize() : (pos.OpenPrice ?? 0) + robo.SL * robo.GetTickSize(),
                    (pos.Amount ?? 0),
                    OrdersType.Manual);
                if (result != "-1") return result;
                return "-1";
            }
            else
            {
                throw new Exception("Cria Saída StopLoss foi chamado com posição zerada!");
            }

        }

        public virtual string EnviaOrdem(Operation lado, double preco, double qtde = 0, OrdersType tipo= OrdersType.Market)
        {
            if ((qtde > 0 && lado == Operation.Buy) || (qtde < 0 && lado == Operation.Sell)) qtde *= -1;            
            if (tipo == OrdersType.Market) { goto Envio; }
            if ((robo.cotacaoAtual >= preco && lado == Operation.Buy) || (robo.cotacaoAtual < preco && lado == Operation.Sell)) tipo = OrdersType.Limit;
            if ((robo.cotacaoAtual < preco && lado == Operation.Buy) || (robo.cotacaoAtual > preco && lado == Operation.Sell)) { tipo = OrdersType.Stop;}

            Envio:
            return Send_order(tipo, lado, preco, qtde, 0);
        }

    }
}

