﻿using AlgoGlauNamespace.SDKEncapsulation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTLRuntime.NETScript;

namespace AlgoGlauNamespace.Timeline
{
    public enum TimelinePairStatus { empty, open, closed }
    public class TimeLinePair
    {
        private PositionWrapper _entrada = new PositionWrapper();
        private PositionWrapper _saida = new PositionWrapper();
        private ObservableCollection<PositionWrapper> _saidas = new ObservableCollection<PositionWrapper>();
        private TimelinePairStatus _status = TimelinePairStatus.empty;

        public PositionWrapper Entrada { get => _entrada; private set { _entrada = value; } }
        public PositionWrapper Saida { get => _saida; private set { _saida = value; } }
        public List<PositionWrapper> Saidas { get => _saidas.ToList(); }
        public TimelinePairStatus Status { get => _status; private set { _status = value; } }

        public bool SetEntrada(PositionWrapper pos)
        {
            if (ReferenceEquals(pos, null)) return false;
            if (pos.Amount == null || pos.Amount==0 || pos.Id == null || pos.NumRobo == null || 
                pos.OpenPrice == null || pos.OpenPrice == 0 ||
                pos.Side == null) return false;
            if (Status == TimelinePairStatus.empty) { Entrada = pos; Status = TimelinePairStatus.open; return true; }
            return false;
        }

        public bool SetSaida(PositionWrapper pos)
        {
            if (CheckConditionToAddNewSaida(pos)) { _saidas.Add(pos); return true; }
            return false;
        }

        private bool CheckConditionToAddNewSaida(PositionWrapper pos)
        {
            if (ReferenceEquals(pos, null)) return false;
            if (pos.Amount == null || pos.Id == null || pos.NumRobo == null || pos.OpenPrice == null || pos.Side == null) return false;
            if (Status == TimelinePairStatus.closed || Status == TimelinePairStatus.empty) return false;
            if (pos.Side == Entrada.Side) return false;
            //if (Entrada.Side == Operation.Buy && pos.OpenPrice < Entrada.OpenPrice) return false;
            //if (Entrada.Side == Operation.Sell && pos.OpenPrice > Entrada.OpenPrice) return false;
            if (Math.Abs(_saidas.Sum(x => x.Amount ?? 0) + pos.Amount ?? 0) > Math.Abs(Entrada.Amount ?? 0)) return false;
            return true;
        }

        public TimeLinePair() => _saidas.CollectionChanged += _saidas_CollectionChanged;

        private void _saidas_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                _saida = new PositionWrapper() {Lucro=0, OpenPrice=0 };                
                _saidas.ToList().ForEach(pos => _saida += pos);
                if (Math.Abs(_saida.Amount??0) == Math.Abs(Entrada.Amount ?? 0)) Status = TimelinePairStatus.closed;
            }
        }        
    }
}
