﻿using PTLRuntime.NETScript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.IO;
using AlgoGlauNamespace.SDKEncapsulation;

namespace AlgoGlauNamespace.Timeline
{
    [DataContract]
    public class TimelineController
    {
        private ProjectAlgoGlau robo;
        private Timeline timeline = new Timeline();
        public TimelineController(ProjectAlgoGlau robo)
        {
            this.robo = robo;
            timeline.TimelineClosedEvent += (List<TimeLinePair> closedTl) =>
            {
                PositionWrapper posicaoFinal = timeline.GetPosicaoMedia(closedTl);
                if (posicaoFinal.Lucro > 0) OnTimelineFechouNoLucro(posicaoFinal.Lucro ?? 0);
                if (posicaoFinal.Lucro < 0) OnTimelineFechouNoPrejuizo(posicaoFinal.Lucro ?? 0);
                TimelineCounter++;
                CalorPorTimeline = 0;
            };

            timeline.TimelineCreatedOrderToCloseEvent += (PositionWrapper pos) =>
                OnOpenOrderRequested(pos.Amount ?? 0, pos.Side ?? Operation.Buy);
        }

        public PositionWrapper PosicaoMedia
        {
            get
            {
                PositionWrapper pos = timeline.GetPosicaoMedia() ?? new PositionWrapper() {Amount=0, Lucro=0, OpenPrice=0 };
                if (robo.GetConnectionStatus() == PTLRuntime.NETScript.Application.ConnectionStatus.Disconnected)
                {
                    pos.Lucro *= robo.custoContratoPorPonto;
                }
                CalorMaximo = CalculaCalor(pos);
                CalorPorTimeline = CalculaCalor(pos);
                if (Math.Abs(pos.Amount ?? 0) > 0)
                {
                    MaximoDeContratosAbertos = Math.Abs(pos.Amount ?? 0);
                }
                return pos;
            }
        }
        public PositionWrapper PosicaoAtiva { get => (timeline.GetLastActiveItem() ?? new PositionWrapper() { Amount = 0, Lucro = 0, OpenPrice = 0 }); }
        public List<TimeLinePair> ItensAtivos { get => timeline.GetActiveItems(); }
        public List<TimeLinePair> AllItems { get => timeline.GetAllItems(); }


        private int _tlCounter = 0;
        private double _lucroDia = 0, _perdasDia = 0, _calorMaximo = 0, _calorPorTimeline = 0, _maximoDeContratosAbertos = 0, _resultadoDia = 0;
        private double _contratos = 0;        

        private double CalculaCalor(PositionWrapper posMedia)
        {
            double calor = 0;
            if (posMedia.Amount != 0 && posMedia.OpenPrice != 0)
            {
                double valor = Math.Abs(posMedia.Amount ?? 0) * (posMedia.OpenPrice ?? 0);
                if (valor != 0)
                {
                    calor = (posMedia.Side == Operation.Buy) ?
                            Math.Abs(posMedia.Amount ?? 0) * robo.lastQuote.Last - valor :
                            valor - Math.Abs(posMedia.Amount ?? 0) * robo.lastQuote.Last;
                }
            }
            return calor;
        }

        public double Contratos
        {
            get
            {
                if (_contratos == 0) _contratos = robo.contratosIniciais;                
                if (_contratos < robo.GetMinimumLot()) _contratos = robo.GetMinimumLot();
                return _contratos;
            }
            set
            {
                _contratos = value;                                
            }
        }
        

        public int TimelineCounter
        {
            get { return _tlCounter; }
            private set
            {
                _tlCounter = value;                
            }
        }
        public double LucroDia { get => _lucroDia; private set { _lucroDia = value; robo.lucroDia = _lucroDia; ResultadoDia = _lucroDia + _perdasDia; } }
        public double PerdasDia { get => _perdasDia; private set { _perdasDia = value; robo.lossDia = _perdasDia; ResultadoDia = _lucroDia + _perdasDia; } }

        public double ResultadoDia
        {
            get { return _resultadoDia; }
            private set
            {
                _resultadoDia = value; if (value > robo.stopGainFinanceiro) OnEstouroDeLucroEvent();
                if (value < -robo.stopLossFinanceiro) OnEstouroDePerdasEvent();
            }
        }

        public void ResetTimeLineVarsDueToDayChanged()
        {
            LucroDia = 0; PerdasDia = 0; TimelineCounter = 0; Contratos = robo.contratosIniciais; CalorPorTimeline = 0;
        }

        public double CalorMaximo { get { return _calorMaximo; } private set { if (value < _calorMaximo) { _calorMaximo = value; robo.calor = value; } } }
        public double CalorPorTimeline
        {
            get { return _calorPorTimeline; }
            private set
            {
                if (value == 0) _calorPorTimeline = 0;
                if (value < _calorPorTimeline)
                {
                    _calorPorTimeline = value;
                    robo.calorPorTimeline = value;
                    if (Math.Abs(value) > robo.stopPorCalorMaximo) { OnFechamentoPorCalorMaximo(Math.Abs(value)); }
                }
            }
        }
        public double MaximoDeContratosAbertos { get => _maximoDeContratosAbertos; private set { if (value == 0) _maximoDeContratosAbertos = 0; if (value > _maximoDeContratosAbertos) { _maximoDeContratosAbertos = value; robo.maximoDeContratosAbertos = _maximoDeContratosAbertos; } } }

        public void ForceTimelineToClose() => timeline.ForceTimelineToClose();
        public void AddToTimeline(OrderWrapper ord)
        {
            timeline.AddToTimeline(new PositionWrapper()
            {
                Amount = ord.Amount,
                Id = ord.Id,
                Lucro = 0,
                NumRobo = robo.numeroRobo.ToString(),
                OpenPrice = ord.Price,
                OpenTime = robo.lastQuote.Time.ToLocalTime(),
                Side = ord.Side
            });
        }

        public event Action EstouroDeTradesEvent;
        public event Action EstouroDeLucroEvent;
        public event Action EstouroDePerdasEvent;
        public event Action<double> FechamentoPorCalorMaximoEvent;
        public event Action<double> TimelineFechouNoLucroEvent;
        public event Action<double> TimelineFechouNoPrejuizoEvent;
        public event Action<double, Operation> OpenOrderRequestedEvent;
        public event Action TimelineFechouEvent;


        private void OnEstouroDeTrades() => EstouroDeTradesEvent?.Invoke();
        private void OnEstouroDeLucroEvent() => EstouroDeLucroEvent?.Invoke();
        private void OnEstouroDePerdasEvent() => EstouroDePerdasEvent?.Invoke();
        private void OnFechamentoPorCalorMaximo(double calor)
        {
            robo.mainControl.AddToInnerStatesBuffer(InnerState.lockdownPorCalorMaximo);
            ForceTimelineToClose();
            FechamentoPorCalorMaximoEvent?.Invoke(calor);
        }
        private void OnTimelineFechouNoLucro(double resultado)
        {
            LucroDia += resultado;
            OnTimelineFechou();
            TimelineFechouNoLucroEvent?.Invoke(resultado);
        }
        private void OnTimelineFechouNoPrejuizo(double resultado)
        {            
            PerdasDia += resultado;
            OnTimelineFechou();
            TimelineFechouNoPrejuizoEvent?.Invoke(resultado);            
        }

        private void OnTimelineFechou() => TimelineFechouEvent?.Invoke();

        private void OnOpenOrderRequested(double amount, Operation op) => OpenOrderRequestedEvent?.Invoke(amount, op);

    }

}

