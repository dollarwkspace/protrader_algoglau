﻿using AlgoGlauNamespace.SDKEncapsulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTLRuntime.NETScript;

namespace AlgoGlauNamespace.Timeline
{
    public class Timeline
    {
        //FIELDS
        private List<TimeLinePair> _timeline = new List<TimeLinePair>();
        private PositionWrapper PosicaoMedia = new PositionWrapper();

        //PRIVATE METHODS
        private void ClearTimeLine() { _timeline.Clear(); }
        private bool TimelineCanBeClosed() => GetActiveItems().Count == 0;
        private void OnTimelineClosed(List<TimeLinePair> obj) => TimelineClosedEvent?.Invoke(obj);
        private void OnTimelineCreatedOrderToClose(PositionWrapper pos) => TimelineCreatedOrderToCloseEvent?.Invoke(pos);

        //PUBLIC METHODS

        public Action<List<TimeLinePair>> TimelineClosedEvent;
        public Action<PositionWrapper> TimelineCreatedOrderToCloseEvent;

        public TimeLinePair GetLastTimeLinePair() => (_timeline.Count > 0) ? _timeline.Last() : null;
        public List<TimeLinePair> GetAllItems() => (_timeline.Count > 0) ? _timeline : new List<TimeLinePair>();
        public List<TimeLinePair> GetActiveItems() => (GetAllItems()?.Count > 0) ? GetAllItems().FindAll(x => x.Status == TimelinePairStatus.open) : new List<TimeLinePair>();
        public TimeLinePair GetLastActiveTimeLinePair() => (GetActiveItems()?.Count > 0) ? GetActiveItems().Last() : null;
        public PositionWrapper GetLastActiveItem() => (GetLastActiveTimeLinePair() != null) ? GetLastActiveTimeLinePair().Entrada : null;


        public bool AddToTimeline(PositionWrapper pos)
        {
            if (GetActiveItems().Count == 0)
            {
                TimeLinePair tp = new TimeLinePair();
                if (tp.SetEntrada(pos))
                {
                    _timeline.Add(tp);
                    return true;
                }
            }
            else
            {
                if (!GetLastActiveTimeLinePair().SetSaida(pos))
                {
                    TimeLinePair tp = new TimeLinePair();
                    if (tp.SetEntrada(pos))
                    {
                        _timeline.Add(tp);
                        return true;
                    }
                }
                if (TimelineCanBeClosed()) { OnTimelineClosed(_timeline); ClearTimeLine(); }
                return true;
            }
            return false;
        }

        public void ForceTimelineToClose()
        {
            if (GetActiveItems().Count == 0) return;
            TimeLinePair lastActiveTlPair = GetLastActiveTimeLinePair();
            PositionWrapper posSum = lastActiveTlPair.Entrada + lastActiveTlPair.Saida;
            PositionWrapper posToClose = new PositionWrapper()
            {
                Amount = (-1) * (posSum.Amount),
                Side = (posSum.Side == Operation.Buy) ? Operation.Sell : Operation.Buy,
                OpenPrice = 0,
                Lucro = 0,
                Id = "0",
                NumRobo = "0",
                OpenTime = null
            };
            OnTimelineCreatedOrderToClose(posToClose);                            
            ForceTimelineToClose();
        }

        public PositionWrapper GetPosicaoMedia()
        {
            if (_timeline.Count > 0)
            {
                PositionWrapper posMedia = new PositionWrapper() { Lucro = 0, OpenPrice = 0 };
                _timeline.ForEach(tl =>
                {
                    posMedia += tl.Entrada + tl.Saida;
                });
                PosicaoMedia = posMedia;                                
            }
            else
            {
                PosicaoMedia.Amount = 0; PosicaoMedia.Lucro = 0; PosicaoMedia.OpenPrice = 0;
            }

            return PosicaoMedia;
        }

        public PositionWrapper GetPosicaoMedia(List<TimeLinePair> tempTl)
        {
            if (tempTl.Count > 0)
            {
                PositionWrapper posMedia = new PositionWrapper() { Lucro = 0, OpenPrice = 0 };
                tempTl.ForEach(tl =>
                {
                    posMedia += tl.Entrada + tl.Saida;
                });
                return posMedia;
            }
            else
            {
                return new PositionWrapper() { Amount = 0, OpenPrice = 0, Lucro = 0 };
            }            
        }

    }
}
