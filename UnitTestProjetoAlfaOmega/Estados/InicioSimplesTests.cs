﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgoGlauNamespace.SDKEncapsulation;
using AlgoGlauNamespace.Timeline;
using AlgoGlauNamespace;
using System.Timers;
using System.Globalization;
using PTLRuntime.NETScript;
using System.Diagnostics;

namespace UnitTestProjectRobo.Estados
{
    [TestClass]
    public class InicioSimplesTests
    {
        private Mock<IOrderRepository> mockOrderRepo;
        private Mock<ProjectAlgoGlau> mockRobo = new Mock<ProjectAlgoGlau>(MockBehavior.Strict);
        private Mock<Comando> comando;
        private AppControl appControl;
        private EstadoInicio inicio;
        private HashSet<OrderWrapper> bufferDeOrdens = new HashSet<OrderWrapper>();        
        private Timer timer = new Timer(1000);
        private Mock<IQuoteRepository> quoteRepoMock = new Mock<IQuoteRepository>();
        private int Id = 0;
        private DateTime currentDT = DateTime.Parse("18/04/2019 12:00:00", new CultureInfo("pt-BR"));


        [TestInitialize]
        public void Setup()
        {
            mockOrderRepo = new Mock<IOrderRepository>();            
            appControl = new AppControl(mockRobo.Object, mockOrderRepo.Object);
            comando = new Mock<Comando>(mockRobo.Object);
            mockRobo.Object.lastQuote = new QuoteWrapper(mockRobo.Object, quoteRepoMock.Object);
            mockRobo.Object.mainControl = appControl;

            mockRobo.SetupAllProperties();
            mockRobo.Object.tipoDeLogicaInicial = TipoDeEntradaInicial.simples;
            mockRobo.Object.tipoDeLogicaControle = TipoDeOperacaoPosPosicao.reEntradaSimples;
            mockRobo.Object.estado = State.inicio;
            mockRobo.Object.numeroRobo = 1;
            mockRobo.Object.HoraDeTermino = new TimeSpan(12, 0, 0);            
            mockRobo.Object.mCurta = 0;
            mockRobo.Object.mCurtaAnterior = 0;
            mockRobo.Object.multiplicador = 10;                        
            mockRobo.Object.mLonga = 0;
            mockRobo.Object.mLongaAnterior = 0;            
            mockRobo.Object.comando = comando.Object;
            mockRobo.Object.mainControl = appControl;

            quoteRepoMock.Setup(p => p.GetAsk()).Returns(100);
            quoteRepoMock.Setup(p => p.GetBid()).Returns(100);
            quoteRepoMock.Setup(p => p.GetLast()).Returns(100);
            quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);
            mockRobo.Setup(p => p.GetConnectionStatus()).Returns(PTLRuntime.NETScript.Application.ConnectionStatus.Disconnected);
            mockRobo.Setup(p => p.Print(It.IsAny<string>())).Callback((string s) => Debug.WriteLine(s));
            mockRobo.Setup(p => p.GetMinimumLot()).Returns(5);

            comando.Setup(p => p.Send_order(It.IsAny<OrdersType>(), It.IsAny<Operation>(), It.IsAny<double>(), It.IsAny<double>(), 0)).Callback(
               (OrdersType tipo, Operation side, double price, double amount, double tp) =>
               {
                   //if (amount > 0 && side == Operation.Buy) amount = amount;
                   if (amount < 0 && side == Operation.Sell) amount = -amount;
                   bufferDeOrdens.Add(new OrderWrapper(mockRobo.Object)
                   {
                       Amount = amount,
                       Id = Id.ToString(),
                       IsActive = true,
                       OpenTime = DateTime.Now,
                       Status = OrderStatus.New,
                       Price = mockRobo.Object.lastQuote.Last,
                       NumRobo = mockRobo.Object.numeroRobo.ToString(),
                       Side = side,
                       Type = tipo
                   });
                   mockOrderRepo.Setup(o => o.GetOrdersFromServer()).Returns(bufferDeOrdens.ToHashSet());
                   appControl.ordersController.UpdateOrders(); //ADICIONA A ORDEM
                    Id++;
                   timer.Enabled = false;
               }
               ).Returns(Id.ToString());
                        
            timer.Elapsed += Timer_Elapsed;
            timer.AutoReset = true;
            timer.Enabled = false;

            inicio = new EstadoInicio(mockRobo.Object);
            
            TraceListener listener = new DelimitedListTraceListener(@"C:\robo\debugfile.txt");
            //TextWriterTraceListener writer = new TextWriterTraceListener(System.Console.Out);
            //Debug.Listeners.Add(writer);            
            Debug.Listeners.Add(listener);
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            appControl.ordersController.UpdateOrders();
            timer.Enabled = false;
        }


        /*[TestMethod]
        //Respeita média longa, usa Entrada Laranja em Todos os Trades, cotação, média curta, média longa
        [DataRow(true, false, 100, 95, 94, 110,109 )]
        [DataRow(true, false, 100, 105, 104, 99, 98)]
        [DataRow(true, true, 100, 94, 95, 109, 110)]
        [DataRow(true, true, 100, 105, 104, 99, 98)]
        public void InicioTest_Cenarios_MustReturnTrueEmTodos(bool respeitaMediaLonga, bool usaEntradaLaranja, double cotacao, double mCurta, double mCurtaAnt, double mLonga, double mLongaAnt)
        {
            //PREPARE
            mockRobo.Object.cotacaoAtual = cotacao;
            mockRobo.Object.mCurta = mCurta;
            mockRobo.Object.mCurtaAnterior = mCurtaAnt;
            mockRobo.Object.mLonga = mLonga;
            mockRobo.Object.mLongaAnterior = mLongaAnt;
            mockRobo.Object.distPtsMediaBaseCurta = 2;
            mockRobo.Object.respeitaMediaLonga = respeitaMediaLonga;
            mockRobo.Object.usaMEntradaLaranjaEmTodosOsTrades = usaEntradaLaranja;
            bool orderAddedEventCalled = false;
            bool orderExecutedEventCalled = false;

            mockRobo.Setup(p => p.GetDifMediasCurtissima()).Returns(0);
            mockRobo.Setup(p => p.GetDifMediasCurta()).Returns(mCurta - mCurtaAnt);
            mockRobo.Setup(p => p.GetDifMediasLonga()).Returns(mLonga - mLongaAnt);

            appControl.OrderAddedEvent += (OrderWrapper ord) => {
                orderAddedEventCalled = true;
                bufferDeOrdens.Clear();
                mockOrderRepo.Setup(o => o.GetOrdersFromServer()).Returns(bufferDeOrdens.ToHashSet());
                appControl.ordersController.UpdateOrders(ord.Id);//executa a ordem imediatamente
            };
            appControl.OrderExecutedEvent += (OrderWrapper ord) =>
            {
                orderExecutedEventCalled = true;
            };

            
            //ARRANGE
            inicio.Run();            

            //ACT & ASSERT
            Assert.IsTrue(orderAddedEventCalled);
            Assert.IsTrue(orderExecutedEventCalled);
            Assert.IsTrue(appControl.timelineController.AllItems.Count != 0);

        }
        */

    }
}
