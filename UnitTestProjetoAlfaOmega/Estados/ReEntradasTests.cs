﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgoGlauNamespace.SDKEncapsulation;
using AlgoGlauNamespace;
using System.Timers;
using System.Globalization;
using PTLRuntime.NETScript;
using System.Diagnostics;

namespace UnitTestProjectRobo.Estados
{
    [TestClass]
    public class ReEntradasTests
    {
        private Mock<IOrderRepository> mockOrderRepo;
        private Mock<ProjectAlgoGlau> mockRobo = new Mock<ProjectAlgoGlau>(MockBehavior.Strict);
        private Mock<Comando> comando;
        private Mock<Funcoes> funcoes;
        private AppControl appControl;
        //private EstadoReEntrada reEntradas;
        private EstadoInicio inicio;
        private HashSet<OrderWrapper> bufferDeOrdens = new HashSet<OrderWrapper>();
        private Mock<IQuoteRepository> quoteRepoMock = new Mock<IQuoteRepository>();
        private int Id = 0;
        private DateTime currentDT = DateTime.Parse("18/04/2019 12:00:00", new CultureInfo("pt-BR"));

        /*
        [TestInitialize]
        public void Setup()
        {
            mockOrderRepo = new Mock<IOrderRepository>();
            appControl = new AppControl(mockRobo.Object, mockOrderRepo.Object);
            comando = new Mock<Comando>(mockRobo.Object);
            funcoes = new Mock<Funcoes>(mockRobo.Object);
            mockRobo.Object.lastQuote = new QuoteWrapper(mockRobo.Object, quoteRepoMock.Object);
            mockRobo.Object.mainControl = appControl;
            mockRobo.Object.comando = comando.Object;
            mockRobo.Object.funcoes = funcoes.Object;

            mockRobo.SetupAllProperties();
            mockRobo.Object.tipoDeLogicaInicial = TipoDeEntradaInicial.simples;
            mockRobo.Object.tipoDeLogicaControle = TipoDeOperacaoPosPosicao.reEntradaSimples;
            mockRobo.Object.estado = State.inicio;
            mockRobo.Object.numeroRobo = 1;
            mockRobo.Object.lossMaximo = 100;
            
            quoteRepoMock.Setup(p => p.GetAsk()).Returns(100);
            quoteRepoMock.Setup(p => p.GetBid()).Returns(100);
            quoteRepoMock.Setup(p => p.GetLast()).Returns(100);
            quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);
            mockRobo.Setup(p => p.GetConnectionStatus()).Returns(PTLRuntime.NETScript.Application.ConnectionStatus.Disconnected);
            mockRobo.Setup(p => p.GetTickSize()).Returns(1);
            mockRobo.Setup(p => p.Print(It.IsAny<string>())).Callback((string s) => Debug.WriteLine(s));
            mockRobo.Setup(p => p.GetMinimumLot()).Returns(5);

            var resultCallback = "";
            comando.Setup(p => p.CriaParDeSaida()).Callback(() => resultCallback = CriaParDeSaida()).Returns(resultCallback);
            comando.Setup(p => p.Send_order(It.IsAny<OrdersType>(), It.IsAny<Operation>(), It.IsAny<double>(), It.IsAny<double>(), 0)).Callback(
               (OrdersType tipo, Operation side, double price, double amount, double tp) =>
               {
                   if (amount > 0 && side == Operation.Buy) amount = -amount;
                   if (amount < 0 && side == Operation.Sell) amount = -amount;
                   bufferDeOrdens.Add(new OrderWrapper(mockRobo.Object)
                   {
                       Amount = amount,
                       Id = Id.ToString(),
                       IsActive = true,
                       OpenTime = DateTime.Now,
                       Status = OrderStatus.New,
                       Price = (price==0)?mockRobo.Object.lastQuote.Last:price,
                       NumRobo = mockRobo.Object.numeroRobo.ToString(),
                       Side = side,
                       Type = tipo
                   });
                   mockOrderRepo.Setup(o => o.GetOrdersFromServer()).Returns(bufferDeOrdens.ToHashSet());
                   appControl.ordersController.UpdateOrders(); //ADICIONA A ORDEM
                   Id++;
               }
               ).Returns(Id.ToString());

            comando.Setup(p => p.ReEntrada(It.IsAny<double>(), It.IsAny<PositionWrapper>())).Callback((double preco, PositionWrapper pos) => 
            {
                if (pos.Side == Operation.Buy)
                {                    
                    comando.Object.Send_order(OrdersType.Market, Operation.Buy, preco, mockRobo.Object.ValoresDeReEntradas[mockRobo.Object.contadorDeReEntradas], 0);
                }

                if (pos.Side == Operation.Sell)
                {                    
                    comando.Object.Send_order(OrdersType.Market, Operation.Sell, preco, mockRobo.Object.ValoresDeReEntradas[mockRobo.Object.contadorDeReEntradas], 0);
                }
            });

            funcoes.Setup(f => f.EqualizaOrdemPosicao(It.IsAny<PositionWrapper>(), It.IsAny<OrderWrapper>())).Returns(true);

            reEntradas = new EstadoReEntrada(mockRobo.Object);
            inicio = new EstadoInicio(mockRobo.Object);

            TraceListener listener = new DelimitedListTraceListener(@"C:\robo\debugfile.txt");
            //TextWriterTraceListener writer = new TextWriterTraceListener(System.Console.Out);
            //Debug.Listeners.Add(writer);            
            Debug.Listeners.Add(listener);
        }

        private string CriaParDeSaida()
        {
            if (mockRobo.Object.mainControl.timelineController.PosicaoMedia.Amount != 0)
            {
                PositionWrapper pos = mockRobo.Object.mainControl.timelineController.PosicaoMedia;
                string result = EnviaOrdem(
                    (pos.Side == Operation.Buy) ? Operation.Sell : Operation.Buy,
                    (pos.Side == Operation.Buy) ? (pos.OpenPrice ?? 0) + mockRobo.Object.TP * mockRobo.Object.GetTickSize() : (pos.OpenPrice ?? 0) - mockRobo.Object.TP * mockRobo.Object.GetTickSize(),
                    (pos.Amount ?? 0),
                    OrdersType.Manual);
                if (result != "-1") return result;
                return "-1";
            }
            return "-1";
        }

        private string EnviaOrdem(Operation lado, double preco, double qtde = 0, OrdersType tipo = OrdersType.Market)
        {
            if (tipo == OrdersType.Market) { preco = 0; goto Envio; }
            if ((mockRobo.Object.cotacaoAtual >= preco && lado == Operation.Buy) || (mockRobo.Object.cotacaoAtual < preco && lado == Operation.Sell)) tipo = OrdersType.Limit;
            if ((mockRobo.Object.cotacaoAtual < preco && lado == Operation.Buy) || (mockRobo.Object.cotacaoAtual > preco && lado == Operation.Sell)) { tipo = OrdersType.Stop; }

        Envio:
            return mockRobo.Object.comando.Send_order(tipo, lado, preco, qtde, 0);
        }


        private void EntradaBuy()
        {
            mockRobo.Object.cotacaoAtual = 25;
            mockRobo.Object.mLonga = 48;
            mockRobo.Object.mLongaAnterior = 47;
            mockRobo.Object.mCurta = 49;
            mockRobo.Object.mCurtaAnterior = 48;            
            mockRobo.Object.multiplicador = 1;
            mockRobo.Object.tipoDeRobo = TipoDeRobo.tendencia;            
            mockRobo.Object.TP = 1;
            mockRobo.Object.ValoresDeReEntradas = new double[] {5,10,20,40};
            mockRobo.Object.ReEntradas = new double[] { 10, 25, 30, 40 };

            DateTime currentDT = DateTime.Parse("18/04/2019 12:00:01", new CultureInfo("pt-BR"));
            mockRobo.Object.numeroRobo = 1;
            quoteRepoMock.Setup(p => p.GetLast()).Returns(25);
            quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);

            appControl.OrderAddedEvent += AppControl_OrderAddedEventFromInicio;

            inicio.Run(); //1a execução uma ordem é adicionada e executada
            appControl.OrderAddedEvent -= AppControl_OrderAddedEventFromInicio;
            inicio.Run(); //2a execução o estado muda pra ReEntrada            
            Assert.IsTrue(mockRobo.Object.estado == State.reentrada);
            Assert.IsTrue(mockRobo.Object.mainControl.timelineController.PosicaoAtiva.Side == Operation.Buy);
            Assert.IsTrue(mockRobo.Object.mainControl.timelineController.PosicaoAtiva.OpenPrice == 25);
        }

        private void AppControl_OrderAddedEventFromInicio(OrderWrapper ord)
        {
            bufferDeOrdens.Clear();
            mockOrderRepo.Setup(o => o.GetOrdersFromServer()).Returns(bufferDeOrdens.ToHashSet());
            appControl.ordersController.UpdateOrders(ord.Id); //ordem será executada
        }
      
        [TestCleanup]
        public void close()
        {
            Debug.Flush();
        }

        */

    }
}
