﻿using AlgoGlauNamespace;
using AlgoGlauNamespace.SDKEncapsulation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PTLRuntime.NETScript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProjectRobo.SDKEncapsulation
{
    [TestClass]
    public class OrderWrapperTests
    {
        private OrderWrapper ord1;
        private OrderWrapper ord2;
        private HashSet<OrderWrapper> hashTest;
        private Mock<ProjectAlgoGlau> roboMock = new Mock<ProjectAlgoGlau>();


        [TestInitialize]
        public void Setup() => hashTest = new HashSet<OrderWrapper>();        

        [TestMethod]        
        public void OrderWrapperEquality_ComparisonBetweenDifIds_MustReturnTrue()
        {
            ord1 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            ord2 = new OrderWrapper(roboMock.Object) { Id = "2", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            Assert.IsTrue(!ord1.Equals(ord2));
            Assert.IsTrue(ord1 != ord2);
        }

        [TestMethod]
        public void OrderWrapperEquality_ComparisonBetweenDifAmount_MustReturnTrue()
        {
            ord1 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            ord2 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 2, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            Assert.IsTrue(!ord1.Equals(ord2));
            Assert.IsTrue(ord1 != ord2);
        }

        [TestMethod]
        public void OrderWrapperEquality_ComparisonBetweenDifPrice_MustReturnTrue()
        {
            ord1 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            ord2 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 2, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            Assert.IsTrue(!ord1.Equals(ord2));
            Assert.IsTrue(ord1 != ord2);
        }

        [TestMethod]
        public void OrderWrapperEquality_ComparisonBetweenDifSide_MustReturnTrue()
        {
            ord1 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            ord2 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Sell, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            Assert.IsTrue(!ord1.Equals(ord2));
            Assert.IsTrue(ord1 != ord2);
        }

        [TestMethod]
        public void OrderWrapperEquality_ComparisonBetweenDifLinkedTo_MustReturnTrue()
        {
            ord1 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            ord2 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "1", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            Assert.IsTrue(!ord1.Equals(ord2));
            Assert.IsTrue(ord1 != ord2);
        }

        [TestMethod]
        public void OrderWrapperEquality_ComparisonBetweenDifIsActive_MustReturnTrue()
        {
            ord1 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            ord2 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = false, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            Assert.IsTrue(!ord1.Equals(ord2));
            Assert.IsTrue(ord1 != ord2);
        }

        [TestMethod]
        public void OrderWrapperEquality_ComparisonBetweenDifNumRobo_MustReturnTrue()
        {
            ord1 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            ord2 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "1", Status = OrderStatus.New, Type = OrdersType.Limit };
            Assert.IsTrue(!ord1.Equals(ord2));
            Assert.IsTrue(ord1 != ord2);
        }

        [TestMethod]
        public void OrderWrapperEquality_ComparisonBetweenDifOrderStatus_MustReturnTrue()
        {
            ord1 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            ord2 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.ConfirmedNew, Type = OrdersType.Limit };
            Assert.IsTrue(!ord1.Equals(ord2));
            Assert.IsTrue(ord1 != ord2);
        }

        [TestMethod]
        public void OrderWrapperEquality_ComparisonBetweenDifType_MustReturnTrue()
        {
            ord1 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            ord2 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Market };
            Assert.IsTrue(!ord1.Equals(ord2));
            Assert.IsTrue(ord1 != ord2);
        }


        [TestMethod]
        public void HashSet_TryingToAddSameIdDifferentPriceToHashSet_MustReturTrue()
        {
            ord1 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            ord2 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 2, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            hashTest.Add(ord1);
            hashTest.Add(ord2);            
            Assert.IsTrue(hashTest.Count == 2);
            Assert.IsTrue(hashTest.Contains(ord1));
            Assert.IsTrue(hashTest.Contains(ord2));
            Assert.IsTrue(ord1 != ord2);
        }

        [TestMethod]
        public void HashSet_TryingToAddSameIdDifferentAmountToHashSet_MustReturTrue()
        {
            ord1 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            ord2 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 2, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            hashTest.Add(ord1);
            hashTest.Add(ord2);
            Assert.IsTrue(hashTest.Count == 2);
            Assert.IsTrue(hashTest.Contains(ord1));
            Assert.IsTrue(hashTest.Contains(ord2));
            Assert.IsTrue(ord1 != ord2);
        }

        [TestMethod]
        public void OrderWrapperEquality_SameOrderElements_MustReturnTrue()
        {
            ord1 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
            ord2 = new OrderWrapper(roboMock.Object) { Id = "1", Amount = 1, Price = 1, Side = Operation.Buy, LinkedTo = "", IsActive = true, NumRobo = "0", Status = OrderStatus.New, Type = OrdersType.Limit };
                       
            Assert.IsTrue(ord1 == ord2);
        }

    }
}
