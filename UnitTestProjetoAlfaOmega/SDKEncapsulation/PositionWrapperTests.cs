﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgoGlauNamespace.SDKEncapsulation;
using PTLRuntime.NETScript;

namespace UnitTestProjectRobo.SDKEncapsulation
{
    [TestClass]
    public class PositionWrapperTests
    {
        private PositionWrapper pos1;
        private PositionWrapper pos2;

        [TestInitialize]
        public void Setup()
        {
            pos1 = new PositionWrapper();
            pos2 = new PositionWrapper();
        }

                      
        [DataTestMethod]
        [DataRow(-5, 100, Operation.Buy, 12, 200, Operation.Sell, 500, 200)] //fechando parcialmente
        [DataRow(-5, 200, Operation.Buy, 12, 100, Operation.Sell, -500, 100)]//fechando parcialmente
        [DataRow(-12, 100, Operation.Buy, 5, 200, Operation.Sell, 500, 100)] //fechando parcialmente
        [DataRow(-12, 200, Operation.Buy, 5, 100, Operation.Sell, -500, 200)]//fechando parcialmente
        [DataRow(5, 100, Operation.Sell, -12, 200, Operation.Buy, -500, 200)]//fechando parcialmente
        [DataRow(5, 200, Operation.Sell, -12, 100, Operation.Buy, 500, 100)] //fechando parcialmente
        [DataRow(12, 100, Operation.Sell, -5, 200, Operation.Buy, -500, 100)]//fechando parcialmente
        [DataRow(12, 200, Operation.Sell, -5, 100, Operation.Buy, 500, 200)] //fechando parcialmente
        [DataRow(10, 200, Operation.Sell, -10, 100, Operation.Buy, 1000, 0)] //fechando totalmente contratos
        [DataRow(10, 100, Operation.Sell, -10, 200, Operation.Buy, -1000, 0)]//fechando totalmente contratos
        [DataRow(-10, 200, Operation.Buy, 10, 100, Operation.Sell, -1000, 0)]//fechando operações
        [DataRow(-10, 100, Operation.Buy, 10, 200, Operation.Sell, 1000, 0)] //fechando operações
        [DataRow(-10, 100, Operation.Buy, -10, 200, Operation.Buy, 0, 150)]  //dobrando posições
        [DataRow(10, 100, Operation.Sell, 10, 200, Operation.Sell, 0, 150)]  //dobrando posições

        public void TesteComutativo(double qtde1, double price1, Operation op1, double qtde2, double price2, Operation op2, double LucroEsperado, double precoMedioEsperado)
        {
            PositionWrapper pos1 = new PositionWrapper() { Amount = qtde1, OpenPrice = price1, Side = op1 };
            PositionWrapper pos2 = new PositionWrapper() { Amount = qtde2, OpenPrice = price2, Side = op2 };
            PositionWrapper result = pos1 + pos2;
            Assert.IsTrue(result == pos2 + pos1);
            Assert.IsTrue(result.Lucro == (pos2 + pos1).Lucro, "Lucro não é comutativo");
            Assert.IsTrue(result.Lucro == LucroEsperado, "Lucro não é o esperado");
            Assert.IsTrue(result.OpenPrice == precoMedioEsperado, "Preço médio não é o esperado");
        }

        [TestMethod]
        public void TestePropaçãoDeLucro()
        {
            PositionWrapper pos1 = new PositionWrapper() { Amount = 10, OpenPrice = 100, Side = Operation.Sell };
            PositionWrapper pos2 = new PositionWrapper() { Amount = -5, OpenPrice = 200, Side = Operation.Buy };
            PositionWrapper pos3 = new PositionWrapper() { Amount = -4, OpenPrice = 100, Side = Operation.Buy };
            PositionWrapper pos4 = new PositionWrapper() { Amount = -1, OpenPrice = 200, Side = Operation.Buy };
            

            PositionWrapper result = pos1;
            result += pos2;
            result += pos3;
            result += pos4;
            double Lucro1 = result.Lucro??0;

            result = pos3;
            result += pos2;
            result += pos1;
            result += pos4;
            double Lucro2 = result.Lucro ?? 0;

            Assert.IsTrue(Math.Round(Lucro1,0) == Math.Round(Lucro2,0));

            result = (pos1 + pos2) + (pos3 + pos4);
            Assert.IsTrue(result == (pos1 + pos3) + (pos2 + pos4));

        }

    }
}
