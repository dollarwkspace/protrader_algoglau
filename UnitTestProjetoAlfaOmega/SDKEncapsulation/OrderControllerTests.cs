﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgoGlauNamespace;
using AlgoGlauNamespace.SDKEncapsulation;
using System.Collections.Generic;
using PTLRuntime.NETScript;
using System.Runtime.Serialization;
using Moq;
using System.Linq;
using System.Diagnostics;

namespace UnitTestProjectRobo.SDKEncapsulation
{
    [TestClass]
    public class OrdersControllerTests
    {

        private OrdersController orderController;        
        private Mock<ProjectAlgoGlau> roboMock;
        private HashSet<OrderWrapper> emptyHashSet = new HashSet<OrderWrapper>();
        private HashSet<OrderWrapper> oneElementHashSet = new HashSet<OrderWrapper>();
        private HashSet<OrderWrapper> oneElementHashSetWithDifferentAmount = new HashSet<OrderWrapper>();
        private HashSet<OrderWrapper> oneElementHashSetWithDifferentPrice = new HashSet<OrderWrapper>();
        private HashSet<OrderWrapper> oneElementHashSetWithDifferentSide = new HashSet<OrderWrapper>();
        private HashSet<OrderWrapper> oneElementHashSetDoubled = new HashSet<OrderWrapper>();
        private HashSet<OrderWrapper> anotherOneElementHashSet = new HashSet<OrderWrapper>();
        private HashSet<OrderWrapper> anotherOneElementHashSetWithDifferentAmount = new HashSet<OrderWrapper>();
        private HashSet<OrderWrapper> twoElementHashSet = new HashSet<OrderWrapper>();
        private Mock<IOrderRepository> orderRepoMock;


        [TestInitialize]
        public void Setup()
        {
            roboMock = new Mock<ProjectAlgoGlau>(MockBehavior.Loose);
            orderRepoMock = new Mock<IOrderRepository>(MockBehavior.Strict);            
            orderController = new OrdersController(orderRepoMock.Object);

            oneElementHashSet.Add(new OrderWrapper(roboMock.Object)
            {
                Id = "1",
                Amount = 1,
                IsActive = true,
                LinkedTo = "",
                NumRobo = "1",
                Price = 23,
                Status = OrderStatus.New,
                Side = Operation.Buy,
                Type = OrdersType.Limit
            });

            oneElementHashSetWithDifferentAmount.Add(new OrderWrapper(roboMock.Object)
            {
                Id = "1",
                Amount = 2,
                IsActive = true,
                LinkedTo = "",
                NumRobo = "1",
                Price = 23,
                Status = OrderStatus.New,
                Side = Operation.Buy,
                Type = OrdersType.Limit
            });

            oneElementHashSetWithDifferentPrice.Add(new OrderWrapper(roboMock.Object)
            {
                Id = "1",
                Amount = 1,
                IsActive = true,
                LinkedTo = "",
                NumRobo = "1",
                Price = 22,
                Status = OrderStatus.New,
                Side = Operation.Buy,
                Type = OrdersType.Limit
            });

            oneElementHashSetWithDifferentSide.Add(new OrderWrapper(roboMock.Object)
            {
                Id = "1",
                Amount = 1,
                IsActive = true,
                LinkedTo = "",
                NumRobo = "1",
                Price = 23,
                Status = OrderStatus.New,
                Side = Operation.Sell,
                Type = OrdersType.Limit
            });

            oneElementHashSetDoubled.Add(new OrderWrapper(roboMock.Object)
            {
                Id = "1",
                Amount = 1,
                IsActive = true,
                LinkedTo = "",
                NumRobo = "1",
                Price = 23,
                Status = OrderStatus.New,
                Side = Operation.Buy,
                Type = OrdersType.Limit
            });

            oneElementHashSetDoubled.Add(new OrderWrapper(roboMock.Object)
            {
                Id = "2",
                Amount = 1,
                IsActive = true,
                LinkedTo = "",
                NumRobo = "1",
                Price = 23,
                Status = OrderStatus.New,
                Side = Operation.Buy,
                Type = OrdersType.Limit
            });

            anotherOneElementHashSet.Add(new OrderWrapper(roboMock.Object)
            {
                Id = "1",
                Amount = 1,
                IsActive = true,
                LinkedTo = "",
                NumRobo = "1",
                Price = 23,
                Status = OrderStatus.New,
                Side = Operation.Buy,
                Type = OrdersType.Limit
            });

            anotherOneElementHashSetWithDifferentAmount.Add(new OrderWrapper(roboMock.Object)
            {
                Id = "1",
                Amount = 2,
                IsActive = true,
                LinkedTo = "",
                NumRobo = "1",
                Price = 23,
                Status = OrderStatus.New,
                Side = Operation.Buy,
                Type = OrdersType.Limit
            });

            twoElementHashSet.Add(new OrderWrapper(roboMock.Object)
            {
                Id = "1",
                Amount = 1,
                IsActive = true,
                LinkedTo = "",
                NumRobo = "1",
                Price = 23,
                Status = OrderStatus.New,
                Side = Operation.Buy,
                Type = OrdersType.Limit
            });
            twoElementHashSet.Add(new OrderWrapper(roboMock.Object)
            {
                Id = "2",
                Amount = 1,
                IsActive = true,
                LinkedTo = "",
                NumRobo = "1",
                Price = 24,
                Status = OrderStatus.New,
                Side = Operation.Sell,
                Type = OrdersType.Limit
            });
        }


        [TestMethod]
        public void UpdateOrders_OrderIsEmptyBufferHasOneElement_RaisesOnOrderAddedEvent()
        {
            //Arrange
            orderController.OrderAddedEvent += (ord) => Assert.AreEqual(oneElementHashSet.First(x => x.Id == ord.Id), ord);
            orderRepoMock.Setup(r => r.GetOrdersFromServer()).Returns(oneElementHashSet);

            //Act
            orderController.UpdateOrders();
            //Assert
        }

        [TestMethod]
        public void UpdateOrders_OrderHasOneElementBufferIsEmpty_RaisesOnOrderRemovedThenOnOrderEmptyEvent()
        {
            OrderWrapper expectedResult = oneElementHashSet.First();
            OrderWrapper returnedResult = null;
            bool orderEmptyCalled = false;
            //Arrange
            orderController.OrderRemovedEvent += (ord) => returnedResult = ord;
            orderController.OrderEmptyEvent += () => { orderEmptyCalled = true; };
            //Act
            orderRepoMock.SetupSequence(r => r.GetOrdersFromServer()).Returns(oneElementHashSet).Returns(emptyHashSet);
            orderController.UpdateOrders();
            orderController.UpdateOrders();

            //Assert
            Assert.IsTrue(orderEmptyCalled);
            Assert.AreEqual(expectedResult, returnedResult);
        }

        [TestMethod]
        public void UpdateOrders_OrderHasTwoElementsBufferIsEmpty_RaisesOnOrderRemoved2TimesThenOnOrderEmptyEvent()
        {
            OrderWrapper returnedResult = null;
            bool orderEmptyCalled = false;
            int orderRemovedCounter = 0;
            //Arrange
            orderController.OrderRemovedEvent += (ord) => { returnedResult = ord; orderRemovedCounter++; };
            orderController.OrderEmptyEvent += () => { orderEmptyCalled = true; };
            //Act
            orderRepoMock.SetupSequence(r => r.GetOrdersFromServer()).Returns(twoElementHashSet).Returns(emptyHashSet);
            orderController.UpdateOrders();
            orderController.UpdateOrders();
            //Assert
            Assert.IsTrue(orderRemovedCounter == 2);
            Assert.IsTrue(orderEmptyCalled);
        }

        [TestMethod]
        public void UpdateOrders_OrderHasTwoElementsBufferHasOne_RaisesOnOrderRemovedAndOnOrderEmptyEventIsNotCalled()
        {
            OrderWrapper returnedResult = null;
            bool orderEmptyCalled = false;
            int orderRemovedCounter = 0;
            //Arrange
            orderController.OrderRemovedEvent += (ord) => { returnedResult = ord; orderRemovedCounter++; };
            orderController.OrderEmptyEvent += () => { orderEmptyCalled = true; };
            //Act
            orderRepoMock.SetupSequence(r => r.GetOrdersFromServer()).Returns(twoElementHashSet).Returns(oneElementHashSet);
            orderController.UpdateOrders();
            orderController.UpdateOrders();
            //Assert            
            Assert.IsTrue(orderRemovedCounter == 1);
            Assert.IsFalse(orderEmptyCalled);
        }

        [TestMethod]
        public void UpdateOrders_OrderHasTwoElementsBufferHasTwoElements_NoEventShouldBeRaised()
        {
            bool orderEmptyCalled = false;
            bool orderRemovedCalled = false;

            //Arrange            
            orderController.OrderRemovedEvent += (ord) => { orderRemovedCalled = true; };
            orderController.OrderEmptyEvent += () => { orderEmptyCalled = true; };
            //Act
            orderRepoMock.SetupSequence(r => r.GetOrdersFromServer()).Returns(twoElementHashSet).Returns(twoElementHashSet);
            orderController.UpdateOrders();
            orderController.UpdateOrders();
            //Assert                        
            Assert.IsFalse(orderEmptyCalled);
            Assert.IsFalse(orderRemovedCalled);
        }

        [TestMethod]
        public void UpdateOrders_OrderHasOneElementAndItWillChangeAmount_OrderRemovedAndOrderAddedMustBeCalled()
        {
            OrderWrapper FirstAddedElement = oneElementHashSet.First();
            OrderWrapper lastAddedElement = oneElementHashSetWithDifferentAmount.First();                        
            bool firstElementAdded = false;

            //Arrange                        
            orderController.OrderAddedEvent += (ord) => { firstElementAdded = true; };
            
            //Adding 1st Element to the order SET
            orderRepoMock.SetupSequence(r => r.GetOrdersFromServer()).Returns(oneElementHashSet);
            orderController.UpdateOrders();

            //Act
            orderRepoMock.SetupSequence(r => r.GetOrdersFromServer()).Returns(oneElementHashSetWithDifferentAmount);
            orderController.UpdateOrders();
            //Assert                  
            Assert.IsFalse(orderController.orders.Contains(FirstAddedElement));
            Assert.IsTrue(orderController.orders.Contains(lastAddedElement));
            Assert.IsTrue(orderController.orders.Length == 1);
            Assert.IsTrue(firstElementAdded);
        }

        [TestMethod]
        public void UpdateOrders_CheckingOneElementEquality_OrdersCountShouldRemainOne()
        {
            //Arrange
            //Act
            orderRepoMock.SetupSequence(r => r.GetOrdersFromServer()).Returns(oneElementHashSet).Returns(anotherOneElementHashSet);
            orderController.UpdateOrders();
            orderController.UpdateOrders();
            //Assert
            Assert.IsTrue(orderController.orders.Length == 1);
            Assert.IsTrue(orderController.orders.ToHashSet<OrderWrapper>().SequenceEqual(oneElementHashSet));
        }

        [TestMethod]
        public void UpdateOrders_AnOrderHasBeenExecuted_RemovedMustBeTrueOrderExecutedMustBeTrue()
        {
            bool executedEvent = false;
            bool removedEvent = false;
            bool emptyEvent = false;
            //Arrange
            orderController.OrderExecutedEvent += (ord) => { executedEvent = true; };
            orderController.OrderRemovedEvent += (ord) => { removedEvent = true; };
            orderController.OrderEmptyEvent += () => { emptyEvent = true; };
            //Act
            orderRepoMock.SetupSequence(r => r.GetOrdersFromServer()).Returns(oneElementHashSet).Returns(emptyHashSet);
            orderController.UpdateOrders();
            orderController.UpdateOrders(oneElementHashSet.First().Id);

            //Assert            
            Assert.IsTrue(executedEvent);
            Assert.IsFalse(removedEvent);
            Assert.IsTrue(emptyEvent);

        }

        [TestMethod]
        public void UpdateOrders_AnOrderHasBeenAddedAndUpdateIsCalled2times()
        {
            bool executedEvent = false;
            bool removedEvent = false;
            bool addedEvent = false;
            bool emptyEvent = false;
            //Arrange
            orderController.OrderAddedEvent += (ord) => { addedEvent = true; };
            orderController.OrderExecutedEvent += (ord) => { executedEvent = true; };
            orderController.OrderRemovedEvent += (ord) => { removedEvent = true; };
            orderController.OrderEmptyEvent += () => { emptyEvent = true; };
            //Act
            orderRepoMock.Setup(r => r.GetOrdersFromServer()).Returns(oneElementHashSet);
            orderController.UpdateOrders();
            Assert.IsTrue(addedEvent);
            addedEvent = false;

            orderController.UpdateOrders();
            orderController.UpdateOrders();
            orderController.UpdateOrders();

            //Assert            
            Assert.IsFalse(addedEvent);
            Assert.IsFalse(executedEvent);
            Assert.IsFalse(removedEvent);
            Assert.IsFalse(emptyEvent);

        }

    }
}
