﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgoGlauNamespace.SDKEncapsulation;
using AlgoGlauNamespace.Timeline;
using AlgoGlauNamespace;
using PTLRuntime.NETScript;
using System.Timers;
using System.Globalization;

namespace UnitTestProjectRobo
{
    [TestClass]
    public class AppControlTests
    {
        private Mock<IOrderRepository> mockOrderRepo;
        private Mock<ProjectAlgoGlau> mockRobo = new Mock<ProjectAlgoGlau>();
        private Mock<Comando> mockComando;        
        private AppControl appControl;
        private HashSet<OrderWrapper> bufferDeOrdens = new HashSet<OrderWrapper>();
        private Timer timer = new Timer(1000);
        private Mock<IQuoteRepository> quoteRepoMock = new Mock<IQuoteRepository>();        
        private int Id = 0;
        private DateTime currentDT = DateTime.Parse("18/04/2019 8:00:00", new CultureInfo("pt-BR"));
        
        [TestInitialize]
        public void Setup()
        {
            mockOrderRepo = new Mock<IOrderRepository>();
            mockComando = new Mock<Comando>(mockRobo.Object);            
            mockRobo.SetupAllProperties();            
            mockRobo.Object.contratosIniciais = 1;                        
            mockRobo.Object.lucroDia = 0;
            mockRobo.Object.lossDia = 0;
            mockRobo.Object.stopGainFinanceiro = 10000;
            mockRobo.Object.stopLossFinanceiro = 10000;
            mockRobo.Object.stopPorCalorMaximo = 10000;
            mockRobo.Object.calorPorTimeline = 0;
            mockRobo.Object.numeroRobo = 1;
            mockRobo.Object.HoraDeInicio = new TimeSpan(9, 0, 0);
            mockRobo.Object.HoraDeTermino = new TimeSpan(15, 0, 0);
            mockRobo.Object.HoraDeTerminoBolsa = new TimeSpan(17, 55, 0);
            mockRobo.Object.lastQuote = new QuoteWrapper(mockRobo.Object, quoteRepoMock.Object);
            mockRobo.Object.comando = mockComando.Object;
            quoteRepoMock.Setup(p => p.GetAsk()).Returns(100);
            quoteRepoMock.Setup(p => p.GetBid()).Returns(100);
            quoteRepoMock.Setup(p => p.GetLast()).Returns(100);
            quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);            

            appControl = new AppControl(mockRobo.Object, mockOrderRepo.Object);
            mockComando.Setup(p => p.EnviaOrdem(It.IsAny<Operation>(), 0, It.IsAny<double>(), OrdersType.Market)).Callback(
                (Operation side, double price, double amount, OrdersType type) =>
                {                    
                    if (amount > 0 && side == Operation.Buy) amount = -amount;
                    if (amount < 0 && side == Operation.Sell) amount = -amount;
                    bufferDeOrdens.Add(new OrderWrapper(mockRobo.Object)
                    {
                        Amount = amount,
                        Id = Id.ToString(),
                        IsActive = true,
                        OpenTime = DateTime.Now,
                        Status = OrderStatus.New,
                        Price = mockRobo.Object.lastQuote.Last,
                        NumRobo = mockRobo.Object.numeroRobo.ToString(),                        
                        Side = side,
                        Type = type
                    });
                    mockOrderRepo.Setup(o => o.GetOrdersFromServer()).Returns(bufferDeOrdens.ToHashSet());
                    appControl.ordersController.UpdateOrders(); //ADICIONA A ORDEM
                    Id++;
                    timer.Enabled = false;
                }
                ).Returns(Id.ToString());

            timer.Elapsed += Timer_Elapsed;
            timer.AutoReset = true;
            timer.Enabled = false;

        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {            
            appControl.ordersController.UpdateOrders();
            timer.Enabled = false;            
        }

        [TestMethod]
        public void AppControl_RefreshDataTimeBefore9am_MustSetHorarioTerminouTrueAndInnerStateTermino()
        {            
            DateTime currentDT = DateTime.Parse("18/04/2019 19:59:59", new CultureInfo("pt-BR"));
            quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);            
         
            appControl.RefreshQuotation();
            Assert.IsTrue(mockRobo.Object.horarioTerminou==true);
            Assert.IsTrue(appControl.GetInnerStatesBuffer().Exists(x => x == InnerState.termino));
        }

        [TestMethod]
        public void AppControl_RefreshDataTime_MustCallOnBolsaEvents()
        {
            bool bolsaFechouEventCalled = false;
            bool horarioDeTerminoEventCalled = false;                        
            DateTime currentDT = DateTime.Parse("18/04/2019 10:00:01", new CultureInfo("pt-BR"));
            quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);
            
            appControl.BolsaAbriuEvent += () => {
                currentDT = DateTime.Parse("18/04/2019 15:01:01", new CultureInfo("pt-BR"));
                quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);
            };
            appControl.HorarioTerminouEvent += () => {
                currentDT = DateTime.Parse("18/04/2019 21:00:01", new CultureInfo("pt-BR"));
                quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);
                horarioDeTerminoEventCalled = true;                
            };
            appControl.BolsaFechouEvent += () =>
            {
                bolsaFechouEventCalled = true;
            };

            appControl.RefreshQuotation(); //BOLSA ABRIU EVENT WILL BE CALLED 
            appControl.RefreshQuotation(); //HORARIO DE TERMINO EVENT WILL BE CALLED
            appControl.RefreshQuotation(); //BOLSA FECHOU EVENT WILL BE CALLED
            
            //System.Threading.Thread.Sleep(1000);

            Assert.IsTrue(horarioDeTerminoEventCalled);
            Assert.IsTrue(bolsaFechouEventCalled);
        }

        [TestMethod]
        public void AppControl_OpenAPositionAndCloseItInProfit()
        {
            //PREPARE
            bool orderAddedEventCalled = false;            
            bool orderExecutedEventCalled = false;
            bool bolsaAbriuEventCalled = false;
            double lucro = 0;
            //ARRANGE
            DateTime currentDT = DateTime.Parse("18/04/2019 12:00:01", new CultureInfo("pt-BR"));            
            mockRobo.Object.numeroRobo = 1;
            quoteRepoMock.Setup(p => p.GetLast()).Returns(90);
            quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);

            appControl.BolsaAbriuEvent += () => bolsaAbriuEventCalled = true;

            appControl.OrderAddedEvent += (OrderWrapper ord) => {
                orderAddedEventCalled = true;                
                bufferDeOrdens.Clear();
                mockOrderRepo.Setup(o => o.GetOrdersFromServer()).Returns(bufferDeOrdens.ToHashSet());
                appControl.ordersController.UpdateOrders(ord.Id);
            };
            appControl.OrderExecutedEvent += (OrderWrapper ord) => orderExecutedEventCalled = true;
            appControl.timelineController.TimelineFechouNoLucroEvent += (double lucroR) => lucro = lucroR;

            //ACT && ASSERT
            appControl.RefreshQuotation();
            Assert.IsTrue(bolsaAbriuEventCalled);

            //Adicione uma Ordem e a execute criando uma posição
            mockRobo.Object.comando.EnviaOrdem(Operation.Buy, 0, 1, OrdersType.Market);                                                

            Assert.IsTrue(orderAddedEventCalled);
            Assert.IsTrue(orderExecutedEventCalled);
            orderAddedEventCalled = false;
            orderExecutedEventCalled = false;

            quoteRepoMock.Setup(p => p.GetLast()).Returns(110);
            appControl.RefreshQuotation(); //vai atualizar a cotação atual e calcular o ponto médio
            Assert.IsTrue(appControl.PontosDoMedio == 20);

            currentDT = DateTime.Parse("18/04/2019 21:00:01", new CultureInfo("pt-BR"));
            quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);            
            appControl.RefreshQuotation(); // vai atualizar o horário e ver que a bolsa vai fechar ..uma ordem de fechamento será emitida e executada 
            Assert.IsTrue(lucro == 20);
        }

        [TestMethod]
        public void AppControl_OpenAPositionAndCloseItInLoss()
        {
            //PREPARE
            bool orderAddedEventCalled = false;
            bool orderExecutedEventCalled = false;
            bool bolsaAbriuEventCalled = false;
            double lucro = 0;
            //ARRANGE
            DateTime currentDT = DateTime.Parse("18/04/2019 12:00:01", new CultureInfo("pt-BR"));
            mockRobo.Object.numeroRobo = 1;
            quoteRepoMock.Setup(p => p.GetLast()).Returns(100);
            quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);

            appControl.BolsaAbriuEvent += () => bolsaAbriuEventCalled = true;

            appControl.OrderAddedEvent += (OrderWrapper ord) => {
                orderAddedEventCalled = true;
                bufferDeOrdens.Clear();
                mockOrderRepo.Setup(o => o.GetOrdersFromServer()).Returns(bufferDeOrdens.ToHashSet());
                appControl.ordersController.UpdateOrders(ord.Id);
            };
            appControl.OrderExecutedEvent += (OrderWrapper ord) => orderExecutedEventCalled = true;
            appControl.timelineController.TimelineFechouNoPrejuizoEvent += (double prejuizo) => lucro = prejuizo;

            //ACT && ASSERT
            appControl.RefreshQuotation();
            Assert.IsTrue(bolsaAbriuEventCalled);

            //Adicione uma Ordem e a execute criando uma posição
            mockRobo.Object.comando.EnviaOrdem(Operation.Buy, 0, 1, OrdersType.Market);

            Assert.IsTrue(orderAddedEventCalled);
            Assert.IsTrue(orderExecutedEventCalled);
            orderAddedEventCalled = false;
            orderExecutedEventCalled = false;

            quoteRepoMock.Setup(p => p.GetLast()).Returns(80);
            appControl.RefreshQuotation(); //vai atualizar a cotação atual e calcular o ponto médio
            Assert.IsTrue(appControl.PontosDoMedio == 20);

            currentDT = DateTime.Parse("18/04/2019 21:00:01", new CultureInfo("pt-BR"));
            quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);
            appControl.RefreshQuotation(); // vai atualizar o horário e ver que a bolsa vai fechar ..uma ordem de fechamento será emitida e executada 
            Assert.IsTrue(lucro == -20);
        }

        [DataTestMethod]
        [DataRow(Operation.Buy, 80)]
        [DataRow(Operation.Sell, 120)]
        public void AppControl_OpenAPositionEajusteCotacaoAcimaDoLossMaximo_SaidaPorLossMaximoEventMustBeCalled(Operation ordemInicial, double cotacaoSaida)
        {
            //PREPARE
            bool orderAddedEventCalled = false;
            bool orderExecutedEventCalled = false;
            bool bolsaAbriuEventCalled = false;
            bool saidaPorLossMaximoCalled = false;
            double lucro = 0;
            //ARRANGE
            DateTime currentDT = DateTime.Parse("18/04/2019 12:00:01", new CultureInfo("pt-BR"));
            mockRobo.Object.numeroRobo = 1;
            mockRobo.Object.lossMaximo = 10;
            quoteRepoMock.Setup(p => p.GetLast()).Returns(100);
            quoteRepoMock.Setup(p => p.GetTime()).Returns(currentDT);

            appControl.BolsaAbriuEvent += () => bolsaAbriuEventCalled = true;

            appControl.OrderAddedEvent += (OrderWrapper ord) => {
                orderAddedEventCalled = true;
                bufferDeOrdens.Clear();
                mockOrderRepo.Setup(o => o.GetOrdersFromServer()).Returns(bufferDeOrdens.ToHashSet());
                appControl.ordersController.UpdateOrders(ord.Id);
            };
            appControl.OrderExecutedEvent += (OrderWrapper ord) => orderExecutedEventCalled = true;
            appControl.timelineController.TimelineFechouNoPrejuizoEvent += (double prejuizo) => lucro = prejuizo;
            appControl.SaidaPorLossMaximoEvent += () => { saidaPorLossMaximoCalled = true; };

            //ACT && ASSERT
            appControl.RefreshQuotation();
            Assert.IsTrue(bolsaAbriuEventCalled);

            //Adicione uma Ordem e a execute criando uma posição
            mockRobo.Object.comando.EnviaOrdem(ordemInicial, 0, 1, OrdersType.Market);

            Assert.IsTrue(orderAddedEventCalled);
            Assert.IsTrue(orderExecutedEventCalled);
            orderAddedEventCalled = false;
            orderExecutedEventCalled = false;

            quoteRepoMock.Setup(p => p.GetLast()).Returns(cotacaoSaida);
            appControl.RefreshQuotation(); //vai atualizar a cotação atual, calcular o ponto médio e ver que ele está além do loss máximo
            Assert.IsTrue(appControl.PontosDoMedio == 20);
            Assert.IsTrue(saidaPorLossMaximoCalled);            
        }



    }
}
