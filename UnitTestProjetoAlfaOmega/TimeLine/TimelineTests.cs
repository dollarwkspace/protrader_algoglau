﻿using AlgoGlauNamespace.SDKEncapsulation;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgoGlauNamespace;
using AlgoGlauNamespace.Timeline;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PTLRuntime.NETScript;

namespace UnitTestProjectRobo
{
    [TestClass]
    public class TimelineTests
    {
        private Timeline timeline = new Timeline();       

        [TestMethod] public void GetLastTimelinePair_EmptyTimeline_MustReturnNull() => Assert.IsTrue(ReferenceEquals(timeline.GetLastActiveTimeLinePair(), null));
        [TestMethod] public void GetAllItems_EmptyTimeline_MustReturnNull() => Assert.IsTrue(timeline.GetAllItems().Count == 0);
        [TestMethod] public void GetActiveItems_EmptyTimeline_MustReturnNull() => Assert.IsTrue(timeline.GetActiveItems().Count == 0);
        [TestMethod] public void GetLastActiveTimelinePair_EmptyTimeline_MustReturnNull() => Assert.IsTrue(ReferenceEquals(timeline.GetLastActiveTimeLinePair(), null));
        [TestMethod] public void GetLastActiveItem_EmptyTimeline_MustReturnNull() => Assert.IsTrue(ReferenceEquals(timeline.GetLastActiveItem(), null));
        [TestMethod] public void AddToTimeline_EmptyTimelineAddingInvalidElement_MustReturnFalse() => Assert.IsTrue(timeline.AddToTimeline(new PositionWrapper()) == false);
        [TestMethod]
        public void AddToTimeline_EmptyTimelineAddingValidElement_MustReturnTrue()
        {
            PositionWrapper el = new PositionWrapper() { Amount = -1, Id = "0", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy };
            Assert.IsTrue(timeline.AddToTimeline(el));
            Assert.IsTrue(timeline.GetLastActiveItem() == el);
            Assert.IsTrue(timeline.GetLastActiveTimeLinePair().Status == TimelinePairStatus.open);
        }

        [TestMethod]
        public void AddToTimeline_TimelineLineWitOneElementAddingValidElementSameSide_MustReturnSize2()
        {
            //Arrange
            PositionWrapper el = new PositionWrapper() { Amount = -1, Id = "0", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy };
            PositionWrapper e2 = new PositionWrapper() { Amount = -5, Id = "2", NumRobo = "0", OpenPrice = 200, Side = Operation.Buy };

            //Act
            Assert.IsTrue(timeline.AddToTimeline(el));
            Assert.IsTrue(timeline.AddToTimeline(e2));

            //Assert
            Assert.IsTrue(timeline.GetActiveItems().Count == 2);
            Assert.IsTrue(timeline.GetLastActiveItem() == e2);
            Assert.IsTrue(timeline.GetLastActiveTimeLinePair().Status == TimelinePairStatus.open);
        }

        [TestMethod]
        public void AddToTimeline_TimelineLineWitOneElementAddingValidElementDifferentSideSameAmount_MustReturnClosedStatus()
        {
            List<TimeLinePair> tlPair = new List<TimeLinePair>();
            //Arrange
            PositionWrapper el1 = new PositionWrapper() { Amount = -1, Id = "0", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy };
            PositionWrapper el2 = new PositionWrapper() { Amount = 1, Id = "2", NumRobo = "0", OpenPrice = 200, Side = Operation.Sell };

            timeline.TimelineClosedEvent += (List<TimeLinePair> obj) =>
            {
                tlPair = new List<TimeLinePair>(obj);
            };

            //Act
            Assert.IsTrue(timeline.AddToTimeline(el1));
            Assert.IsTrue(timeline.AddToTimeline(el2));

            //Assert
            Assert.IsTrue(timeline.GetAllItems().Count == 0);
            Assert.IsTrue(tlPair.Count == 1);
            Assert.IsTrue(tlPair.First().Status == TimelinePairStatus.closed);
            Assert.IsTrue(tlPair.First().Entrada.Equals(el1));
            Assert.IsTrue(tlPair.First().Saidas[0].Equals(el2));
            Assert.IsTrue(tlPair.First().Saida.Equals(el2));
            Assert.IsTrue(tlPair.First().Saida.Amount == 1);
        }

        [TestMethod]
        public void AddToTimeline_TimelineLineWitOneElementAdding2ValidElementsDifferentSideAmountSumEqualsEntrada_MustReturnClosedStatus()
        {
            List<TimeLinePair> tlPair = new List<TimeLinePair>();
            //Arrange
            PositionWrapper el1 = new PositionWrapper() { Amount = -10, Id = "0", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy };
            PositionWrapper el2 = new PositionWrapper() { Amount = 6, Id = "2", NumRobo = "0", OpenPrice = 200, Side = Operation.Sell };
            PositionWrapper el3 = new PositionWrapper() { Amount = 4, Id = "3", NumRobo = "0", OpenPrice = 300, Side = Operation.Sell };

            timeline.TimelineClosedEvent += (List<TimeLinePair> obj) =>
            {
                tlPair = new List<TimeLinePair>(obj);
            };

            //Act
            Assert.IsTrue(timeline.AddToTimeline(el1));
            Assert.IsTrue(timeline.AddToTimeline(el2));
            Assert.IsTrue(timeline.AddToTimeline(el3));

            List<TimeLinePair> result = timeline.GetActiveItems();
            //Assert
            Assert.IsTrue(result?.Count == 0);
            Assert.IsTrue(tlPair.Count == 1);
            Assert.IsTrue(tlPair.Last().Saidas.Count == 2);
            Assert.IsTrue(tlPair.First().Status == TimelinePairStatus.closed);
            Assert.IsTrue(tlPair.First().Entrada.Equals(el1));
            Assert.IsTrue(tlPair.First().Saidas[0].Equals(el2));
            Assert.IsTrue(tlPair.First().Saidas[1].Equals(el3));
            Assert.IsTrue(tlPair.First().Saida.Amount == 10);
            Assert.IsTrue(tlPair.First().Saida == (el2+el3));
            Assert.IsTrue(tlPair.First().Saida.OpenPrice == 240);
        }

        [TestMethod]
        public void AddToTimeline_TimelineLineWitOneElementAdding2ValidElementsDifferentSideAmountSumsLessThanEntrada_MustReturnOpenStatus()
        {
            List<TimeLinePair> tlPair = new List<TimeLinePair>();
            bool timelineclosedcalled = false;
            //Arrange
            PositionWrapper el1 = new PositionWrapper() { Amount = -11, Id = "0", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy };
            PositionWrapper el2 = new PositionWrapper() { Amount = 6, Id = "2", NumRobo = "0", OpenPrice = 200, Side = Operation.Sell };
            PositionWrapper el3 = new PositionWrapper() { Amount = 4, Id = "3", NumRobo = "0", OpenPrice = 300, Side = Operation.Sell };

            timeline.TimelineClosedEvent += (List<TimeLinePair> obj) =>
            {
                tlPair = new List<TimeLinePair>(obj);
            };

            //Act
            Assert.IsTrue(timeline.AddToTimeline(el1));
            Assert.IsTrue(timeline.AddToTimeline(el2));
            Assert.IsTrue(timeline.AddToTimeline(el3));

            if (tlPair.Count == 0) tlPair = new List<TimeLinePair>(timeline.GetAllItems());
            //Assert
            Assert.IsTrue(timelineclosedcalled == false);
            Assert.IsTrue(timeline.GetAllItems().Count == 1);
            Assert.IsTrue(tlPair.Count == 1);
            Assert.IsTrue(tlPair.Last().Saidas.Count == 2);
            Assert.IsTrue(tlPair.Last().Status == TimelinePairStatus.open);
            Assert.IsTrue(tlPair.Last().Entrada.Equals(el1));
            Assert.IsTrue(tlPair.Last().Saidas[0].Equals(el2));
            Assert.IsTrue(tlPair.Last().Saidas[1].Equals(el3));
            Assert.IsTrue(tlPair.Last().Saida.Amount == 10);
            Assert.IsTrue(tlPair.Last().Saida.OpenPrice == 240);
        }

        [TestMethod]
        public void AddToTimeline_TimelineLineWitOneElementAdding2ValidElementsDifferentSideAmountSumsHigherThanEntrada_MustReturn2TimeLinePairs()
        {
            List<TimeLinePair> tlPair = new List<TimeLinePair>();
            bool timelineclosedcalled = false;
            //Arrange
            PositionWrapper el1 = new PositionWrapper() { Amount = -11, Id = "0", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy };
            PositionWrapper el2 = new PositionWrapper() { Amount = 6, Id = "2", NumRobo = "0", OpenPrice = 200, Side = Operation.Sell };
            PositionWrapper el3 = new PositionWrapper() { Amount = 10, Id = "3", NumRobo = "0", OpenPrice = 300, Side = Operation.Sell };

            timeline.TimelineClosedEvent += (List<TimeLinePair> obj) =>
            {
                tlPair = new List<TimeLinePair>(obj);
            };

            //Act
            Assert.IsTrue(timeline.AddToTimeline(el1));
            Assert.IsTrue(timeline.AddToTimeline(el2));
            Assert.IsTrue(timeline.AddToTimeline(el3));

            if (tlPair.Count == 0) tlPair = new List<TimeLinePair>(timeline.GetAllItems());
            //Assert
            Assert.IsTrue(timelineclosedcalled == false);
            Assert.IsTrue(tlPair.Count == 2);
            Assert.IsTrue(tlPair.First().Entrada == el1);
            Assert.IsTrue(tlPair.First().Saida == el2);
            Assert.IsTrue(tlPair.Last().Entrada == el3);
            Assert.IsTrue(timelineclosedcalled == false);
        }

        [TestMethod]
        public void AddToTimeline_2TimelinesClosing_MustReturnZeroActiveElements()
        {

            //Arrange
            PositionWrapper el1 = new PositionWrapper() { Amount = -11, Id = "0", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy };
            PositionWrapper el2 = new PositionWrapper() { Amount = 6, Id = "1", NumRobo = "0", OpenPrice = 200, Side = Operation.Sell };
            PositionWrapper el3 = new PositionWrapper() { Amount = 10, Id = "2", NumRobo = "0", OpenPrice = 300, Side = Operation.Sell }; //tryied to close but will open another timelinePair
            PositionWrapper el4 = new PositionWrapper() { Amount = -10, Id = "3", NumRobo = "0", OpenPrice = 200, Side = Operation.Buy }; // close the last timelinepair
            PositionWrapper el5 = new PositionWrapper() { Amount = 5, Id = "4", NumRobo = "0", OpenPrice = 101, Side = Operation.Sell }; // close the first timelinepair

            timeline.TimelineClosedEvent += (obj) =>
            {
                PositionWrapper pMedia = timeline.GetPosicaoMedia();
                PositionWrapper result = (el1 + el2 + el3 + el4 + el5);
                Assert.IsTrue(pMedia == result);
            };

            //Act
            Assert.IsTrue(timeline.AddToTimeline(el1));
            Assert.IsTrue(timeline.AddToTimeline(el2));
            Assert.IsTrue(timeline.AddToTimeline(el3));
            Assert.IsTrue(timeline.AddToTimeline(el4));
            Assert.IsTrue(timeline.AddToTimeline(el5));

            //Assert
            Assert.IsTrue(timeline.GetActiveItems()?.Count == 0);
        }

        [DataTestMethod]
        [DataRow(50)]
        [DataRow(100)]
        [DataRow(150)]
        public void AddToTimeline_OneElementTimeline_MustReturnSameElementAsPosicaoMedia(double cotacaoAtual)
        {
            //Arrange
            PositionWrapper el1 = new PositionWrapper() { Amount = 11, Id = "0", NumRobo = "0", OpenPrice = 100, Side = Operation.Sell };
            PositionWrapper pMedia = new PositionWrapper();
            //Act
            Assert.IsTrue(timeline.AddToTimeline(el1));
            pMedia = timeline.GetPosicaoMedia();
            //Assert
            Assert.IsTrue(timeline.GetActiveItems()?.Count == 1);
            Assert.IsTrue(pMedia == el1);

        }

        [DataTestMethod]
        [DataRow(50)]
        [DataRow(100)]
        [DataRow(150)]
        public void AddToTimeline_OneElementTimeline_MustReturnCalculatedPosicaoMedia(double cotacaoAtual)
        {
            //Arrange
            PositionWrapper el1 = new PositionWrapper() { Amount = 10, Id = "0", NumRobo = "0", OpenPrice = 100, Side = Operation.Sell };
            PositionWrapper el2 = new PositionWrapper() { Amount = -5, Id = "1", NumRobo = "0", OpenPrice = 50, Side = Operation.Buy };
            PositionWrapper el3 = new PositionWrapper() { Amount = 5, Id = "2", NumRobo = "0", OpenPrice = 75, Side = Operation.Sell };

            PositionWrapper pMedia = new PositionWrapper();
            //Act
            Assert.IsTrue(timeline.AddToTimeline(el1));
            Assert.IsTrue(timeline.AddToTimeline(el2));
            Assert.IsTrue((el1 + el2).Lucro == 250);
            Assert.IsTrue(timeline.AddToTimeline(el3));
            pMedia = timeline.GetPosicaoMedia();
            //Assert
            Assert.IsTrue(timeline.GetActiveItems()?.Count == 2);
            Assert.IsTrue(pMedia == el1 + el2 + el3);
        }

        [TestMethod]
        public void AddToTimeline_OneElementTimelineWithMultipleSaidas_MustReturnCalculatedPosicaoMedia()
        {
            //Arrange
            PositionWrapper el1 = new PositionWrapper() { Amount = 10, Id = "0", NumRobo = "0", OpenPrice = 100, Side = Operation.Sell };
            PositionWrapper el2 = new PositionWrapper() { Amount = -5, Id = "0", NumRobo = "0", OpenPrice = 50, Side = Operation.Buy };
            PositionWrapper el3 = new PositionWrapper() { Amount = -1, Id = "0", NumRobo = "0", OpenPrice = 70, Side = Operation.Buy };
            PositionWrapper el4 = new PositionWrapper() { Amount = -3, Id = "0", NumRobo = "0", OpenPrice = 90, Side = Operation.Buy };

            PositionWrapper pMedia, pResult;
            //Act
            Assert.IsTrue(timeline.AddToTimeline(el1));
            Assert.IsTrue(timeline.AddToTimeline(el2));
            Assert.IsTrue(timeline.AddToTimeline(el3));
            Assert.IsTrue(timeline.AddToTimeline(el4));
            pMedia = timeline.GetPosicaoMedia();
            pResult = el1 + el2 + el3 + el4;
            //Assert            
            Assert.IsTrue(pMedia == pResult);
            Assert.IsTrue(pMedia == (el1 + el2 + el3 + el4));
        }

        [TestMethod]
        public void ForceTimelineToClose_TimelineForcedToCloseCalled_MustEmitOnTimeLineCreatedOrderToCloseEvent()
        {
            PositionWrapper el1 = new PositionWrapper() { Amount = 10, Id = "0", NumRobo = "0", OpenPrice = 100, Side = Operation.Sell };
            PositionWrapper el2 = new PositionWrapper() { Amount = -5, Id = "0", NumRobo = "0", OpenPrice = 50, Side = Operation.Buy };
            PositionWrapper el3 = new PositionWrapper() { Amount = 5, Id = "0", NumRobo = "0", OpenPrice = 70, Side = Operation.Sell };
            PositionWrapper el4 = new PositionWrapper() { Amount = -3, Id = "0", NumRobo = "0", OpenPrice = 50, Side = Operation.Buy };
            Assert.IsTrue(timeline.AddToTimeline(el1));
            Assert.IsTrue(timeline.AddToTimeline(el2));
            Assert.IsTrue(timeline.AddToTimeline(el3));
            Assert.IsTrue(timeline.AddToTimeline(el4));

            List<PositionWrapper> result = new List<PositionWrapper>();

            timeline.TimelineClosedEvent += (List<TimeLinePair> closedTl) => 
            {
                PositionWrapper posicaoFinal = timeline.GetPosicaoMedia(closedTl);
                Assert.IsTrue(posicaoFinal.Lucro == (250 - 500 + 60 - 260));
            };

            timeline.TimelineCreatedOrderToCloseEvent += (PositionWrapper pos) => 
            {
                result.Add(pos);
                //SIMULANDO COMO SE ORDEM FOSSE ENVIADA E VALOR TIVESSE RETORNADO
                pos.OpenPrice = 200;
                timeline.AddToTimeline(pos);
            };
            timeline.ForceTimelineToClose();  

            Assert.IsTrue(result.Count == 2);
            Assert.IsTrue(timeline.GetActiveItems().Count == 0);
            
        }

    }




}
