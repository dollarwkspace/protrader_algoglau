﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTLRuntime.NETScript;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgoGlauNamespace;
using AlgoGlauNamespace.SDKEncapsulation;
using AlgoGlauNamespace.Timeline;

namespace UnitTestProjectRobo.TimeLine
{
    [TestClass]
    public class TimelineControllerTests
    {
        private Mock<ProjectAlgoGlau> roboMock = new Mock<ProjectAlgoGlau>();
        private Mock<AppControl> appMock;
        private Mock<IQuoteRepository> quoteMock = new Mock<IQuoteRepository>();
        private Mock<IOrderRepository> orderRepoMock;
        private Timeline timeline = new Timeline();
        private TimelineController tlController;
        private List<InnerState> innerStates = new List<InnerState>();        

        [TestInitialize]
        public void Setup()
        {
            tlController = new TimelineController(roboMock.Object);
            orderRepoMock = new Mock<IOrderRepository>();
            appMock = new Mock<AppControl>(roboMock.Object, orderRepoMock.Object);
            roboMock.SetupAllProperties();                        
            roboMock.Object.contratosIniciais = 1;                        
            roboMock.Object.lucroDia = 0;
            roboMock.Object.lossDia = 0;
            roboMock.Object.stopGainFinanceiro = 10000;
            roboMock.Object.stopLossFinanceiro = 10000;
            roboMock.Object.stopPorCalorMaximo = 10000;
            roboMock.Object.calorPorTimeline = 0;
            roboMock.Object.numeroRobo = 1;
            roboMock.Object.lastQuote = new QuoteWrapper(roboMock.Object, quoteMock.Object);
            roboMock.Object.mainControl = appMock.Object;
            roboMock.Setup(p => p.GetConnectionStatus()).Returns(PTLRuntime.NETScript.Application.ConnectionStatus.Disconnected);
            appMock.Setup(a => a.AddToInnerStatesBuffer(It.IsAny<InnerState>())).Callback((InnerState el) => innerStates.Add(el));
            appMock.Setup(a => a.RemoveFromInnerStatesBuffer(It.IsAny<InnerState>())).Callback((InnerState el) => innerStates.Remove(el));            
            quoteMock.Setup(p => p.GetAsk()).Returns(100);
            quoteMock.Setup(p => p.GetBid()).Returns(100);
            quoteMock.Setup(p => p.GetLast()).Returns(100);
            quoteMock.Setup(p => p.GetTime()).Returns(DateTime.Now);


        }

        [TestMethod]
        public void TimeLineController_AddToTimelineOneElement_MustReturnTimelineWithOneElement()
        {            
            tlController.AddToTimeline(new OrderWrapper(roboMock.Object)
            {
                Amount=-1, Price=100, NumRobo=roboMock.Object.numeroRobo.ToString(), Side=Operation.Buy, Status=OrderStatus.New, Type=OrdersType.Limit, Id="0", IsActive=true, OpenTime=DateTime.Now
            });
            Assert.IsTrue(tlController.AllItems.Count == 1);
            Assert.IsTrue(tlController.PosicaoMedia.Amount == -1);
            Assert.IsTrue(tlController.PosicaoMedia.OpenPrice == 100);
            Assert.IsTrue(tlController.PosicaoMedia.Side == Operation.Buy);
        }

        [DataTestMethod]
        [DataRow(80,-40)]
        [DataRow(120,40)]
        public void TimeLineController_ForceTimelineToCloseOnProfitAndLoss_SeveralThingsMustHappen(double cotacaoAtual, double lucroEsperado)
        {
            //on timeline fechou no lucro event must be called
            //timelineCounter must increase by one
            //Calor por timeline = 0
            //lucro dia += lucro dia
            //timeline count = 0

            //Arrange
            OrderWrapper ordEntrada = new OrderWrapper(roboMock.Object) { Amount = -2, Price = 100, NumRobo = roboMock.Object.numeroRobo.ToString(), Side = Operation.Buy, Status = OrderStatus.New, Type = OrdersType.Limit, Id = "0", IsActive = true, OpenTime = DateTime.Now };
            tlController.AddToTimeline(ordEntrada);
            quoteMock.Setup(p => p.GetLast()).Returns(cotacaoAtual);

            tlController.OpenOrderRequestedEvent += (double amount, Operation op) =>
             {
                 OrderWrapper ordSaida = new OrderWrapper(roboMock.Object) { Amount = amount, Side = op, Price=cotacaoAtual, Id="1", NumRobo=roboMock.Object.numeroRobo.ToString(), Status=OrderStatus.New, Type=OrdersType.Limit, IsActive=true, OpenTime=DateTime.Now };
                 tlController.AddToTimeline(ordSaida);
             };

            tlController.TimelineFechouNoLucroEvent += (double lucro) => 
            {
                Assert.IsTrue(lucro == 40, $"Erro Lucro: {lucro}");
                Assert.IsTrue(tlController.LucroDia == 40, $"Erro Lucro Dia:{tlController.LucroDia}");
                Assert.IsTrue(tlController.CalorPorTimeline == 0);                
            };

            tlController.TimelineFechouNoPrejuizoEvent += (double prejuizo) =>
            {
                Assert.IsTrue(prejuizo == -40, $"Erro Prejuízo : {prejuizo}");
                Assert.IsTrue(tlController.PerdasDia == -40, $"Erro Perdas Dia:{tlController.PerdasDia}");
                Assert.IsTrue(tlController.CalorPorTimeline == 0);
                
            };

            tlController.ForceTimelineToClose();
            Assert.IsTrue(tlController.PosicaoMedia.Amount == 0, "Posição Média Final errada!");
            Assert.IsTrue(tlController.TimelineCounter == 1);
        }

        [TestMethod]
        public void TimeLineController_FechamentoPorCalorMáximo()
        {
            double cotacaoAtual = 80;
            quoteMock.Setup(p => p.GetLast()).Returns(cotacaoAtual);
            roboMock.Object.stopPorCalorMaximo = 39;
            //Arrange
            OrderWrapper ordEntrada = new OrderWrapper(roboMock.Object) { Amount = -2, Price = 100, NumRobo = roboMock.Object.numeroRobo.ToString(), Side = Operation.Buy, Status = OrderStatus.New, Type = OrdersType.Limit, Id = "0", IsActive = true, OpenTime = DateTime.Now };
            tlController.AddToTimeline(ordEntrada);            

            tlController.OpenOrderRequestedEvent += (double amount, Operation op) =>
            {
                OrderWrapper ordSaida = new OrderWrapper(roboMock.Object) { Amount = amount, Side = op, Price = cotacaoAtual, Id = "1", NumRobo = roboMock.Object.numeroRobo.ToString(), Status = OrderStatus.New, Type = OrdersType.Limit, IsActive = true, OpenTime = DateTime.Now };
                tlController.AddToTimeline(ordSaida);
            };
            
            tlController.TimelineFechouNoPrejuizoEvent += (double prejuizo) =>
            {
                Assert.IsTrue(prejuizo == -40, $"Erro Prejuízo : {prejuizo}");
                Assert.IsTrue(tlController.PerdasDia == -40, $"Erro Perdas Dia:{tlController.PerdasDia}");
                Assert.IsTrue(tlController.CalorPorTimeline == -40);
            };

            tlController.FechamentoPorCalorMaximoEvent += (double calor) => Assert.IsTrue(calor == 40);                

            PositionWrapper pMedia = tlController.PosicaoMedia;
            
            Assert.IsTrue(tlController.TimelineCounter == 1);
            Assert.IsTrue(innerStates.Exists(x => x ==InnerState.lockdownPorCalorMaximo));
        }

        [TestMethod]
        public void TimeLineController_EstouroDeLucro_MustCallOnEstouroDeLucroEvent()
        {
            roboMock.Object.stopGainFinanceiro=1;
            double cotacaoAtual = 102;
            quoteMock.Setup(p => p.GetLast()).Returns(cotacaoAtual);
            bool estouroDeLucro = false;

            //Arrange
            tlController.EstouroDeLucroEvent += () => estouroDeLucro = true;
            tlController.OpenOrderRequestedEvent += (double amount, Operation op) =>
            {
                OrderWrapper ordSaida = new OrderWrapper(roboMock.Object) { Amount = amount, Side = op, Price = cotacaoAtual, Id = "1", NumRobo = roboMock.Object.numeroRobo.ToString(), Status = OrderStatus.New, Type = OrdersType.Limit, IsActive = true, OpenTime = DateTime.Now };
                tlController.AddToTimeline(ordSaida);
            };

            OrderWrapper ordEntrada = new OrderWrapper(roboMock.Object) { Amount = -1, Price = 100, NumRobo = roboMock.Object.numeroRobo.ToString(), Side = Operation.Buy, Status = OrderStatus.New, Type = OrdersType.Limit, Id = "0", IsActive = true, OpenTime = DateTime.Now };
            tlController.AddToTimeline(ordEntrada);
            tlController.ForceTimelineToClose();            
            Assert.IsTrue(estouroDeLucro);
        }


        [TestMethod]
        public void TimeLineController_EstouroDePerdas_MustCallOnEstouroDePerdasEvent()
        {
            roboMock.Object.stopLossFinanceiro = 1;
            double cotacaoAtual = 98;
            quoteMock.Setup(p => p.GetLast()).Returns(cotacaoAtual);
            bool estouroDePerdas = false;

            //Arrange
            tlController.EstouroDePerdasEvent += () => estouroDePerdas = true;
            tlController.OpenOrderRequestedEvent += (double amount, Operation op) =>
            {
                OrderWrapper ordSaida = new OrderWrapper(roboMock.Object) { Amount = amount, Side = op, Price = cotacaoAtual, Id = "1", NumRobo = roboMock.Object.numeroRobo.ToString(), Status = OrderStatus.New, Type = OrdersType.Limit, IsActive = true, OpenTime = DateTime.Now };
                tlController.AddToTimeline(ordSaida);
            };

            OrderWrapper ordEntrada = new OrderWrapper(roboMock.Object) { Amount = -1, Price = 100, NumRobo = roboMock.Object.numeroRobo.ToString(), Side = Operation.Buy, Status = OrderStatus.New, Type = OrdersType.Limit, Id = "0", IsActive = true, OpenTime = DateTime.Now };
            tlController.AddToTimeline(ordEntrada);
            tlController.ForceTimelineToClose();
            Assert.IsTrue(estouroDePerdas);
        }
    }

}
