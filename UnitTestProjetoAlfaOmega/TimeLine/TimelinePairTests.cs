﻿using AlgoGlauNamespace.Timeline;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgoGlauNamespace.SDKEncapsulation;
using PTLRuntime.NETScript;

namespace UnitTestProjectRobo.TimeLine
{
    [TestClass]
    public class TimelinePairTests
    {
        private TimeLinePair timelinePair = new TimeLinePair();

        [TestMethod]
        public void SetEntrada_TryingToAddNullOrIncompletePosition_ReturnMustBeFalse()
        {
            Assert.IsFalse(timelinePair.SetEntrada(null));
            Assert.IsFalse(timelinePair.SetEntrada(new PositionWrapper() { }));            
            Assert.IsFalse(timelinePair.SetEntrada(new PositionWrapper() { Id = "1", NumRobo = "0", OpenPrice = 0, Side = Operation.Buy }));            
        }

        [TestMethod]
        public void SetEntrada_AddingPositionOnEmptyStatus_StatusMustBeOpen()
        {
            Assert.IsTrue(timelinePair.SetEntrada(new PositionWrapper() { Amount = -1, Id = "1", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy }));
            Assert.IsTrue(timelinePair.Status == TimelinePairStatus.open);
        }

        [TestMethod]
        public void SetEntrada_AddingPositionOnNotEmptyStatus_ReturnMustBeFalse()
        {
            //Arrange
            Assert.IsTrue(timelinePair.SetEntrada(new PositionWrapper() { Amount = -1, Id = "1", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy }));
            Assert.IsTrue(timelinePair.Status == TimelinePairStatus.open);
            //Act
            Assert.IsFalse(timelinePair.SetEntrada(new PositionWrapper() { Amount = 10, Id = "2", NumRobo = "1", OpenPrice = 200, Side = Operation.Sell }));
        }

        [TestMethod]
        public void SetSaida_OnEmptyTimelinePairOrInvalidData_ReturnMustBeFalse()
        {
            Assert.IsFalse(timelinePair.SetSaida(null)); //null
            Assert.IsFalse(timelinePair.SetSaida(new PositionWrapper() { Id = "1", NumRobo = "0", OpenPrice = 0, Side = Operation.Buy })); //incomplete
            Assert.IsFalse(timelinePair.SetSaida(new PositionWrapper() { Amount = 10, Id = "2", NumRobo = "1", OpenPrice = 200, Side = Operation.Sell })); //EmptyTimelineLinePair
        }

        [TestMethod]
        public void SetSaida_SameSideWithEntrada_ReturnMustBeFalse()
        {
            //Arrange
            Assert.IsTrue(timelinePair.SetEntrada(new PositionWrapper() { Amount = -1, Id = "1", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy }));
            Assert.IsTrue(timelinePair.Status == TimelinePairStatus.open);
            //Act && Assert
            Assert.IsFalse(timelinePair.SetSaida(new PositionWrapper() { Amount = -1, Id = "3", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy }));
        }

        [TestMethod]
        public void SetSaida_EntradaIsBuySaidaHasSmallerPrice_ReturnMustBeTrue()
        {
            //Arrange
            Assert.IsTrue(timelinePair.SetEntrada(new PositionWrapper() { Amount = -1, Id = "1", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy }));
            Assert.IsTrue(timelinePair.Status == TimelinePairStatus.open);
            //Act && Assert
            Assert.IsTrue(timelinePair.SetSaida(new PositionWrapper() { Amount = 1, Id = "3", NumRobo = "0", OpenPrice = 99, Side = Operation.Sell }));
        }

        [TestMethod]
        public void SetSaida_EntradaIsSellSaidaHasHigherPrice_ReturnMustBeTrue()
        {
            //Arrange
            Assert.IsTrue(timelinePair.SetEntrada(new PositionWrapper() { Amount = 1, Id = "1", NumRobo = "0", OpenPrice = 100, Side = Operation.Sell }));
            Assert.IsTrue(timelinePair.Status == TimelinePairStatus.open);
            //Act && Assert
            Assert.IsTrue(timelinePair.SetSaida(new PositionWrapper() { Amount = -1, Id = "3", NumRobo = "0", OpenPrice = 101, Side = Operation.Buy }));
        }

        [TestMethod]
        public void SetSaida_EntradaIsBuySaidaHasBiggerAmount_ReturnMustBeFalse()
        {
            //Arrange
            Assert.IsTrue(timelinePair.SetEntrada(new PositionWrapper() { Amount = -1, Id = "1", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy }));
            Assert.IsTrue(timelinePair.Status == TimelinePairStatus.open);
            //Act && Assert
            Assert.IsFalse(timelinePair.SetSaida(new PositionWrapper() { Amount = 2, Id = "3", NumRobo = "0", OpenPrice = 110, Side = Operation.Sell }));
        }

        [TestMethod]
        public void SetSaida_EntradaAndSaidaSameAmount_TimelineMustReturnClosed()
        {
            //Arrange
            Assert.IsTrue(timelinePair.SetEntrada(new PositionWrapper() { Amount = -2, Id = "1", NumRobo = "0", OpenPrice = 100, Side = Operation.Buy }));
            Assert.IsTrue(timelinePair.Status == TimelinePairStatus.open);
            //Act && Assert
            Assert.IsFalse(timelinePair.SetSaida(new PositionWrapper() { Amount = 3, Id = "3", NumRobo = "0", OpenPrice = 110, Side = Operation.Sell })); //bigger amount
            Assert.IsTrue(timelinePair.SetSaida(new PositionWrapper() { Amount = 1, Id = "3", NumRobo = "0", OpenPrice = 99, Side = Operation.Sell })); //lowerPrice
            Assert.IsFalse(timelinePair.SetSaida(new PositionWrapper() { Amount = 1, Id = "3", NumRobo = "0", OpenPrice = 110, Side = Operation.Buy })); //Same Side
            Assert.IsFalse(timelinePair.SetSaida(new PositionWrapper() { Id = "3", NumRobo = "0", OpenPrice = 110, Side = Operation.Sell })); //null Amount 
            Assert.IsFalse(timelinePair.SetSaida(new PositionWrapper() { Amount = 1, Id = "3", NumRobo = "0", OpenPrice = 110 })); //null side 
            Assert.IsFalse(timelinePair.SetSaida(new PositionWrapper() { Amount = 1, NumRobo = "0", OpenPrice = 110, Side = Operation.Sell })); //null id
            Assert.IsTrue(timelinePair.SetSaida(new PositionWrapper() { Amount = 1, Id = "3", NumRobo = "0", OpenPrice = 110, Side = Operation.Sell }));
            Assert.IsTrue(timelinePair.Status == TimelinePairStatus.closed);
        }


    }
}
