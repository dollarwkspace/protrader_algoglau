﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using PTLRuntime.NETScript;
using PTLRuntime.NETScript.Charts;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using PTLRuntime.NETScript.Indicators;
using System.Linq;
using PTLRuntime.NETScript.Application;
using System.Threading.Tasks;


namespace Indicadores_Camilo
{

    public enum DATE_TYPE
    {
        DAILY,
        WEEKLY,
        MONTHLY
    };
    public enum PRICE_TYPE
    {
        OPEN,
        CLOSE,
        HIGH,
        LOW,
        OPEN_CLOSE,
        HIGH_LOW,
        CLOSE_HIGH_LOW,
        OPEN_CLOSE_HIGH_LOW
    }

    #region Indicador MACD
    public class Camilo_macd : NETIndicator
    {
        public Indicator macd;

        public Camilo_macd()
            : base()
        {
            #region Initialization
            base.Author = "";
            base.Comments = "";
            base.Company = "";
            base.Copyrights = "";
            base.DateOfCreation = "29.08.2018";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "";
            base.ProjectName = "Camilo MACD";
            #endregion

            base.SetIndicatorLine("Macd", Color.Green, 1, LineStyle.SimpleChart);
            base.SetIndicatorLine("Sinal", Color.White, 1, LineStyle.SimpleChart);
            base.SetIndicatorLine("Histograma", Color.Blue, 1, LineStyle.HistogrammChart);
            base.SeparateWindow = true;
        }

        [InputParameter("Sinal", 0)]
        public int signal = 5;
        [InputParameter("Rápido", 1)]
        public int fast = 10;
        [InputParameter("Lento", 2)]
        public int slow = 15;


        public override void Init()
        {
            macd = Indicators.iMACD(CurrentData, fast, slow, signal);
        }

        public override void OnQuote()
        {
            SetValue(0, macd.GetValue(0, 0));
            SetValue(1, macd.GetValue(1, 0));
            SetValue(2, macd.GetValue(0, 0) - macd.GetValue(1, 0));
        }

        public override void Complete() { macd = null; }
    }
    #endregion MACD

    public class Camilo_hilo : NETIndicator
    {
        public Indicator hi;
        public Indicator lo;

        public Camilo_hilo()
            : base()
        {
            #region Initialization
            base.Author = "";
            base.Comments = "";
            base.Company = "";
            base.Copyrights = "";
            base.DateOfCreation = "29.08.2018";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "";
            base.ProjectName = "Camilo HiLo";
            #endregion

            base.SetIndicatorLine("High", Color.Red, 3, LineStyle.SimpleChart);
            base.SetIndicatorLine("Low", Color.Red, 3, LineStyle.SimpleChart);
            base.SeparateWindow = false;
        }

        [InputParameter("Periodo", 0)]
        public int periodo = 15;

        [InputParameter("Tipo de Média", 1, new Object[]
        {
            "Simples", MAMode.SMA, "Exponencial", MAMode.EMA
        }
        )]
        public MAMode tipoDeMedia = MAMode.SMA;

        public override void Init()
        {
            hi = Indicators.iMA(CurrentData, periodo, tipoDeMedia, 0, PriceType.High);
            lo = Indicators.iMA(CurrentData, periodo, tipoDeMedia, 0, PriceType.Low);
        }

        public override void OnQuote()
        {
            SetValue(0, hi.GetValue());
            SetValue(1, lo.GetValue());

        }

        public override void Complete() { hi = null; lo = null; }
    }

    public class camilo_4mm : NETIndicator
    {
        public Indicator sinal;
        public Indicator suporte;
        public Indicator bloqueio;
        public Indicator mercado;
        public Indicator myATR;
        

        public camilo_4mm()
            : base()
        {
            #region Initialization
            base.Author = "";
            base.Comments = "";
            base.Company = "";
            base.Copyrights = "";
            base.DateOfCreation = "20/01/2019";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "";
            base.ProjectName = "Camilo 4MM";
            #endregion

            base.SetIndicatorLine("Sinal", Color.Green, 2, LineStyle.SimpleChart);
            base.SetIndicatorLine("Fast", Color.Yellow, 2, LineStyle.DotLineChart);
            base.SetIndicatorLine("Slow", Color.Blue, 2, LineStyle.DotLineChart);
            base.SetIndicatorLine("ATRSup", Color.Red, 2, LineStyle.DotLineChart);
            base.SetIndicatorLine("ATRInf", Color.Red, 2, LineStyle.DotLineChart);
            base.SetIndicatorLine("Mercado", Color.White, 1, LineStyle.DotLineChart);
            base.SeparateWindow = false;
        }

        [InputParameter("Sinal", 0)]
        public int periodo_sinal = 4;
        [InputParameter("Suporte", 1)]
        public int periodo_suporte = 15;
        [InputParameter("Bloqueio", 2)]
        public int periodo_bloqueio = 25;
        [InputParameter("Mercado", 3)]
        public int periodo_mercado = 10;
        [InputParameter("Multiplicador", 4, 0, 1000, 5, 0.00001)]
        public double multiplicador = 2.5;                

        [InputParameter("Tipo de Média", 5, new Object[]
        {
            "Simples", MAMode.SMA, "Exponencial", MAMode.EMA
        }
        )]
        public MAMode tipoDeMedia = MAMode.EMA;

        public override void Init()
        {
            try
            {
                sinal = Indicators.iMA(CurrentData, periodo_sinal, tipoDeMedia);
                suporte = Indicators.iMA(CurrentData, periodo_suporte, tipoDeMedia);
                bloqueio = Indicators.iMA(CurrentData, periodo_bloqueio, tipoDeMedia);
                mercado = Indicators.iMA(CurrentData, periodo_mercado, tipoDeMedia);
                myATR = Indicators.iATR(CurrentData, periodo_suporte, MAMode.EMA);
            }
            catch (Exception exc)
            {
                Print($"Indicador: Erro na inicialização do indicador. {0}", exc.Message);
            }
        }

        public override void OnQuote()
        {
            try
            {                   
                SetValue(0, sinal.GetValue());
                SetValue(1, suporte.GetValue());
                SetValue(2, bloqueio.GetValue());
                SetValue(3, suporte.GetValue() + multiplicador * myATR.GetValue());
                SetValue(4, suporte.GetValue() - multiplicador * myATR.GetValue());
                SetValue(5, mercado.GetValue());                
            }
            catch (Exception exc)
            {
                Print("Indicador: ERRO no ONQUOTE!");
            }
        }

        public override void Complete()
        {
            sinal = null; suporte = null; bloqueio = null; myATR = null; mercado = null;
        }
    }

    public class Camilo_4mm_semATR : NETIndicator
    {
        public Indicator sinal;
        public Indicator suporte;
        public Indicator bloqueio;
        public Indicator mercado;

        public Camilo_4mm_semATR()
            : base()
        {
            #region Initialization
            base.Author = "";
            base.Comments = "";
            base.Company = "";
            base.Copyrights = "";
            base.DateOfCreation = "20/01/2019";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "";
            base.ProjectName = "Camilo 4MM Sem ATR";
            #endregion

            base.SetIndicatorLine("Sinal", Color.Green, 2, LineStyle.SimpleChart);
            base.SetIndicatorLine("Fast", Color.Yellow, 2, LineStyle.DotLineChart);
            base.SetIndicatorLine("Slow", Color.Blue, 2, LineStyle.DotLineChart);            
            base.SetIndicatorLine("Mercado", Color.White, 1, LineStyle.DotLineChart);
            base.SeparateWindow = false;
        }

        [InputParameter("Sinal", 0)]
        public int periodo_sinal = 4;
        [InputParameter("Suporte", 1)]
        public int periodo_suporte = 15;
        [InputParameter("Bloqueio", 2)]
        public int periodo_bloqueio = 25;
        [InputParameter("Mercado", 3)]
        public int periodo_mercado = 10;        

        [InputParameter("Tipo de Média", 3, new Object[]
        {
            "Simples", MAMode.SMA, "Exponencial", MAMode.EMA
        }
        )]
        public MAMode tipoDeMedia = MAMode.SMA;

        public override void Init()
        {
            try
            {
                sinal = Indicators.iMA(CurrentData, periodo_sinal, tipoDeMedia);
                suporte = Indicators.iMA(CurrentData, periodo_suporte, tipoDeMedia);
                bloqueio = Indicators.iMA(CurrentData, periodo_bloqueio, tipoDeMedia);
                mercado = Indicators.iMA(CurrentData, periodo_mercado, tipoDeMedia);                
            }
            catch (Exception exc)
            {
                Print($"Indicador: Erro na inicialização do indicador. {0}", exc.Message);
            }
        }

        public override void OnQuote()
        {
            try
            {
                SetValue(0, sinal.GetValue());
                SetValue(1, suporte.GetValue());
                SetValue(2, bloqueio.GetValue());                
                SetValue(3, mercado.GetValue());
            }
            catch (Exception exc)
            {
                Print("Indicador: ERRO no ONQUOTE!");
            }
        }

        public override void Complete()
        {
            sinal = null; suporte = null; bloqueio = null; mercado = null;
        }
    }

    public class Camilo_2mm : NETIndicator
    {
        public Indicator signal;
        public Indicator slow;

        public Camilo_2mm()
            : base()
        {
            #region Initialization
            base.Author = "";
            base.Comments = "";
            base.Company = "";
            base.Copyrights = "";
            base.DateOfCreation = "26/12/2018";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "";
            base.ProjectName = "Camilo 2MM";
            #endregion

            base.SetIndicatorLine("Sinal", Color.Green, 2, LineStyle.SimpleChart);
            base.SetIndicatorLine("Slow", Color.Blue, 2, LineStyle.DotLineChart);
            base.SeparateWindow = false;
        }

        [InputParameter("Sinal", 0)]
        public int periodo_sinal = 7;
        [InputParameter("Período Lento", 2)]
        public int periodo_lento = 50;

        [InputParameter("Tipo de Média", 3, new Object[]
        {
            "Simples", MAMode.SMA, "Exponencial", MAMode.EMA
        }
        )]
        public MAMode tipoDeMedia = MAMode.EMA;

        public override void Init()
        {
            signal = Indicators.iMA(CurrentData, periodo_sinal, tipoDeMedia);
            slow = Indicators.iMA(CurrentData, periodo_lento, tipoDeMedia);
        }

        public override void OnQuote()
        {
            SetValue(0, signal.GetValue());
            SetValue(1, slow.GetValue());
        }

        public override void Complete()
        {
            slow = null; signal = null;
        }
    }

    public class Camilo_3mm : NETIndicator
    {
        public Indicator mCurtissima;
        public Indicator mCurta;
        public Indicator mLonga;

        public Camilo_3mm()
            : base()
        {
            #region Initialization
            base.Author = "";
            base.Comments = "";
            base.Company = "";
            base.Copyrights = "";
            base.DateOfCreation = "06/12/2019";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "";
            base.ProjectName = "Camilo 3MM";
            #endregion

            base.SetIndicatorLine("Curtíssima", Color.Orange, 2, LineStyle.DotLineChart);
            base.SetIndicatorLine("Curta (Base)", Color.Blue, 2, LineStyle.SimpleChart);
            base.SetIndicatorLine("Longa", Color.Green, 2, LineStyle.SimpleChart);
            base.SeparateWindow = false;
        }

        [InputParameter("Curtissima", 0)]
        public int curtissima = 3;
        [InputParameter("Curta (Base)", 1)]
        public int curta = 7;
        [InputParameter("Longa", 2)]
        public int longa = 35;

        [InputParameter("Tipo de Média", 3, new Object[]
        {
            "Simples", MAMode.SMA, "Exponencial", MAMode.EMA
        }
        )]
        public MAMode tipoDeMedia = MAMode.EMA;

        public override void Init()
        {
            mCurtissima = Indicators.iMA(CurrentData, curtissima, tipoDeMedia);
            mCurta = Indicators.iMA(CurrentData, curta, tipoDeMedia);
            mLonga = Indicators.iMA(CurrentData, longa, tipoDeMedia);
        }

        public override void OnQuote()
        {
            SetValue(0, mCurtissima.GetValue());
            SetValue(1, mCurta.GetValue());
            SetValue(2, mLonga.GetValue());
        }

        public override void Complete()
        {
            mCurta = null; mCurtissima = null; mLonga = null;
        }
    }

    public class Camilo_SniperElite : NETIndicator
    {
        public Indicator MM;
        private double media = 0;
        private double mediaAnterior = 0;
        private double histoResult;
        private int contador = 0;

        public Camilo_SniperElite()
            : base()
        {
            #region Initialization
            base.Author = "";
            base.Comments = "";
            base.Company = "";
            base.Copyrights = "";
            base.DateOfCreation = "23/12/2018";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "";
            base.ProjectName = "Camilo_SniperElite";
            #endregion            
            base.SetIndicatorLine("Histograma", Color.Blue, 2, LineStyle.HistogrammChart);
            base.SetIndicatorLine("DeltaSup", Color.Yellow, 1, LineStyle.IsoDotChart);
            base.SetIndicatorLine("DeltaInf", Color.Yellow, 1, LineStyle.IsoDotChart);
            base.SetIndicatorLine("SensibilidadeSup", Color.Green, 1, LineStyle.IsoDotChart);
            base.SetIndicatorLine("SensibildiadeInf", Color.Green, 1, LineStyle.IsoDotChart);
            base.SeparateWindow = true;
        }

        [InputParameter("Sinal", 1)]
        public int periodo_sinal = 7;

        [InputParameter("Sensibilidade de Entrada", 2, 0, 100, 3, 0.001)]
        public double sensibilidadeDeEntrada = 2;

        [InputParameter("Resiliência de Saída", 2, 0, 100, 3, 0.001)]
        public double resilienciaDeSaida = 2;

        [InputParameter("Multiplicador", 3, 0, 100, 3, 0.001)]
        public double multiplicador = 10;

        public override void Init()
        {

        }

        public override void OnQuote()
        {
            try
            {
                SetValue(1, resilienciaDeSaida);
                SetValue(2, -resilienciaDeSaida);
                SetValue(3, sensibilidadeDeEntrada);
                SetValue(4, -sensibilidadeDeEntrada);

                histoResult = (media - mediaAnterior) * multiplicador;
                SetValue(0, histoResult);
            }
            catch (Exception exc)
            {
                Print("Erro OnQuote. " + exc.Message);
            }

        }

        public override void NextBar()
        {
            if (contador > periodo_sinal)
            {
                mediaAnterior = media;

                double mediaAcc = 0;

                for (int i = 0; i < periodo_sinal; i++)
                {
                    mediaAcc += CurrentData.GetPrice(PriceType.Close, i);
                }

                media = mediaAcc / periodo_sinal;
            }
            else
            {
                contador++;
            }
        }

        public override void Complete()
        {

        }
    }

    public class Camilo_Fibonacci : NETIndicator
    {
        private Indicator MM;
        private double[] fibonacci = new double[] { 1, 0 };
        private double[] retracaoFraca = new double[] { 0.764, 0.236 };
        private double retracaoModerada = 0.382;
        private double retracaoForte = 0.618;
        private double pontoMédio = 0.5;

        //retração leve 23.6% - após chegar nesse patamar, se a tendência inverter ele vai cair mais um patamar
        //retração moderada 38.20% - quando a correção chega próximo ela perde força
        //retração forte 61.80%


        public Camilo_Fibonacci()
            : base()
        {
            #region Initialization
            base.Author = "";
            base.Comments = "";
            base.Company = "";
            base.Copyrights = "";
            base.DateOfCreation = "06/01/2018";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "";
            base.ProjectName = "Camilo_Fibonacci";
            #endregion            
            base.SetIndicatorLine("Histograma", Color.Blue, 2, LineStyle.HistogrammChart);
            base.SetIndicatorLine("DeltaSup", Color.Yellow, 1, LineStyle.IsoDotChart);
            base.SetIndicatorLine("DeltaInf", Color.Yellow, 1, LineStyle.IsoDotChart);
            base.SetIndicatorLine("SensibilidadeSup", Color.Green, 1, LineStyle.IsoDotChart);
            base.SetIndicatorLine("SensibildiadeInf", Color.Green, 1, LineStyle.IsoDotChart);
            base.SeparateWindow = true;
        }

        [InputParameter("Sinal", 1)]
        public int periodo_sinal = 7;

        public override void Init()
        {
            SetValue(0, 0);
        }

        public override void OnQuote()
        {
            try
            {

            }
            catch (Exception exc)
            {
                Print("Erro OnQuote. " + exc.Message);
            }

        }

        public override void NextBar()
        {

        }

        public override void Complete()
        {

        }
    }

    public class camilo_vwap : NETIndicator
    {
        public camilo_vwap() : base()
        {
            #region Initialization
            base.Author = "PF";
            base.Password = "";
            base.ProjectName = "Camilo Vwap";
            #endregion

            base.SetIndicatorLine("VWAP daily", Color.Orange, 2, LineStyle.ShapedChart);
            base.SetIndicatorLine("VWAP weekly", Color.CornflowerBlue, 2, LineStyle.ShapedChart);
            base.SetIndicatorLine("VWAP monthly", Color.LimeGreen, 2, LineStyle.ShapedChart);
            base.SeparateWindow = false;
        }

        [InputParameter("Price_Type", 0, new object[]
        {
             "CLOSE", PRICE_TYPE.CLOSE,
             "CLOSE_HIGH_LOW", PRICE_TYPE.CLOSE_HIGH_LOW,
             "HIGH", PRICE_TYPE.HIGH,
             "HIGH_LOW", PRICE_TYPE.HIGH_LOW,
             "LOW", PRICE_TYPE.LOW,
             "OPEN", PRICE_TYPE.OPEN,
             "OPEN_CLOSE", PRICE_TYPE.OPEN_CLOSE,
             "OPEN_CLOSE_HIGH_LOW", PRICE_TYPE.OPEN_CLOSE_HIGH_LOW
        })]
        public PRICE_TYPE Price_Type = PRICE_TYPE.CLOSE_HIGH_LOW;

        [InputParameter("Enable_Daily", 1)]
        public bool Enable_Daily = true;

        [InputParameter("Enable_Weekly", 2)]
        public bool Enable_Weekly = true;

        [InputParameter("Enable_Monthly", 3)]
        public bool Enable_Monthly = true;

        [InputParameter("Start Daily", 4)]
        public TimeSpan dtStartDaily = new TimeSpan(09, 00, 00);

        [InputParameter("End Daily", 5)]
        public TimeSpan dtEndDaily = new TimeSpan(18, 00, 00);

        BarData barData;

        double nPriceArr, nTotalTPV, nTotalVol, nSumDailyTPV, nSumWeeklyTPV, nSumMonthlyTPV, price;
        double nSumDailyVol = 1;
        double nSumWeeklyVol = 1;
        double nSumMonthlyVol = 1;

        public override void Init()
        {
            barData = CurrentData as BarData;
            SetValue(0, 0);
            SetValue(1, 0);
            SetValue(2, 0);
        }


        public override void OnQuote()
        {

            barData = CurrentData as BarData;

            switch (Price_Type)
            {
                case PRICE_TYPE.OPEN:
                    nPriceArr = barData.Open();
                    break;
                case PRICE_TYPE.CLOSE:
                    nPriceArr = barData.Close();
                    break;
                case PRICE_TYPE.HIGH:
                    nPriceArr = barData.High();
                    break;
                case PRICE_TYPE.LOW:
                    nPriceArr = barData.Low();
                    break;
                case PRICE_TYPE.HIGH_LOW:
                    nPriceArr = (barData.High() + barData.Low()) / 2;
                    break;
                case PRICE_TYPE.OPEN_CLOSE:
                    nPriceArr = (barData.Open() + barData.Close()) / 2;
                    break;
                case PRICE_TYPE.CLOSE_HIGH_LOW:
                    nPriceArr = (barData.Close() + barData.High() + barData.Low()) / 3;
                    break;
                case PRICE_TYPE.OPEN_CLOSE_HIGH_LOW:
                    nPriceArr = (barData.Open() + barData.Close() + barData.High() + barData.Low()) / 4;
                    break;
                default:
                    nPriceArr = (barData.Close() + barData.High() + barData.Low()) / 3;
                    break;
            }
            nTotalTPV = (nPriceArr * barData.Volume());
            nTotalVol = barData.Volume();
            price = barData.GetPrice(PriceType.Close);

        }


        public override void NextBar()
        {
            if (Enable_Daily)
            {
                if (barData.Time().ToLocalTime() >= new DateTime(DateTime.Today.Year, DateTime.Today.Month,
                                DateTime.Today.Day, dtStartDaily.Hours, dtStartDaily.Minutes, dtStartDaily.Seconds).ToLocalTime() &&
                                barData.Time().ToLocalTime() < new DateTime(DateTime.Today.Year, DateTime.Today.Month,
                                DateTime.Today.Day, dtEndDaily.Hours, dtEndDaily.Minutes, dtEndDaily.Seconds).ToLocalTime())
                {
                    nSumDailyTPV += nTotalTPV;
                    nSumDailyVol += nTotalVol;
                    SetValue(0, nSumDailyTPV / nSumDailyVol);
                }
            }
            if (Enable_Weekly)
            {
                if (barData.Time().ToLocalTime() >= new DateTime(DateTime.Today.Year, DateTime.Today.Month,
                                DateTime.Today.AddDays(DayOfWeek.Monday - DateTime.Today.DayOfWeek).Day, dtStartDaily.Hours, dtStartDaily.Minutes, dtStartDaily.Seconds).ToLocalTime() &&
                                barData.Time().ToLocalTime() < new DateTime(DateTime.Today.Year, DateTime.Today.Month,
                                DateTime.Today.Day, dtEndDaily.Hours, dtEndDaily.Minutes, dtEndDaily.Seconds).ToLocalTime())
                {
                    nSumWeeklyTPV += nTotalTPV;
                    nSumWeeklyVol += nTotalVol;
                    SetValue(1, nSumWeeklyTPV / nSumWeeklyVol);
                }
            }
            if (Enable_Monthly)
            {
                if (barData.Time().ToLocalTime() >= new DateTime(DateTime.Today.Year, DateTime.Today.Month,
                                1, dtStartDaily.Hours, dtStartDaily.Minutes, dtStartDaily.Seconds).ToLocalTime() &&
                                barData.Time().ToLocalTime() < new DateTime(DateTime.Today.Year, DateTime.Today.Month,
                                DateTime.Today.Day, dtEndDaily.Hours, dtEndDaily.Minutes, dtEndDaily.Seconds).ToLocalTime())
                {
                    nSumMonthlyTPV += nTotalTPV;
                    nSumMonthlyVol += nTotalVol;
                    SetValue(2, nSumMonthlyTPV / nSumMonthlyVol);
                }
            }

        }

        public override void Complete()
        {
            barData = null;
        }

    }

    public class Camilo_Volume : NETIndicator
    {
        private Instrument instrument;
        private List<double> volumeAcc = new List<double>();
        private List<double> volumeDer = new List<double>();
        private double sumAcc = 0;
        private double sumNextBar = 0;
        private DateTime timeAnterior = DateTime.Now;
        private double derivadaMax = 0;
        private double derivadaMin = 0;

        [InputParameter("Plotar Histograma de Volumes", 0)]
        public Boolean plotHist = true;

        [InputParameter("Plotar Volume Acumulado", 1)]
        public Boolean plotVol = false;

        [InputParameter("Plotar Derivada", 2)]
        public Boolean plotDerivada = false;

        [InputParameter("tempo em milisegundos para computar derivada", 3)]
        public int milisec = 100;

        public Camilo_Volume()
            : base()
        {
            #region Initialization
            base.Author = "";
            base.Comments = "";
            base.Company = "";
            base.Copyrights = "";
            base.DateOfCreation = "29.09.2018";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "";
            base.ProjectName = "Camilo - Volume Absoluto por Cota e relativo";
            #endregion

            base.SetIndicatorLine("Volume", Color.Blue, 9, LineStyle.HistogrammChart);
            base.SetIndicatorLine("Volume", Color.Red, 9, LineStyle.HistogrammChart);
            base.SetIndicatorLine("VolAcc", Color.White, 2, LineStyle.DotLineChart);
            base.SetIndicatorLine("DerivadaMax", Color.Yellow, 12, LineStyle.HistogrammChart);
            base.SetIndicatorLine("DerivadaMin", Color.Orange, 12, LineStyle.HistogrammChart);
            base.SeparateWindow = true;
        }

        public override void Init()
        {
            instrument = Instruments.Current;
            Instruments.NewTrade += OnTrade;

            Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);
        }

        private void OnTrade(Instrument instr, Trade trade)
        {
            try
            {
                var volume = trade.Size;
                if (trade.Aggressor == AggressorFlag.Bid) volume = -volume;

                if (trade.Aggressor == AggressorFlag.Ask || trade.Aggressor == AggressorFlag.Bid)
                {
                    sumAcc += volume;
                    sumNextBar += volume;
                    volumeAcc.Add(sumAcc);
                    if (plotHist && sumNextBar > 0) { SetValue(0, sumNextBar); SetValue(1, 0); }
                    if (plotHist && sumNextBar < 0) { SetValue(1, sumNextBar); SetValue(0, 0); }
                    if (plotVol) SetValue(2, sumAcc);

                    double deltaTime = trade.Time.Subtract(timeAnterior).TotalMilliseconds;
                    volumeDer.Add(volume);

                    if (deltaTime >= milisec)
                    {
                        var deltaVolume = volumeDer.Sum();
                        var derivada = deltaVolume / (deltaTime / 1000);
                        timeAnterior = trade.Time;
                        volumeDer.Clear();

                        if (derivada > derivadaMax) derivadaMax = derivada;
                        if (derivada < derivadaMin) derivadaMin = derivada;

                        if (plotDerivada)
                        {
                            SetValue(3, derivadaMax);
                            SetValue(4, derivadaMin);
                        }

                    }
                }
            }
            catch (Exception exc)
            {
                Alert(exc.Message);
            }

        }

        public override void OnQuote()
        {
            base.OnQuote();
        }

        public override void NextBar()
        {
            sumNextBar = 0;
            derivadaMax = 0;
            derivadaMin = 0;
            SetValue(3, 0);
            SetValue(4, 0);
        }

        public override void Complete()
        {
            Instruments.Unsubscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);
            Instruments.NewTrade -= OnTrade;
            volumeAcc.Clear();
            volumeAcc = null;

        }
    }

    public class Camilo_Derivadas : NETIndicator
    {
        private Instrument instrument;

        private List<double> volumeDer = new List<double>();
        private List<double> derMax = new List<double>();
        private List<double> derMin = new List<double>();

        private DateTime timeAnterior = DateTime.Now;
        private double derivadaMax = 0;
        private double derivadaMin = 0;
        private double dpDerivadaMax = 0;
        private double dpDerivadaMin = 0;

        [InputParameter("Plotar Derivada?")]
        public Boolean plotDerivada = true;

        [InputParameter("Plotar Desvio Padrão da Derivada?")]
        public Boolean plotDP = false;

        [InputParameter("tempo em milisegundos para computar derivada")]
        public int milisec = 100;

        [InputParameter("Buffer das Derivadas")]
        public int buffer = 5;

        public Camilo_Derivadas()
            : base()
        {
            #region Initialization
            base.Author = "";
            base.Comments = "";
            base.Company = "";
            base.Copyrights = "";
            base.DateOfCreation = "29.09.2018";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "";
            base.ProjectName = "Camilo - Volume Absoluto por Cota e relativo";
            #endregion

            base.SetIndicatorLine("DerivadaMax", Color.Yellow, 12, LineStyle.HistogrammChart);
            base.SetIndicatorLine("DerivadaMin", Color.Orange, 12, LineStyle.HistogrammChart);
            base.SetIndicatorLine("Desvio Padrão Derivada Max", Color.Blue, 3, LineStyle.SimpleChart);
            base.SetIndicatorLine("Desvio Padrão Derivada Min", Color.Red, 3, LineStyle.SimpleChart);
            base.SeparateWindow = true;
        }

        public override void Init()
        {
            instrument = Instruments.Current;
            Instruments.NewTrade += OnTrade;
            Fills.FillAdded += Fills_FillAdded;

            Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);
        }

        private void Fills_FillAdded(Fill lastFill)
        {
            //outputting of all the information about the trade.
            Print(
                "--------------------------------------------\n" +
                "Instrument : \t" + lastFill.Instrument + "\n" +
                "Account : \t" + lastFill.Account + "\n" +
                "Id : \t" + lastFill.Id + "\n" +
                "OrdeId : \t" + lastFill.OrderId + "\n" +
                "PositionId : \t" + lastFill.PositionId + "\n" +
                "Amount : \t" + lastFill.Amount + "\n" +
                "Price : \t" + lastFill.Price + "\n" +
                "Commission : \t" + lastFill.Commission + "\n" +
                "Profit : \t" + lastFill.Profit + "\n" +
                "Time : \t" + lastFill.Time + "\n" +
                "Side : \t" + lastFill.Side + "\n" +
                "ExternalId : \t" + lastFill.ExternalId + "\n" +
                "ExternalPrice : \t" + lastFill.ExternalPrice + "\n" +
                "Exchange : \t" + lastFill.Exchange + "\n" +
                "Type : \t" + lastFill.Type + "\n" +
                "--------------------------------------------"
            );
        }

        private void OnTrade(Instrument instr, Trade trade)
        {
            try
            {
                var volume = trade.Size;
                if (trade.Aggressor == AggressorFlag.Bid) volume = -volume;

                if (trade.Aggressor == AggressorFlag.Ask || trade.Aggressor == AggressorFlag.Bid)
                {
                    double deltaTime = trade.Time.Subtract(timeAnterior).TotalMilliseconds;
                    volumeDer.Add(volume);

                    if (deltaTime >= milisec)
                    {
                        var deltaVolume = volumeDer.Sum();
                        var derivada = deltaVolume / (deltaTime / 1000);
                        timeAnterior = trade.Time;
                        volumeDer.Clear();

                        if (derivada > derivadaMax) derivadaMax = derivada;
                        if (derivada < derivadaMin) derivadaMin = derivada;

                        derMax.Add(derivadaMax);
                        derMin.Add(derivadaMin);

                        var dpMax = CalculaDesvioPadrao(derMax);
                        var dpMin = CalculaDesvioPadrao(derMin);

                        if (dpMax > dpDerivadaMax) dpDerivadaMax = dpMax;
                        if (dpMin > dpDerivadaMin) dpDerivadaMin = dpMin;

                        if (derMax.Count > buffer)
                        {
                            derMax.RemoveAt(0);
                            derMin.RemoveAt(0);


                            SetValue(2, dpDerivadaMax);
                            SetValue(3, dpDerivadaMin);
                        }

                        if (plotDerivada)
                        {
                            SetValue(0, derivadaMax);
                            SetValue(1, derivadaMin);
                        }

                    }
                }
            }
            catch (Exception exc)
            {
                Alert(exc.Message);
            }

        }

        public override void OnQuote()
        {
            base.OnQuote();
        }

        public override void NextBar()
        {
            derivadaMax = 0;
            derivadaMin = 0;
            dpDerivadaMax = 0;
            dpDerivadaMin = 0;
        }

        public override void Complete()
        {
            Instruments.Unsubscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);
            Instruments.NewTrade -= OnTrade;
            Fills.FillAdded -= Fills_FillAdded;
        }

        public double CalculaDesvioPadrao(List<double> lista)
        {
            var N = lista.Count;
            double media = lista.Sum() / N;
            double variancia = lista.Sum(val => Math.Pow((val - media), 2));
            return Math.Sqrt(variancia / N);
        }


    }

    public class camilo_Agressores : NETIndicator
    {
        private Instrument instrument;
        private List<Trade> volumeAgressoresAccTempo = new List<Trade>();
        private List<Trade> volumeAgressoresAccCandle = new List<Trade>();
        private double somaVolumeAgressoresTempo = 0;
        private double somaVolumeAgressoresCandle = 0;
        private DateTime horaAnterior = DateTime.Now;
        private int i = 0;

        [InputParameter("tempo em milisegundos para computar derivadas", 0)]
        public int milisec = 100;

        public camilo_Agressores()
            : base()
        {
            #region Initialization
            base.Author = "";
            base.Comments = "";
            base.Company = "";
            base.Copyrights = "";
            base.DateOfCreation = "13.02.2019";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "";
            base.ProjectName = "(Agressores x Volume) ";
            #endregion

            base.SetIndicatorLine("(Agressores x Volume) / candle ", Color.Yellow, 9, LineStyle.HistogrammChart);
            base.SetIndicatorLine("(Agressores x Volume) / tempo ", Color.Blue, 2, LineStyle.DotLineChart);
            base.SeparateWindow = true;
        }

        public override void Init()
        {
            instrument = Instruments.Current;
            Instruments.NewTrade += OnTrade;

            Instruments.Subscribe(instrument, QuoteTypes.Trade|QuoteTypes.Quote);
        }

        private void OnTrade(Instrument instr, Trade trade)
        {
            try
            {
                if (trade.Aggressor == AggressorFlag.Ask || trade.Aggressor == AggressorFlag.Bid)
                {
                    double deltaTime = trade.Time.Subtract(horaAnterior).TotalMilliseconds;
                    volumeAgressoresAccTempo.Add(trade);
                    volumeAgressoresAccCandle.Add(trade);

                    if (deltaTime >= milisec)
                    {
                        horaAnterior = trade.Time;
                        volumeAgressoresAccTempo.Clear();
                        somaVolumeAgressoresTempo = 0;
                    }

                    somaVolumeAgressoresTempo += trade.Size * ((trade.Aggressor == AggressorFlag.Ask) ? 1 : -1);
                    somaVolumeAgressoresCandle += trade.Size * ((trade.Aggressor == AggressorFlag.Ask) ? 1 : -1);
                }
            }
            catch (Exception exc)
            {
                Alert(exc.Message);
            }

        }

        public override void NextBar()
        {            
            volumeAgressoresAccCandle.Clear();
            volumeAgressoresAccTempo.Clear();
            somaVolumeAgressoresTempo = 0;
            somaVolumeAgressoresCandle = 0;
        }

        public override void OnQuote()
        {
            try
            {
                SetValue(0, somaVolumeAgressoresCandle);
                SetValue(1, somaVolumeAgressoresTempo);

            } catch (Exception ex)
            {
                
            }
        }

        public override void OnPaintChart(object sender, PaintChartEventArgs args)
        {
            i = 15;
            if (volumeAgressoresAccTempo.Count > 0)
            {
                volumeAgressoresAccTempo.ForEach(x =>
                {                    
                    PrintTela($"{x.Size} {x.Aggressor} R${x.Price}", Color.White, args.Rectangle.Width - 100, i, args);
                    i += 15;                    
                });
            }
        }

        private void PrintTela(string msg, Color cor, int x, int y, PaintChartEventArgs args, int size = 10, FontStyle style = FontStyle.Regular)
        {
            args.Graphics.DrawString(msg, new Font("Tahoma", size, style), new SolidBrush(cor), new Point(x, y));
        }

        public override void Complete()
        {
            Instruments.Unsubscribe(instrument, QuoteTypes.Trade | QuoteTypes.Quote);
            Instruments.NewTrade -= OnTrade;

        }
    }

    public class MarketDepth : NETIndicator
    {
        public MarketDepth()
            : base()
        {
            #region // Initialization
            base.Author = "PFSoft";
            base.Comments = "Market depth .NET";
            base.Company = "PFSoft";
            base.Copyrights = "(c) PFSoft";
            base.DateOfCreation = "21.07.2014";
            base.ExpirationDate = 0;
            base.Version = "";
            base.Password = "66b4a6416f59370e942d353f08a9ae36";
            base.ProjectName = "Market depth";
            #endregion

            base.SetIndicatorLine("line1", Color.Blue, 1, LineStyle.SimpleChart);
            base.SeparateWindow = false;
        }


        [InputParameter("Color")]
        public Color Color = Color.Black;

        private Font font;
        private Brush brush;
        private Instrument instrument;
        private List<Trade> lastTrades = new List<Trade>();
        private const int COUNT_TRADE = 20;


        /// <summary>
        /// This function will be called after creating
        /// </summary>
        public override void Init()
        {
            font = new Font("Tahoma", 10);
            brush = new SolidBrush(Color);

            instrument = Instruments.Current;

            Instruments.NewTrade += OnTrade;
            Instruments.NewLevel2 += OnQuote;

            Instruments.Subscribe(instrument, QuoteTypes.Trade | QuoteTypes.Level2);

            lastTrades.Add(instrument.GetLastTrade());

        }

        /// <summary>
        /// This function will be called before removing
        /// </summary>
        public override void Complete()
        {
            Instruments.NewTrade -= OnTrade;
            Instruments.NewLevel2 -= OnQuote;

            Instruments.Unsubscribe(instrument, QuoteTypes.Trade | QuoteTypes.Level2);

            lastTrades.Clear();

        }

        private void OnQuote(Instrument instr, Level2 quote)
        {
            //force refresh
            CurrentChart.Refresh();
        }

        private void OnTrade(Instrument instr, Trade trade)
        {
            lastTrades.Insert(0, trade);
            Alert("A trade have arrived!" + trade.Aggressor);

            if (lastTrades.Count > COUNT_TRADE)
                lastTrades.RemoveAt(COUNT_TRADE);

            CurrentChart.Refresh();
        }

        private void DrawLevel2(Level2[] quotes, Graphics gr, Rectangle rect, int offset)
        {

            gr.DrawString("Price", font, brush, rect.X + offset, rect.Y + 40);
            gr.DrawString("Size", font, brush, rect.X + offset + 70, rect.Y + 40);

            for (int i = 0; i < quotes.Length; i++)
            {
                Level2 quote = quotes[i];
                //getting the information about Level2 quote
                double price = quote.Price;
                double size = quote.Size;

                string text_bid_price = String.Format("{0}", instrument.FormatPrice(price));
                string text_bid_size = String.Format("{0:N2}", size / Instruments.Current.LotSize);

                gr.DrawString(text_bid_price, font, brush, rect.X + offset, rect.Y + i * 20 + 60);
                gr.DrawString(text_bid_size, font, brush, rect.X + offset + 70, rect.Y + i * 20 + 60);
            }
        }

        private void DrawTrades(Graphics gr, Rectangle rect)
        {
            gr.DrawString("Time", font, brush, rect.X + 400, rect.Y + 40);
            gr.DrawString("Price", font, brush, rect.X + 500, rect.Y + 40);
            gr.DrawString("Size", font, brush, rect.X + 600, rect.Y + 40);

            for (int i = 0; i < lastTrades.Count; i++)
            {
                Trade trade = lastTrades[i];
                //getting the information about this trade
                DateTime time = trade.Time;
                double price = trade.Price;
                double size = trade.Size;

                string text_trade_time = String.Format("{0:HH:mm:ss}", time);
                string text_trade_price = String.Format("{0}", instrument.FormatPrice(price));
                string text_trade_size = String.Format("{0:N2}", size / instrument.LotSize);

                gr.DrawString(text_trade_time, font, brush, rect.X + 400, rect.Y + i * 20 + 60);
                gr.DrawString(text_trade_price, font, brush, rect.X + 500, rect.Y + i * 20 + 60);
                gr.DrawString(text_trade_size, font, brush, rect.X + 600, rect.Y + i * 20 + 60);
            }
        }

        public override void OnPaintChart(object sender, PaintChartEventArgs args)
        {
            Graphics gr = args.Graphics;
            Rectangle rect = args.Rectangle;

            //drawing bids, ask and trades on the chart
            DrawLevel2(instrument.getBidsDepth(), gr, rect, 20);
            DrawLevel2(instrument.getAsksDepth(), gr, rect, 180);
            DrawTrades(gr, rect);
        }
    }



}
